@extends('themes.default1.client.layout.logclient')

@section('title')
My Tickets -
@stop

@section('myticket')
class="active"
@stop

@section('content')
<!-- Main content -->
<div id="content" class="site-content col-md-12">
<?php
$org_id=App\Model\helpdesk\Agent_panel\User_org::where('user_id', '=', Auth::user()->id)->select('org_id')->first();

$system = App\Model\helpdesk\Settings\CommonSettings::
                    where('option_name', '=', 'user_show_org_ticket')
                    ->first();
$status_array = new App\Http\Controllers\Agent\helpdesk\Filter\FilterController(new Illuminate\Http\Request);
if ($org_id && $system->status != 1) {
    $org_members=App\Model\helpdesk\Agent_panel\User_org::where('org_id', '=', $org_id->org_id)->pluck('user_id')->toArray();
    $open = App\Model\helpdesk\Ticket\Tickets::whereIn('user_id', $org_members)
        ->where(function ($query) use ($status_array) {
            $query->whereIN('status', $status_array->getStatusArray('open'))
            ->orWhereIN('status', $status_array->getStatusArray('approval'));
        })
        ->orderBy('id', 'DESC')
        ->paginate(20);
    $close = App\Model\helpdesk\Ticket\Tickets::whereIn('user_id', $org_members)
        ->whereIN('status', $status_array->getStatusArray('closed'))
        ->orderBy('id', 'DESC')
        ->paginate(20);
} else {
    $open = App\Model\helpdesk\Ticket\Tickets::where('user_id', '=', Auth::user()->id)
        ->where(function ($query) use ($status_array) {
            $query->whereIN('status', $status_array->getStatusArray('open'))
            ->orWhereIN('status', $status_array->getStatusArray('approval'));
        })
        ->orderBy('id', 'DESC')
        ->paginate(20);
    $close = App\Model\helpdesk\Ticket\Tickets::where('user_id', '=', Auth::user()->id)
        ->whereIN('status', $status_array->getStatusArray('closed'))
        ->orderBy('id', 'DESC')
        ->paginate(20);
}
?>
    <div class="nav-tabs-custom">
    {!! Form::open(['route'=>'select_all','method'=>'post']) !!}
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" onclick="removecheck()">{!! Lang::get('lang.opened') !!} <small class="label bg-orange">{!! $open->total() !!}</small></a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" onclick="removecheck()">{!! Lang::get('lang.closed') !!} <small class="label bg-green">{!! $close->total() !!}</small></a></li>
            <div class="pull-right">
                <div class="btn-group">
                <?php
                 $statuses = App\Model\helpdesk\Ticket\Ticket_Status::where('visibility_for_client',1)->where('allow_client',1)->where('purpose_of_status','!=',5)->get();
                 $unselect_status=App\Model\helpdesk\Ticket\Ticket_Status::where('visibility_for_client',1)->where('allow_client',0)->where('purpose_of_status','!=',5)->get();

                 ?>

                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="d1"><i class="fa fa-exchange" style="color:teal;" id="hidespin"> </i><i class="fa fa-spinner fa-spin" style="color:teal; display:none;" id="spin"></i>
                    {!! Lang::get('lang.change_status') !!} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach($statuses as $ticket_status)    
                    <li onclick="changeStatus({!! $ticket_status->id !!}, '{!! $ticket_status->name !!}')"><a href="#"><i class="{!! $ticket_status->icon !!}" style="color:{!! $ticket_status->icon_color !!};"> </i>{!! $ticket_status->name !!}</a></li>
                    @endforeach
                    @foreach($unselect_status as $ticket_status)    
                    <li '{!! $ticket_status->name !!}')"><a><i class="{!! $ticket_status->icon !!}" style="color:{!! $ticket_status->icon_color !!};" readonly="readonly"> </i>{!! $ticket_status->name !!}</a></li>
                    @endforeach
                </ul>
            </div>
            </div>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <div class="mailbox-controls tabmailbox">
                    <!-- Check all button -->
                    <!-- <a class="btn btn-default btn-sm checkbox-toggle" id="ch1"><i class="fa fa-square-o"></i></a> -->
                    <div class="tabcnt" id="refresh21">
                        {!! $open->count().'-'.$open->total(); !!}
                    </div>
                </div>
                <div class=" table-responsive mailbox-messages"  id="refresh1">
                    <p style="display:none;text-align:center; position:fixed; margin-left:37%;margin-top:-80px;" id="show1" class="text-red"><b>{!! Lang::get('lang.loading') !!}</b></p>
                    <!-- table -->
                    <table class="table table-hover table-striped">
                        <thead class="ribbon">
						<th>
                            <a class="btn btn-default btn-sm checkbox-toggle" id="ch1"><i class="fa fa-square-o"></i></a>
                        </th>
                        <th>
                            {!! Lang::get('lang.subject') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.ticket_id') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.priority') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.last_replier') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.last_activity') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.status') !!}
                        </th>
                        </thead>
                        <tbody id="hello">
                            @foreach ($open  as $ticket )
                            <tr <?php if ($ticket->seen_by == null) { ?> style="color:green;" <?php }
    ?> >
                                <td class="rbmr"><input type="checkbox" class="icheckbox_flat-blue ch1" name="select_all[]" value="{{$ticket->id}}"/></td>
                                <?php
                                $title = App\Model\helpdesk\Ticket\Ticket_Thread::where('ticket_id', '=', $ticket->id)->orderBy('id')->first();
                                $string = strip_tags($title->title);
                                if (strlen($string) > 40) {
                                    $stringCut = substr($string, 0, 25);
                                    $string = $stringCut.'....';
                                }
                                $TicketData = App\Model\helpdesk\Ticket\Ticket_Thread::where('ticket_id', '=', $ticket->id)
                                    ->where('user_id', '!=' , null)
                                    ->min('id');
                                $TicketDatarow = App\Model\helpdesk\Ticket\Ticket_Thread::where('id', '=', $TicketData)->first();
                                $LastResponse = App\User::where('id', '=', $TicketDatarow->user_id)->first();
                                if ($LastResponse->role == "user") {
                                    $rep = "#F39C12";
                                    $username = $LastResponse->user_name;
                                } else {
                                    $rep = "#000";
                                    $username = $LastResponse->first_name . " " . $LastResponse->last_name;
                                    if ($LastResponse->first_name == null || $LastResponse->last_name == null) {
                                        $username = $LastResponse->user_name;
                                    }
                                }
                                $titles = App\Model\helpdesk\Ticket\Ticket_Thread::where('ticket_id', '=', $ticket->id)->where('is_internal', '=', 0)->get();
                                $count = count($titles);
                                foreach ($titles as $title) {
                                    $title = $title;
                                }
                                ?>
                                <td class="mailbox-name rbmr"><a href="{!! URL('check_ticket',[Crypt::encrypt($ticket->id)]) !!}" title="{!! $title->title !!}">{{$string}}   </a>({!! $count!!})<i class="fa fa-comment"></i></td>
                                <td class="mailbox-Id rbmr">#{!! $ticket->ticket_number !!}</td>
                                <td class="mailbox-priority rbmr"><spam>{{$ticket->priority->priority}}</spam></td>

                        <td class="mailbox-last-reply rbmr" style="color: {!! $rep !!}">{!! $username !!}</td>
                        <td class="mailbox-last-activity rbmr">{!! faveoDate($title->updated_at) !!}</td>
                        <?php $status = App\Model\helpdesk\Ticket\Ticket_Status::where('id', '=', $ticket->status)->first(); ?>
                        <td class="mailbox-date rbmr">{!! $status->name !!}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table><!-- /.table -->
                    <div class="pull-right">
                        <?php echo $open->setPath(url('mytickets'))->render(); ?>&nbsp;
                    </div>
                </div><!-- /.mail-box-messages -->
            </div><!-- /.box-body -->
            {{-- /.tab_1 --}}
            <div class="tab-pane" id="tab_2">
                <div class="mailbox-controls">
                    <!-- Check all button -->
                    <a class="btn btn-default btn-sm checkbox-toggle" id="ch2"><i class="fa fa-square-o"></i></a>
                    <div class="pull-right" id="refresh22">
                        {!! $close->count().'-'.$close->total(); !!}
                    </div>
                </div>
                <div class=" table-responsive mailbox-messages" id="refresh2">
                    <p style="display:none;text-align:center; position:fixed; margin-left:40%;margin-top:-70px;" id="show2" class="text-red"><b>{!! Lang::get('lang.loading') !!}</b></p>
                    <!-- table -->
                    <table class="table table-hover table-striped">
                        <thead>
                        <th></th>
                        <th>
                            {!! Lang::get('lang.subject') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.ticket_id') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.priority') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.last_replier') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.last_activity') !!}
                        </th>
                        <th>
                            {!! Lang::get('lang.status') !!}
                        </th>
                        </thead>
                        <tbody id="hello">
                            @foreach ($close  as $ticket )
                            <tr <?php if ($ticket->seen_by == null) { ?> style="color:green;" <?php }
                        ?> >
                                <td><input type="checkbox" class="icheckbox_flat-blue ch2" name="select_all[]" value="{{$ticket->id}}"/></td>
                                <?php
                                $title = App\Model\helpdesk\Ticket\Ticket_Thread::where('ticket_id', '=', $ticket->id)->first();
                                $string = strip_tags($title->title);
                                if (strlen($string) > 40) {
                                    $stringCut = substr($string, 0, 40);
                                    $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . ' ...';
                                }
                                $TicketData = App\Model\helpdesk\Ticket\Ticket_Thread::where('ticket_id', '=', $ticket->id)->min('id');
                                $TicketDatarow = App\Model\helpdesk\Ticket\Ticket_Thread::where('id', '=', $TicketData)->first();
                                $LastResponse = App\User::where('id', '=', $TicketDatarow->user_id)->first();
                                if ($LastResponse->role == "user") {
                                    $rep = "#F39C12";
                                    $username = $LastResponse->user_name;
                                } else {
                                    $rep = "#000";
                                    $username = $LastResponse->first_name . " " . $LastResponse->last_name;
                                    if ($LastResponse->first_name == null || $LastResponse->last_name == null) {
                                        $username = $LastResponse->user_name;
                                    }
                                }
                                $titles = App\Model\helpdesk\Ticket\Ticket_Thread::where('ticket_id', '=', $ticket->id)->where("is_internal", "=", 0)->get();
                                $count = count($titles);
                                foreach ($titles as $title) {
                                    $title = $title;
                                }
                                ?>
                                <td class="mailbox-name"><a href="{!! URL('check_ticket',[Crypt::encrypt($ticket->id)]) !!}" title="{!! $title->title !!}">{{$string}}   </a> ({!! $count!!}) <i class="fa fa-comment"></i></td>
                                <td class="mailbox-Id">#{!! $ticket->ticket_number !!}</td>
                                <?php $priority = App\Model\helpdesk\Ticket\Ticket_Priority::where('priority_id', '=', $ticket->priority_id)->first(); ?>
                                <td class="mailbox-priority"><spam class="btn btn-{{$priority->priority_color}} btn-xs">{{$priority->priority}}</spam></td>
                        <td class="mailbox-last-reply" style="color: {!! $rep !!}">{!! $username !!}</td>
                        <td class="mailbox-last-activity">{!! faveoDate($title->updated_at) !!}</td>
                        <?php $status = App\Model\helpdesk\Ticket\Ticket_Status::where('id', '=', $ticket->status)->first(); ?>
                        <td class="mailbox-date">{!! $status->name !!}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table><!-- /.table -->
                    <div class="pull-right">
                        <?php echo $close->setPath(url('mytickets'))->render(); ?>&nbsp;
                    </div>
                </div><!-- /.mail-box-messages -->
                {!! Form::close() !!}
            </div>
        </div><!-- /. box -->
    </div>
</div>

<script>
    $(function() {
        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function() {
            var clicks = $(this).data('clicks');
            if (clicks) {
                //Uncheck all checkboxes
                $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
            } else {
                //Check all checkboxes
                $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
            }
            $(this).data("clicks", !clicks);
        });
    });

    $(function() {
        // Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function() {
            var clicks = $(this).data('clicks');
            var check_class = $(this).attr('id');
            if (clicks) {
                //Uncheck all checkboxes
                $("."+check_class).prop('checked', true);
            } else {
                //Check all checkboxes
                $("."+check_class).prop('checked', false);
            }
            $(this).data("clicks", !clicks);
        });
    });

    $(document).ready(function() { /// Wait till page is loaded
        $('#click1').click(function() {
            $('#refresh1').load('mytickets #refresh1');
            $('#refresh21').load('mytickets #refresh21');
            $("#show1").show();
        });
    });

    $(document).ready(function() { /// Wait till page is loaded
        $('#click2').click(function() {
            $('#refresh2').load('mytickets #refresh2');
            $('#refresh22').load('mytickets #refresh22');
            $("#show2").show();
        });
    });

    function changeStatus(id, name) {
        var inputs = $('form').serialize();
        if (inputs.indexOf("select_all") == -1) {
            alert("{!! Lang::get('lang.select-ticket') !!}");
        } else {
            var t_id = [];
            $("input:checkbox[name='select_all[]']:checked").each(function(){
                t_id.push($(this).val());
            });
            var url = '{{url("ticket/change-status/")}}/'+t_id+'/'+id;
            $.ajax({
                type: "GET",
                url: url,
                dataType: "html",
                data: $(this).serialize(),
                beforeSend: function() {
                    $("#hidespin").hide();
                    $("#spin").show();
                    $("#show2").show();
                    $("#show1").show();
                },
                success: function(response) {
                    $("#show2").hide();
                    $("#show1").hide();
                    $("#hidespin").show();
                    $("#spin").hide();
                    location.reload();
                },
                error: function(response) {
                }
            })
        }
    }
    function removecheck(){
        $(".ch1, .ch2").prop('checked', false);
        $(".fa", ".checkbox-toggle").removeClass("fa-check-square-o").addClass('fa-square-o');
    }
</script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
   <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // $('#myTable1').DataTable();
   //  $('.table-hover').DataTable();
</script>
@stop