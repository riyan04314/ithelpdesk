<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        $title = App\Model\helpdesk\Settings\System::where('id', '=', '1')->first();
        if (isset($title->name)) {
            $title_name = $title->name;
        } else {
            $title_name = "SUPPORT CENTER";
        }
           $portal = App\Model\helpdesk\Theme\Portal::where('id', '=', '1')->first();
        ?>
        @if($portal->client_header_color !== 'null')
         <style type="text/css">

            .site-navigation > ul > li.active > a, .site-navigation > ul > li:hover > a, .site-navigation > ul > li > a:hover, .site-navigation > ul > li > a:focus {
                background: none !important;
                border-top-color:{{$portal->client_header_color}}!important;
                color:{{$portal->client_header_color}}!important;

            }
            .site-navigation li.active > a, .site-navigation li:hover > a, .site-navigation li > a:hover, .site-navigation li > a:focus {

                color:{{$portal->client_header_color}}!important;


            

                .navbar-default .navbar-nav > li > a {
                    color:{{$portal->client_header_color}}!important;

                }

                 .section-title > i.line {
            width:100%,!important;
     
    }
                </style>
                <style type="text/css">
            .section-title>.line{
                width: 100% !important;
            }
        </style>

  @endif

  @if($portal->client_input_fild_color !== 'null')
                  <style type="text/css">
                .form-control {
                    border-radius: 0px !important;
                    box-shadow: none;
                    border-color:{{$portal->client_input_fild_color}}!important;
                }
                </style>
  @endif


@if($portal->client_button_color !== 'null' && $portal->client_button_color !== 'null')
                  <style type="text/css">

                .btn {
                    font-weight: 600;
                    text-transform: uppercase;
                    color: #fff;
                    background-color:{{$portal->client_button_color}}!important;
                    border-color: {{$portal->client_button_border_color}}!important;
                </style>
 @endif
        <title> @yield('title') {!! strip_tags($title_name) !!} </title>
        <!-- faveo favicon -->
        <!-- <link href="{{asset("lb-faveo/media/images/favicon.ico")}}"  rel="shortcut icon" > -->
          @if($portal->icon!=0)
            <link href="{{asset("uploads/icon/$portal->icon")}}" rel="shortcut icon">
        @else
        <link href="{{asset("lb-faveo/media/images/favicon.ico")}}" rel="shortcut icon"> 
        @endif
        
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{asset("lb-faveo/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Admin LTE CSS -->
        <link href="{{asset("lb-faveo/css/AdminLTEsemi.css")}}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="{{asset("lb-faveo/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset("lb-faveo/css/ionicons.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- fullCalendar 2.2.5-->
        <link href="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{asset("lb-faveo/css/jquery.rating.css")}}" rel="stylesheet" type="text/css" />
        
        <link href="{{asset("lb-faveo/css/app.css")}}" rel="stylesheet" type="text/css" />
        
        <link href="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{asset("lb-faveo/js/jquery2.1.1.min.js")}}" type="text/javascript"></script>
        @yield('HeadInclude')

<!-- 21-06-2020 : New Template Start-->	
	<link rel="stylesheet" type="text/css" href="/arc-helpdesk-html/css/style.css"/>  <!-- Custome css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
<!-- 21-06-2020 : New Template End-->		

		
         <style type="text/css">
          
            @font-face {
                      font-family: dubaiFonts;
                      src: url({{asset("lb-faveo/fonts/DubaiW23-Light.woff")}});
                   }
            body{
                font-family:dubaiFonts !important;
            }
        </style>
		
		<style>
					
.nav > li > a {
    padding: 0px 15px;
}
.navbar {
    border-radius: 0px;
}
.navbar-inverse.yelow_menu_bg{
	background:#b78537;
}


.ribbon {
 font-size: 16px !important;
 /* This ribbon is based on a 16px font side and a 24px vertical rhythm. I've used em's to position each element for scalability. If you want to use a different font size you may have to play with the position of the ribbon elements */

 /*width: 50%;*/
    
 position: relative;
 background: #B78537;
 color: #fff;
 text-align: center;
 padding: 1em 2em; /* Adjust to suit */
 margin: 2em auto 3em; /* Based on 24px vertical rhythm. 48px bottom margin - normally 24 but the ribbon 'graphics' take up 24px themselves so we double it. */
}
.ribbon:before {
 left: 15px;
  border-right-width: 0px;
 border-left-color: transparent;

border-left-color: transparent;
border-top-width: 12px;
border-left-width: 15px;
}

.ribbon:before {
 content: "";
 position: absolute;
 display: block;
 border-style: solid;
 border-color: #966A25 transparent transparent transparent;
 bottom: 166px;
}
.ribbon  {
 left: 0;
 border-width: 1em 0 0 1em;
}
.rbmr{
	margin-top:20px !important;
	padding-top:20px !important;
}

.nav-tabs > li.active {
	background:#966a25;
	height:35px;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
	background:inherit;
	border: none;
    color: #fff;
	line-height: 32px;
}
.nav-tabs > li > a{
	background:#f9daa9;
	line-height: 32px;
}
.tabmailbox {
    padding: 0px!important;
}
.tabcnt{
	position:absolute;
	right:35px;
	color:#FFF;
	
}
.imsz img{
	width:25px;
	}
        </style>
		
    </head>
    <body>
        <div id="page" class="hfeed site">
<!-- 21-06-2020 : New Template Start-->	

<header class="menu_header">
	<div class="container-fluid">					
			<ul class="top_logos">
				<li><div class="logo_1">
					<a href="#"><img title="aman" src="/arc-helpdesk-html/images/logo.jpg" alt="dubai"></a>
				</div></li>
			
				<li><div class="logo_2" style="width:100%;">
					<a href="#"><img title="Year of Tollerance" src="/arc-helpdesk-html/images/logo_2.jpg" alt="image"></a>
				</div></li>
		
				<li style="float:right; width:40%"><div class="logo_3" style="float:right; width:100%; text-align:right;">				
					<?php
						$company 	= App\Model\helpdesk\Settings\Company::where('id', '=', '1')->first();
						$system 	= App\Model\helpdesk\Settings\System::where('id', '=', '1')->first();
						$homepageurl = '';
					?>
					@if($system->url)
						@php($homepageurl = $system->url)
						<a href="{!! $system->url !!}" rel="home">
					@else
						@php($homepageurl = url('/'))
						<a href="{{url('/')}}" rel="home">
					@endif						
					@if($company->use_logo == 1)
						<img title="Arc training" src="{{asset('uploads/company')}}{{'/'}}{{$company->logo}}" alt="image">
					@else
						@if($system->name)
							{!! $system->name !!}
						@else
							<b>SUPPORT</b> CENTER
						@endif
					@endif
					</a>
					
				</div></li>			
		   </ul>
		   
	</div>
	<div class="menu_bg d-none" style="display:none1;">
		<div class="container-fluid">	     				
			<nav>
				<ul class="menu_home">
					<li><a href="{{$homepageurl}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
					@if($system->first()->status == 1)
						<li @yield('submit')><a href="{{URL::route('form')}}">{!! Lang::get('lang.submit_a_ticket') !!}</a></li>
					@endif
					<li @yield('kb') class="dropdown">
						<?php /*<a href="{!! url('knowledgebase') !!}">*/?>
						<a class="dropdown-toggle" data-toggle="dropdown">
						{!! Lang::get('lang.knowledge_base') !!}</a>
						<span class="caret"></span>
						<ul class="dropdown-menu">
							<li><a href="{{route('category-list')}}">{!! Lang::get('lang.categories') !!}</a></li>
							<li><a href="{{route('article-list')}}">{!! Lang::get('lang.articles') !!}</a></li>
						</ul>
					</li>				
					<?php 
						$pages = App\Model\kb\Page::where('status', '1')->where('visibility', '1')->get();
					?>
					@foreach($pages as $page)
						<li><a href="{{route('pages',$page->slug)}}">{{$page->name}}</a></li>
					@endforeach
								
					@if(Auth::user())
						<li @yield('myticket')><a href="{{url('mytickets')}}">{!! Lang::get('lang.my_tickets') !!}</a></li>
						<?php /*<li @yield('profile')><a href="#" >{!! Lang::get('lang.my_profile') !!}</a>
							<ul class="dropdown-menu">
								<li>
									<div class="banner-wrapper user-menu text-center clearfix">
										<img src="{{Auth::user()->profile_pic}}"class="img-circle" alt="User Image" height="80" width="80"/><br/><br/>
										 <span>{!! Lang::get('lang.hello') !!}</span><br/>
										<h3 class="banner-title text-info h4">{{Auth::user()->first_name." ".Auth::user()->last_name}}</h3>
										<div class="banner-content">
											{{-- <a href="{{url('kb/client-profile')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.edit_profile') !!}</a> --}} <a href="{{url('auth/logout')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.log_out') !!}</a>
										</div>
										@if(Auth::user())
											@if(Auth::user()->role != 'user')
												<div class="banner-content">
													<a href="{{url('dashboard')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.dashboard') !!}</a>
												</div>
											@endif
										@endif
										@if(Auth::user())
											@if(Auth::user()->role == 'user')
												<div class="banner-content">
													<a href="{{url('client-profile')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.profile') !!}</a>
												</div>
											@endif
										@endif
									</div>
								</li>
							</ul>
						</li>	*/ ?>							                            
					@endif
				</ul><!-- .navbar-user -->
						
						
						

				 
				 <?php /*
				<li><a href="#">About Us<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
				<li><a href="#">Safety Certificate<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
				<li><a href="#">Link - Delink Company<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
				<li><a href="#">AMC<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
				*/ ?>	
				</ul>
			</nav>
			
			<div class="veloume">
			<ul class="icons_list" style="display:none;">
				<li><a href="#"><img src="/arc-helpdesk-html/images/veloum_icon.jpg" alt="image"></a></li>
				<li><a href="#"><img src="/arc-helpdesk-html/images/view_icon.jpg" alt="image"></a></li>
				<li><a href="#"><img src="/arc-helpdesk-html/images/decrease_icon.jpg" alt="image"></a></li>
				<li><a href="#"><img src="/arc-helpdesk-html/images/increase_icon.jpg" alt="image"></a></li>
				<li><a href="#"><img src="/arc-helpdesk-html/images/search_icon.jpg" alt="image"></a></li>
			</ul>
			
			
			
			
			<ul class="icons_list2">						
				@if(isset($errors))															
					<ul class="nav navbar-nav navbar-login">
						<li <?php	if (is_object($errors)) {if ($errors->first('email') || $errors->first('password')) {echo ' class="sfHover" ';}}	?> >
							<a href="#"  data-toggle="collapse"  
								<?php	if (is_object($errors)) {if ($errors->first('email') || $errors->first('password')) {}else{	echo ' class="collapsed" '; }}?> data-target="#login-form">{!! Lang::get('lang.login') !!} 
								<i class="sub-indicator fa fa-chevron-circle-down fa-fw text-muted"></i>
							</a>
						</li>
					</ul>
				@endif								 			


				<li class="dropdown" style="margin-right:20px;">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="imsz"><img src="/flag-english.png"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="/"><i class="imsz"><img src="/flag-english.png"></i> English</a></li>
						<li><a href="/"><i class="imsz"><img src="/flag-uae.jpg"></i> عربى</a></li>
					</ul>
				</li>

				@if(Auth::user())
					<li @yield('profile') class="dropdown">									
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="/arc-helpdesk-html/images/man_icon.jpg" alt="image">
							<span class="caret"></span>
						</a>					
						<ul class="dropdown-menu">
							<li>
								<div class="banner-wrapper user-menu text-center clearfix">
									<img src="{{Auth::user()->profile_pic}}"class="img-circle" alt="User Image" height="80" width="80"/><br/><br/>
									 <span>{!! Lang::get('lang.hello') !!}</span><br/>
									<h3 class="banner-title text-info h4">{{Auth::user()->first_name." ".Auth::user()->last_name}}</h3>
									<div class="banner-content">
										{{-- <a href="{{url('kb/client-profile')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.edit_profile') !!}</a> --}} <a href="{{url('auth/logout')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.log_out') !!}</a>
									</div>
									@if(Auth::user())
										@if(Auth::user()->role != 'user')
											<div class="banner-content">
												<a href="{{url('dashboard')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.dashboard') !!}</a>
											</div>
										@endif
									@endif
									@if(Auth::user())
										@if(Auth::user()->role == 'user')
											<div class="banner-content">
												<a href="{{url('client-profile')}}" class="btn btn-custom btn-xs">{!! Lang::get('lang.profile') !!}</a>
											</div>
										@endif
									@endif
								</div>
							</li>
						</ul>
					</li>
				@else
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="/arc-helpdesk-html/images/man_icon.jpg" alt="image">
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<form id="Login">
									<div class="form-group">
										<input type="email" class="form-control" id="inputEmail" placeholder="Email Address">
									</div>
									<div class="form-group">
										<input type="password" class="form-control" id="inputPassword" placeholder="Password">
									</div>
									<div class="forgot">
										<a href="reset.html">Forgot password?</a>
									</div>
									<button type="submit" class="btn btn-primary">Login</button>
								</form>
							</li>
						</ul>
					</li>				
				@endif	
								
				
			</ul>			
		</div>			
		</div>
	</div>
</header>
<!-- 21-06-2020 : New Template End-->
            <!-- Left side column. contains the logo and sidebar -->
   
                         
            <!-- Main content -->
            <div id="main" class="site-main clearfix" style="margin-top:unset;">
                <div class="container">
                    <div class="content-area">
                        <div class="row">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <i class="fa  fa-check-circle"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('success')}}
                            </div>
                            @endif
                            @if(Session::has('warning'))
                            <div class="alert alert-warning alert-dismissable">
                                <i class="fa  fa-check-circle"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! Session::get('warning') !!}
                            </div>
                            @endif
                            <!-- failure message -->
                            @if(Session::has('fails'))
                            @if(Session::has('check'))
<?php goto a; ?>
                            @endif
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <b>{!! Lang::get('lang.alert') !!} !</b>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('fails')}}
                            </div>
<?php a: ?>
                            @endif
                            @yield('content')
                            <div id="sidebar" class="site-sidebar col-md-3">
                                <div class="widget-area">
                                    <section id="section-banner" class="section">
                                        @yield('check')
                                    </section><!-- #section-banner -->
                                    <section id="section-categories" class="section">
                                        @yield('category')
                                    </section><!-- #section-categories -->
                                </div>
                            </div><!-- #sidebar -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-wrapper -->
            <?php
            $footer1 = App\Model\helpdesk\Theme\Widgets::where('name', '=', 'footer1')->first();
            $footer2 = App\Model\helpdesk\Theme\Widgets::where('name', '=', 'footer2')->first();
            $footer3 = App\Model\helpdesk\Theme\Widgets::where('name', '=', 'footer3')->first();
            $footer4 = App\Model\helpdesk\Theme\Widgets::where('name', '=', 'footer4')->first();
            ?>
            <footer id="colophon" class="site-footer" role="contentinfo" style="display:none;">
                <div class="container">
                    <div class="row col-md-12">
                        @if($footer1->title == null)
                        @else
                        <div class="col-md-3">
                            <div class="widget-area">
                                <section id="section-about" class="section">
                                    <h2 class="section-title h4 clearfix" style="word-wrap: break-word;">{!!$footer1->title!!}</h2>
                                    <div class="textwidget">
                                           <span style="word-wrap: break-word;"><?php  echo ($footer1->value);?></span>
                                    </div>
                                </section><!-- #section-about -->
                            </div>
                        </div>
                        @endif
                        @if($footer2->title == null)
                        @else
                        <div class="col-md-3">
                            <div class="widget-area">
                                <section id="section-latest-news" class="section">
                                    <h2 class="section-title h4 clearfix" style="word-wrap: break-word;">{!!$footer2->title!!}</h2>
                                    <div class="textwidget">
                                         <span style="word-wrap: break-word;"><?php  echo ($footer2->value);?></span>
                                    </div>
                                </section><!-- #section-latest-news -->
                            </div>
                        </div>
                        @endif
                        @if($footer3->title == null)
                        @else
                        <div class="col-md-3">
                            <div class="widget-area">
                                <section id="section-newsletter" class="section">
                                    <h2 class="section-title h4 clearfix" style="word-wrap: break-word;">{!!$footer3->title!!}</h2>
                                    <div class="textwidget">
                                         <span style="word-wrap: break-word;"><?php  echo ($footer3->value);?></span>
                                    </div>
                                </section><!-- #section-newsletter -->
                            </div>
                        </div>
                        @endif
                        @if($footer4->title == null)
                        @else
                        <div class="col-md-3">
                            <div class="widget-area">
                                <section id="section-newsletter" class="section">
                                    <h2 class="section-title h4 clearfix" style="word-wrap: break-word;">{{$footer4->title}}</h2>
                                    <div class="textwidget">
                                            <span style="word-wrap: break-word;"><?php  echo ($footer4->value);?></span>
                                    </div>
                                </section>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                    <hr style="color:#E5E5E5"/>
                    <div class="row">
                        <div class="site-info col-md-6">
                            <p class="text-muted">{!! Lang::get('lang.copyright') !!} &copy; {!! date('Y') !!}  <a href="{!! $company->website !!}" target="_blank">{!! $company->company_name !!}</a>. {!! Lang::get('lang.all_rights_reserved') !!}. 


                <?php
                 $Whitelabel = \Event::fire('Whitelabel.faveo', array());
                 ?>
                 @if (count($Whitelabel) !=0) 
                  {!! Lang::get('lang.all_rights_reserved') !!}. {!! Lang::get('lang.powered_by') !!} <a href="#" target="_blank">Helpdesk</a>
                 @else
                 {!! Lang::get('lang.all_rights_reserved') !!}. {!! Lang::get('lang.powered_by') !!} <a href="http://www.faveohelpdesk.com/" target="_blank">Faveo</a>
        
                 @endif

                            <!-- {!! Lang::get('lang.powered_by') !!} <a href="http://www.faveohelpdesk.com/"  target="_blank">Faveo</a></p> -->
                        </div>
                        <div class="site-social text-right col-md-6">
<?php $socials = App\Model\helpdesk\Theme\Widgets::all(); ?>
                            <ul class="list-inline hidden-print">
                                @foreach($socials as $social)
                                @if($social->name == 'facebook')
                                @if($social->value)
                                <li><a href="{!! $social->value !!}" class="btn btn-social btn-facebook" target="_blank"><i class="fa fa-facebook fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "twitter")
                                @if($social->value)
                                <li><a href="{{ $social->value }}" class="btn btn-social btn-twitter" target="_blank"><i class="fa fa-twitter fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "google")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-google-plus" target="_blank"><i class="fa fa-google-plus fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "linkedin")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-linkedin" target="_blank"><i class="fa fa-linkedin fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "vimeo")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-vimeo" target="_blank"><i class="fa fa-vimeo-square fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "youtube")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-youtube" target="_blank"><i class="fa fa-youtube-play fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "pinterest")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-pinterest" target="_blank"><i class="fa fa-pinterest fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "dribbble")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-dribbble" target="_blank"><i class="fa fa-dribbble fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "flickr")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-flickr" target="_blank"><i class="fa fa-flickr fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "instagram")
                                @if($social->value)
                                <li><a href="{{$social->value }}" class="btn btn-social btn-instagram" target="_blank"><i class="fa fa-instagram fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "rss")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-rss" target="_blank"><i class="fa fa-rss fa-fw"></i></a></li>
                                @endif
                                @endif

                                @if($social->name == "skype")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-facebook" target="_blank"><i class="fa fa-skype fa-fw"></i></a></li>
                                @endif
                                @endif
                                @if($social->name == "deviantart")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-facebook" target="_blank"><i class="fa fa-deviantart fa-fw"></i></a></li>
                                @endif
                                @endif
                                 @if($social->name == "stumble")
                                @if($social->value)
                                <li><a href="{{$social->value}}" class="btn btn-social btn-danger" target="_blank"><i class="fa fa-stumbleupon fa-fw"></i></a></li>
                                @endif
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
            </footer><!-- #colophon -->
            <!-- jQuery 2.1.1 -->

<footer class="footer_menu">
	<div class="container-fluid">
	 		<ul class="footer_menu_list">
		
				<li><a href="#">Privacy & Security</a></li>
				<li><a href="#">Terms & Conditions</a></li>
				<li><a href="#">Accessibility</a></li>
				<li><a href="#">Customer Charter</a></li>
				<li><a href="#">Polls</a></li>
				<li><a href="#">Feedback</a></li>
				<li><a href="#">Contact Us</a></li>
				<li><a href="#">Newsletter</a></li>
				<li><a href="#">Sitemap </a></li>
				<li><a href="#">Faq</a></li>
				
			</ul>
	
		    <div class="reserved">
			 <p>All Rights Reserved | Copyright © ARC 2020</p>
			 <p>Version 5.3 | Last Updated 21/06/2020 | Visits: 100,000</p>
			 <p> Best viewed in 1280X800 resolution with <a href="#">Firefox25.0+</a> <a href="#">Safari5.0+</a> <a href="#">IE10.0+</a> <a href="#">Chrome31.0+</a> <a href="#">Adobe Flash Adobe Pdf</a></p>
			</div>
		 
		
		<div class="row">
	      
		   
			 <div class="social_icon">
			 	<ul class="social_list">
			 		<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			 		<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			 		<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
			 	</ul>
			 
			 <div class="app_store">
			 	 <ul>
			 	 	<li><a href="#"><img src="images/google_play.jpg" alt="image"></li>
			 	 	<li><a href="#"><img src="images/app_store.jpg" alt="image"></li>
			 	 </ul>
			 </div>
			</div>
		 
		</div><!-- end row -->
		
	</div><!-- end container -->
</footer><!-- end  section -->			
			
			
			
            <script src="{{asset("lb-faveo/js/jquery2.1.1.min.js")}}" type="text/javascript"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script src="{{asset("lb-faveo/js/bootstrap.min.js")}}" type="text/javascript"></script>
            <!-- Slimscroll -->
            <script src="{{asset("lb-faveo/js/superfish.js")}}" type="text/javascript"></script>
            
            <script src="{{asset("lb-faveo/js/mobilemenu.js")}}" type="text/javascript"></script>
            
            <script src="{{asset("lb-faveo/js/know.js")}}" type="text/javascript"></script>
            
            <script src="{{asset("lb-faveo/js/jquery.rating.pack.js")}}" type="text/javascript"></script>
            
            <script src="{{asset("lb-faveo/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}" type="text/javascript"></script>
            
            <script src="{{asset("lb-faveo/plugins/iCheck/icheck.min.js")}}" type="text/javascript"></script>
            
            <script>
$(function () {
//Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $("input[type='checkbox']", ".mailbox-messages").iCheck("uncheck");
        } else {
            //Check all checkboxes
            $("input[type='checkbox']", ".mailbox-messages").iCheck("check");
        }
        $(this).data("clicks", !clicks);
    });
//Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function (e) {
        e.preventDefault();
//detect type
        var $this = $(this).find("a > i");
        var glyph = $this.hasClass("glyphicon");
        var fa = $this.hasClass("fa");
//Switch states
        if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
        }
        if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
        }
    });
});
            </script>
           <script type="text/javascript">
                function changeLang(lang) {
                    var link = "{{url('swtich-language')}}/"+lang;
                    window.location = link;
                }
            </script>
<script src="{{asset("lb-faveo/js/angular/angular.min.js")}}" type="text/javascript"></script>
 <script src="{{asset("lb-faveo/js/angular/ng-scrollable.min.js")}}" type="text/javascript"></script>
<script src="{{asset("lb-faveo/js/angular/angular-moment.min.js")}}" type="text/javascript"></script>
<script src="{{asset('lb-faveo/js/angular/ng-flow-standalone.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/fusty-flow.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/fusty-flow-factory.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/ng-file-upload.js')}}"></script>
<script src="{{asset('lb-faveo/js/angular/ng-file-upload-shim.min.js')}}"></script>
 


<script>
              var app=angular.module('clientApp',['ngFileUpload']);
            </script>
            <script type="text/javascript">
 $(function () {
       if('{{Lang::getLocale()}}'=='ar'){
        // $('.container').attr('dir','RTL');
// site-logo text-center
      // 
         $('#navbar').css('margin-right','200px');
         $('.pull-right').toggleClass("pull-left");
          
         $('.hfeed').find('.text-center').addClass("pull-right");
         $('.nav-tabs li,.form-border,.navbar-nav li,.navbar-nav,.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,.col-xs-6').css('float','right');
         $('#content').attr('dir','RTL');
         $(".mailbox-name a").attr('dir','RTL');
         $('.box-header').attr('dir','RTL');
         $('#colophon').attr('dir','rtl');
         // input searchbox
          $('.input-lg').attr('dir','RTL');

          // breadcrumb 
        $('.breadcrumb').attr('dir','RTL');
// site-hero clearfix
        // $('.container').find('.pull-right').removeClass("pull-right");
        $('.container').find('.site-logo').addClass("pull-right");
$('#hello').css('text-align','center');
       };
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        
        if($('.alert-success').html()){
              
                setInterval(function(){
                    $('.alert-success').slideUp( 3000, function() {});
                }, 2000);
            }
        if($('.alert-danger').html()){
              
                setInterval(function(){
                    $('.alert-danger').slideUp( 3000, function() {});
                }, 2000);
            }
        
    })
</script>









<script>
              var app=angular.module('clientApp',['ngFileUpload']);
            </script>
            <script type="text/javascript">
 $(function () {
    //    if('{{Lang::getLocale()}}'=='ar'){
    //     $('.container').attr('dir','RTL');
    //     $('.formbilder').attr('dir','RTL');
    //     $('.content-area').attr('dir','RTL');
        
    //     setTimeout(function(){  
    //     $('#cke_details').addClass( "cke_rtl" );
    //     $(".cke_wysiwyg_frame").contents().find("body").attr('dir','RTL');
    //     }, 3000);
    //     // $('#wys-form').remove();create-form1
    //     // $('#mobile-RTL').css('display','block');
    //     // $('#mobile-normal').css('display','none');
    //     // $('#form-foot1').css('display','block');
    //     // $('.list-inline').attr('dir','RTL');
    //    };
    });
</script>
            </script>
      @stack('scripts')    
    </body>
</html>