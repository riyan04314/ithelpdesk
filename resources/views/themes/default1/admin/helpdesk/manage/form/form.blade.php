@extends('themes.default1.admin.layout.admin')
<style>
   input[type="radio"]:focus, input[type="checkbox"]:focus {
       outline: none !important;
     }
</style>
@section('Manage')
active
@stop

@section('manage-bar')
active
@stop

@section('forms')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('lang.forms') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')
        <link href="{{asset('lb-faveo/js/form/angular-ui-tree.css')}}" rel="stylesheet" type="text/css" > 
        <link href="{{asset('lb-faveo/js/form/app.css')}}" rel="stylesheet" type="text/css" >
  
<style>
     .tree-node{
        margin:10px;
     }
     .angular-ui-tree-empty {
    border: 1px solid gainsboro;
    min-height: 500px;
    height: 100%;
    background-color: white;
    background-image: none;
  }
  .list-group-item{
    background-color: transparent;
  }
  .list-group{
    border: 1px solid #ddd;
  }
  .behave{
    border: none;
  }
  .list-inline > li {
    padding: 0px;
  }
  .sticky {
      position: fixed;
      top: 50px;
      z-index: 100;
      -webkit-box-shadow: 0 2px 6px rgba(63,63,63,0.1);
      box-shadow: 0 2px 6px rgba(63,63,63,0.1);
      
  }
  .drg-content{
      min-height: 45px !important;
  }
  .glypic-li{
      margin-top: 4px !important;
      cursor: move;
  }
  .no-click {
     pointer-events: none;
}
.notific{
     padding: 19px !important;
  }
.panel-collapse>.list-group .list-group-item:first-child {border-top-right-radius: 0;border-top-left-radius: 0;}
.panel-collapse>.list-group .list-group-item {border-width: 1px 0;}
.panel-collapse>.list-group {margin-bottom: 0;}
.panel-collapse .list-group-item {border-radius:0;}
  </style>

<div class="well" style="display:none;"></div>

<div class="box" ng-controller="CloningCtrl">
   
    <div class="box-body with-border">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check-circle"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif
        @if(Session::has('warn'))
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('warn')}}
        </div>
        @endif
        <!--Code-->
            
  <!-- Nested node template -->
<script type="text/ng-template" id="nodes_renderer1.html">
  <div ui-tree-handle class="tree-node tree-node-content" style="margin:0px">
    <sapn>
      <i class="fa fa-check-square" aria-hidden="true" ng-if="node.type=='checkbox'"></i>
      <i class="fa fa-dot-circle-o" aria-hidden="true" ng-if="node.type=='radio'"></i>
      <i class="fa fa-list-alt" aria-hidden="true" ng-if="node.type=='select'"></i>
      <i class="fa fa-code-fork" aria-hidden="true" ng-if="node.title=='Api'"></i>
      <i class="fa fa-paragraph" aria-hidden="true" ng-if="node.type=='textarea'"></i>
      <i class="fa fa-sort-numeric-asc" aria-hidden="true" ng-if="node.type=='number'"></i>
      <i class="fa fa-calendar" aria-hidden="true" ng-if="node.type=='date'"></i>
      <i class="fa fa-text-width" aria-hidden="true" ng-if="node.type=='text' && node.title !='Api'"></i>
    </sapn>
    <span>@{{node.title}}</span>
  </div> 
  <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}" class="list-inline">
    <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer1.html'">
    </li>
  </ol>
</script>
<script type="text/ng-template" id="nodes_renderer2.html">
  <div class="tree-node">
    <div class="pull-left tree-handle drg-content" ui-tree-handle>
      <i class="fa fa-list glypic-li" aria-hidden="true"></i>
    </div>
    <div class="tree-node-content dropdown">
       <div  data-toggle="collapse">
       <label>@{{node.title}}</label>
      
      <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="remove(this)" ng-show="node.default=='no'" style="padding: 3px 5px;padding-bottom: 5px;"><span class="glyphicon glyphicon-remove"> </span></a>
      <a class="pull-right btn btn-info btn-xs collapser"  data-toggle="collapse" aria-controls="collapseExample" style="padding: 3px 5px;padding-bottom: 5px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
      </div>
      <div class="panel-collapse collapse" id="collapseExample" style="margin-top: 10px;background-color: white">
        <ul class="list-group" >
           <li class="list-group-item row behave" ng-if="node.title=='Requester'" style="margin-left: 0px;margin-right: 0px">
             <div class="col-sm-9">
                  <h3>Behaviour</h3>
             </div>
             <div class="col-sm-6">
                <h4>For Agent</h4>
                <div class="col-sm-1" style="padding-right: 0px;padding-left: 0px;width: 30px"><input type="checkbox" class="form-control" ng-model="node.agentCCfield" style="margin-top: -6px;width: 65%;"></div>
                <div class="col-sm-11" style="padding-left: 0px"><p>Display CC Field</p></div>
             </div>
             <div class="col-sm-6">
                <h4>For Customers</h4>
             </div>
          </li>
          <li class="list-group-item row behave" ng-if="node.title!='Requester'" style="margin-left: 0px;margin-right: 0px">
             <div class="col-sm-9"><h3>Behaviour</h3></div>
             <div class="col-sm-6">
                <h4>For Agent</h4>
                <div class="col-sm-1" style="padding-right: 0px;padding-left: 0px"><input type="checkbox" class="form-control" ng-model="node.agentRequiredFormSubmit" style="margin-top: -6px;width: 55%;"></div>
                <div class="col-sm-11" style="padding-left: 0px"><p>Required when submitting the form</p></div>
             </div>
             <div class="col-sm-6">
                <h4>For Customers</h4>
                <div class="col-sm-1"  style="padding-right: 0px;padding-left: 0px"><input type="checkbox" class="form-control" ng-model="node.customerDisplay" style="margin-top: -6px;width: 55%;"></div>
                <div class="col-sm-11" style="padding-left: 0px"><p>Display to customer</p></div>
                <div class="col-sm-1" style="padding-right: 0px;padding-left: 0px"><input type="checkbox" class="form-control" ng-model="node.customerRequiredFormSubmit" style="margin-top: -6px;width: 55%;"></div>
                <div class="col-sm-11" style="padding-left: 0px"><p>Required when submitting the form</p></div>
             </div>
          </li>
          <li class="list-group-item row behave" ng-if="node.agentlabel!='undefined'&&node.clientlabel!='undefiened'" style="margin-left: 0px;margin-right: 0px;">
            <h4 style="margin-left: 15px">Label</h4>
            <div class="col-sm-6" style="padding-left: 0px">
              <div class="col-sm-12" ng-repeat="agent in node.agentlabel" style="padding: 0px">
                  <div class="col-sm-9"><input type="text" name="" class="form-control" ng-model="agent.label" style="border-radius: 0px"></div>
                   <div  class="col-sm-3" style="padding: 0px">
                           
                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"  title="Choose Language"><img ng-src="@{{agent.flag}}"><input type="hidden" ng-model="agent.language">&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach($langs as $key => $value)
                                            <?php $src = $key.".png"; ?>
                                            <li><a href="#" id="{{$key}}" ng-click="changeFlang(this,$event,$index,'agent')" ><img src="{{asset("lb-faveo/flags/$src")}}">&nbsp;{{$value}}</a></li>
                                @endforeach       
                            </ul>
                            <span ng-show="$last"><a href="javascript:void(0)" ng-click="addAgentLabel(this,node.agentlabel[0].label)" title="Add Your Language" style="font-size: 19px"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></span>
                            <span ng-show="!$last"><a href="javascript:void(0)" ng-click="removeAgentLabel(this,$index)" title="remove language" style="font-size: 19px;color:#dd4b39"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span>
                   </div>
                </div>
             </div>
             <div class="col-sm-6" style="padding-left: 0px">
                <div class="col-sm-12" ng-repeat="client in node.clientlabel" style="padding: 0px">
                   <div class="col-sm-8"><input type="text" name="" class="form-control" ng-model="client.label" style="border-radius: 0px"></div>
                   <div  class="col-sm-3" style="padding: 0px">
                           
                         <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"  title="Choose Language"><img ng-src="@{{client.flag}}"><input type="hidden" ng-model="client.language">&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach($langs as $key => $value)
                                            <?php $src = $key.".png"; ?>
                                            <li><a href="#" id="{{$key}}" ng-click="changeFlang(this,$event,$index,'client')"><img src="{{asset("lb-faveo/flags/$src")}}">&nbsp;{{$value}}</a></li>
                                @endforeach       
                            </ul>
                            <span ng-show="$last"><a href="javascript:void(0)" ng-click="addclientLabel(this,node.clientlabel[0].label)" title="Add Your Language" style="font-size: 19px"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></span>
                            <span ng-show="!$last"><a href="javascript:void(0)" ng-click="removeclientLabel(this,$index)" title="remove language" style="font-size: 19px;color:#dd4b39"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span>
                   </div>
                  </div>
             </div>
          </li>

<!--           <li class="list-group-item row" ng-show="node.placeholder==''||node.placeholder!=null" style="margin-left: 0px;margin-right: 0px">
              <div class="col-sm-3" style="line-height: 2.5"><label>Placeholder</label></div>
             <div class="col-sm-9"><input type="text" class="form-control" name="" ng-model="node.placeholder" style="border-radius:0px"></div>
          </li>
          <li class="list-group-item row" ng-show="node.name==''||node.name!=null&&node.title!='Requester'" style="margin-left: 0px;margin-right: 0px">
              <div class="col-sm-3" style="line-height: 2.5"><label>Name</label></div>
             <div class="col-sm-9"><input type="text" class="form-control" name="" ng-model="node.name" style="border-radius: 0px"></div>
          </li> -->
          <li class="list-group-item row behave" ng-if="node.title=='Api'" style="margin-left: 0px;margin-right: 0px">
              <div class="col-sm-1" style="line-height: 2.5"><label>Api Link</label></div>
             <div class="col-sm-11"><input type="text" class="form-control" name="" ng-model="node.api" style="border-radius: 0px"></div>
             <div class="col-sm-1" style="line-height: 2.5"><label>Format</label></div>
             <div class="col-sm-11">@{{node.format}}-->Ex:[{'id':'1','optionvalue':'Your Options-1'},{'id':'2','optionvalue':'Your Options-2'}]</div>
          </li>
           <li class="list-group-item row behave" ng-if="node.type=='radio'&& node.default=='no' || node.type=='select'&& node.default=='no' || node.type=='checkbox' && node.default=='no'"><h4 style="margin-left: 27px">Dropdown Items</h4></li>
           <li class="list-group-item row behave"   style="margin-left: 0px;margin-right: 0px" ng-repeat="option in node.options" ng-if="node.type=='radio'&& node.default=='no' || node.type=='select'&& node.default=='no' || node.type=='checkbox' && node.default=='no'">
             <div><label style="margin-left: 15px">Option@{{$index+1}}</label></div>
             <div class="col-sm-7" ng-repeat="opt in option.optionvalue" style="padding: 0px">
              
             <div class="col-sm-8" style="width: 63%"><input type="text" class="form-control" name="" ng-model="opt.option" style="border-radius: 0px">
             </div>
             <div class="col-sm-2" style="padding: 0px">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"  
                    title="Choose Language"><img ng-src=@{{opt.flag}}><input type="hidden" ng-model="agent.language">&nbsp;<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach($langs as $key => $value)
                                            <?php $src = $key.".png"; ?>
                                            <li><a href="#" id="{{$key}}" ng-click="changeFlang(this,$event,$index,$parent.$index)" ><img src="{{asset("lb-faveo/flags/$src")}}">&nbsp;{{$value}}</a></li>
                                @endforeach       
                            </ul>
                   <span ng-show="$last"><a href="javascript:void(0)" ng-click="addLangOpt(this,$parent.$index)" title="Add Your Language" style="font-size: 19px"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></span>
                   <span ng-show="!$last"><a href="javascript:void(0)" ng-click="removeLangOpt(this,$parent.$index,$index)" title="remove language" style="font-size: 19px;color:#dd4b39"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span>
              </div>
              </div>
             <div class="col-sm-5">
                  <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="removeOption(this,$index)" title="remove Option@{{$index+1}}" style="padding: 3px 5px">
                      <span class="glyphicon glyphicon-remove"> </span>
                  </a>
                  <div class="dropdown" ng-show="node.title=='Nested Select'||node.title=='Nested Radio' ||node.title=='Nested Checkbox'">
                      <a class="pull-right btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" data-nodrag  style="margin-right: 8px;" title="Add Child Form">
                         <span class="glyphicon glyphicon-plus"></span> Add Child
                      </a>
                      <ul class="dropdown-menu">
                          <li ng-repeat="Ifield in tree1"><a href="javascript:void(0)" ng-click="newSubForm(this,Ifield.title,$parent.$index)">@{{Ifield.title}}</a></li>
                      </ul>
                  </div>
             </div>
            <ol class="col-sm-12" ui-tree-nodes="" ng-model="option.nodes" ng-class="{hidden: collapsed}" style="padding-left: 0px">
                <li ng-repeat="node in option.nodes" ui-tree-node ng-include="'nodes_renderer2.html'"></li>
            </ol> 
          </li>
          <li class="list-group-item row behave"   style="margin-left: 0px;margin-right: 0px"  ng-if="node.value=='Status'">
              <div class="col-sm-5">
                    <label>For Agents</label>
                </div>
              <div class="col-sm-5">
                    <label>For Customers</label>
              </div>
              
           </li>
           <li class="list-group-item row behave"   style="margin-left: 15px" ng-if="node.title=='Nested Select'||node.title=='Nested Radio'||node.title=='Nested Checkbox'">
                 <input type="button" name="addOption" class="btn btn-default" value="Add Option" ng-click="addOption(this,node.type)">
          </li>
          <li class="list-group-item row behave"   style="margin-left: 15px" ng-if="node.title!='Nested Radio' && node.type=='radio'||node.title!='Nested Checkbox' && node.type=='checkbox'||node.type=='select' && node.title!='Nested Select'&& node.default=='no'">
                 <input type="button" name="addOption" class="btn btn-default" value="Add Option" ng-click="addTypeOption(this,node.type)">
          </li>
       </ul>
      </div>
    </div>
  </div>
  <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
    <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer2.html'">
    </li>
  </ol>
</script>
<div  class="col-sm-12" style="background-color: white;margin: -10px;width: 102%;padding-left: 0px;padding-right: 0px" id="category">
    <div class="col-sm-6">
    <h3>Ticket Fields(Drag Here)</h3>
    </div>
    <div class="col-sm-6" style="margin-top: 14px">

{!!Form::button('<i class="fa fa-floppy-o" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.save'),['type' => 'submit', 'class' =>'btn btn-primary btn-sm','ng-click'=>'editFormValue($event)','style'=>'float: right;margin-right:30px','id'=>'save'])!!}

    </div>
    <div ui-tree="treeOptions" id="tree1-root" data-clone-enabled="true" data-nodrop-enabled="true" class="col-sm-12">
      <ol ui-tree-nodes="" ng-model="tree1" class="list-inline" style="margin-bottom: 15px;">
        <li ng-repeat="node in tree1" ui-tree-node ng-include="'nodes_renderer1.html'" ng-click="addToTree(node)"></li>
      </ol>
    </div>
</div>
<div class="row">
<div class="col-sm-12" id="content1" style="padding-left:8px">
    <h3 style="padding-left: 6px">Ticket Fields(Drop Here)</h3>
    <div ui-tree id="tree2-root" data-clone-enabled="true" id="drp">
      <ol ui-tree-nodes="" ng-model="tree2" id="drp">
        <li ng-repeat="node in tree2" ui-tree-node ng-include="'nodes_renderer2.html'">
         
        </li>
      </ol>
    </div>
  </div>
</div>
        
        <!--end Code-->
    </div>
</div>
@stop

@push('scripts')



<script>
$(function () {
   $('body').on('click', '[data-toggle=collapse]', function (e) {
        
       $(this).next().collapse('toggle');
       console.log($(this).next());
});

});

$(document).ready(function() {
var stickyNavTop = parseInt($('#category').offset().top)-100;

var stickyNav = function(){
var scrollTop = $(window).scrollTop();

if (scrollTop > stickyNavTop) {
    var width=$('#category').outerWidth(); 
    $('#category').addClass('sticky');
    $('#category').css('width',width);
} else {
    $('#category').removeClass('sticky'); 
}
};
 
stickyNav();
 
$(window).scroll(function() {
  stickyNav();
});
});

</script>

<script>
(function () {
  'use strict';
  app.controller('CloningCtrl', ['$scope','$http', function ($scope,$http) {
     $http.get("{{url('form/ticket')}}").success(function(data){
         $scope.tree2 = data[0];
         console.log($scope.tree2);
        setTimeout(function(){
         $(document).find('.toolbtn').tooltip();
        },200)
     })
    $scope.treeOptions = {
       dropped: function(e) {
        var sourceValue = e.source.cloneModel;
             randomString(16, 'abcdefghijklmnopqrstuvwxyz');
            
            function randomString(length, chars) {
                var result = '';
                for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
                        sourceValue['unique']=result;
                  }
         }
      };
     $scope.node1=['submit','button'];
      $scope.remove = function (scope) {
        scope.remove();
      };

      $scope.toggle = function (scope) {
        scope.toggle();
      };
      $scope.removeAgentLabel=function(scope,x){
            var nodedata=scope.$modelValue.agentlabel;
            nodedata.splice(x,1);
      }
      $scope.removeclientLabel=function(scope,x){
             var nodedata=scope.$modelValue.clientlabel;
             nodedata.splice(x,1);
      }
      $scope.removeLangOpt=function(scope,x,y){
           var nodedata=scope.$modelValue.options[x].optionvalue;
             nodedata.splice(y,1);
      }
      $scope.addAgentLabel=function(scope,label){
          var nodedata=scope.$modelValue.agentlabel;
          nodedata.push({'language':'en','label':label,'flag':'{{asset("lb-faveo/flags/en.png")}}'})
          //console.log(nodedata);
      }
      $scope.addclientLabel=function(scope,label){
        //console.log(label);
          var nodedata=scope.$modelValue.clientlabel;
          nodedata.push({'language':'en','label':label,'flag':'{{asset("lb-faveo/flags/en.png")}}'})
      }
      $scope.addLangOpt=function(scope,x){
          var nodedata=scope.$modelValue.options[x].optionvalue;
          nodedata.push({'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'})
      }
      $scope.changeFlang=function(scope,event,index,user){
        var src=event.currentTarget.childNodes[0].currentSrc;
        var id=event.currentTarget.id;
        console.log(index,user);
       $(event.currentTarget.parentNode.parentNode.previousElementSibling.childNodes[0]).attr('src',src);

       if(user=='agent'){
          var nodedata=scope.$modelValue.agentlabel[index];
          nodedata.language=id; 
          nodedata.flag=src;
       }
       else if(user=='client'){
           var nodedata=scope.$modelValue.clientlabel[index];
           nodedata.language=id;
           nodedata.flag=src;  
       }
       else{
            var nodedata=scope.$modelValue.options[user].optionvalue[index];
            nodedata.language=id; 
            nodedata.flag=src;
       }

      }
      $scope.addOption = function (scope,x) {
          var nodeData = scope.$modelValue.options;
          var index=scope.$modelValue.options.length;
          if(x=='checkbox'){
             nodeData.push({
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'checked':'',
              'nodes':[]
           })

          }
          else if(x=='select'){
            nodeData.push({
              'id':index+1,
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'nodes':[]
           })
          }
          else{
           nodeData.push({
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'nodes':[]
           })
         }
      };
      $scope.addTypeOption= function (scope,x) {
          var nodeData = scope.$modelValue.options;
        if(x=='checkbox'){
             nodeData.push({
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'checked':''
           })
          }
        else{
           nodeData.push({
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ]
           })
          }
      };
      
      $scope.removeOption = function (scope,y) {
          var nodeData = scope.$modelValue.options;
           nodeData.splice(y,1);
      };
      $scope.addToTree = function (scope) {
          $scope.tree2.push(scope);
      };
      $scope.editFormValue=function(x){
          $('#save').attr('disabled','disabled');
           x.currentTarget.innerHTML = "<i class='fa fa-circle-o-notch fa-spin fa-1x fa-fw'></i>Saving...";
          console.log($scope.tree2);
          $http.post("{{url('forms')}}",$scope.tree2).success(function(data){
        $('.well').css('display','block');      
        $('.well').html(data);
         x.currentTarget.innerHTML = "Saved"
        $('.well').css('color','#fff');
        $('.well').css('background-color','#5cb85c');
        $('html, body').animate({scrollTop:0}, 500);
         setTimeout(function(){
             location.reload();
         },2000);      
          }).error(function(data){
              $('.well').css('display','block');      
              $('.well').html(data);
              $('.well').css('color','#fff');
              $('.well').css('background-color','red');
          })
       

       }
       
      $scope.newSubForm=function(scope,x,y){

        var nodeData = scope.$modelValue.options[y];
       
       if(x=='Nested Select'){
        nodeData.nodes.push({
           'title': 'Nested Select',
           'agentlabel': [
                 {'language':'en','label':'Nested Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
           'clientlabel': [
                 {'language':'en','label':'Nested Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
           'type': 'select',
           'placeholder':'',
           'name':'',
           'value':'',
           'agentRequiredFormSubmit':true,
           'customerDisplay':true,
           'customerRequiredFormSubmit':false,
           'options':[
              {
                 'id':1,
                 'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
                 'nodes':[]
              }
            ],
            'default':'no'
          });
       }
       else if(x=='Nested Radio'){
        nodeData.nodes.push({
         'title': 'Nested Radio',
        'agentlabel': [
                 {'language':'en','label':'Nested Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel': [
                 {'language':'en','label':'Nested Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'placeholder':'',
        'name':'',
        'value':'',
        'type':'radio',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'nodes':[]
           }
        ],
        'default':'no'
          });
       }
       else if(x=='Nested Checkbox'){
        nodeData.nodes.push({
          'title': 'Nested Checkbox',
          'agentlabel': [
                 {'language':'en','label':'Nested Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
          'clientlabel': [
                 {'language':'en','label':'Nested Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
          'type':'checkbox',
          'placeholder':'',
          'name':'',
          'agentRequiredFormSubmit':true,
          'customerDisplay':true,
          'customerRequiredFormSubmit':false,
          'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'checked':'',
              'nodes':[]
           }
        ],
          'default':'no'
         });
      }
       else if(x=='Text Field'){
        nodeData.nodes.push({
        'title': 'Text Field',
        'agentlabel':[
                 {'language':'en','label':'Text Field','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Text Field','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'text',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      });
       }
       else if(x=='Text Area'){
        nodeData.nodes.push({
        'title': 'Text Area',
        'agentlabel':[
                 {'language':'en','label':'Text Area','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Text Area','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'textarea',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      });
       }
       else if(x=='Number'){
        nodeData.nodes.push({
        'title': 'Number',
        'agentlabel':[
                 {'language':'en','label':'Number','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Number','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'number',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      });
       }
       else if(x=='Select'){
        nodeData.nodes.push({
        'title': 'Select',
        'agentlabel':[
                 {'language':'en','label':'Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'select',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
           }
        ],
        'default':'no'
      });
       }
       else if(x=='Radio'){
        nodeData.nodes.push({
        'title': 'Radio',
        'agentlabel':[
                 {'language':'en','label':'Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'radio',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
           }
        ],
        'default':'no'
      });
       }
       else if(x=='Checkbox'){
        nodeData.nodes.push({
        'title': 'Checkbox',
        'agentlabel':[
                 {'language':'en','label':'Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'checkbox',
        'name':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'checked':''
           }
        ],
        'default':'no'
      });
       }
       else if(x=='Date'){
        nodeData.nodes.push({
        'title': 'Date',
        'agentlabel':[
                 {'language':'en','label':'Date','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Date','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'date',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      });
       }

        else if(x=='Api'){
        nodeData.nodes.push({
        'title': 'Api',
        'agentlabel':[
                 {'language':'en','label':'Api','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Api','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],

        'type':'text',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no',
        'api':'',
        'format':'Json'
      });
     }



      };


      $scope.tree1 = [{
        'title': 'Text Field',
        'agentlabel':[
                 {'language':'en','label':'Text Field','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Text Field','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'text',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      }, {
        'title': 'Text Area',
        'agentlabel':[
                 {'language':'en','label':'Text Area','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Text Area','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'textarea',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      },{
        'title': 'Number',
        'agentlabel':[
                 {'language':'en','label':'Number','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Number','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'number',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      },{
        'title': 'Nested Select',
        'agentlabel':[
                 {'language':'en','label':'Nested Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Nested Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'select',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'id':1,
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'nodes':[]
           }
        ],
        'default':'no'
      },{
        'title': 'Nested Radio',
        'agentlabel':[
                 {'language':'en','label':'Nested Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Nested Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'radio',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'nodes':[]
           }
        ],
        'default':'no'
      },{
        'title': 'Nested Checkbox',
        'agentlabel':[
                 {'language':'en','label':'Nested Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Nested Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'checkbox',
        'placeholder':'',
        'name':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'checked':'',
              'nodes':[]
           }
        ],
        'default':'no'
      },{
        'title': 'Select',
        'agentlabel':[
                 {'language':'en','label':'Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Select','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'select',
        'placeholder':'',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           { 
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ]
           }
        ],
        'default':'no'
      },
      {
        'title': 'Radio',
        'agentlabel':[
                 {'language':'en','label':'Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Radio','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'radio',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ]
           }
        ],
        'default':'no'
      }, {
        'title': 'Checkbox',
        'agentlabel':[
                 {'language':'en','label':'Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Checkbox','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'checkbox',
        'name':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'options':[
           {
              'optionvalue':[
                 {'language':'en','option':'value','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
              'checked':''
           }
        ],
        'default':'no'
      },{
        'title': 'Date',
        'agentlabel':[
                 {'language':'en','label':'Date','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Date','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'date',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no'
      },
      {
        'title': 'Api',
        'agentlabel':[
                 {'language':'en','label':'Api','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Api','flag':'{{asset("lb-faveo/flags/en.png")}}'}
                ],
        'type':'text',
        'name':'',
        'value':'',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'no',
        'api':'',
        'format':'Json'
      }
      ];
      
    }]);

   
})();

</script>

@endpush