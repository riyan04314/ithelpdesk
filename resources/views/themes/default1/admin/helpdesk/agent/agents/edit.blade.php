@extends('themes.default1.admin.layout.admin')
<link href="{{asset("lb-faveo/js/intlTelInput.css")}}" rel="stylesheet" type="text/css">
<style type="text/css" media="screen">
     .permission-menu{
        width: 300px !important;
   }  
.permisson-drop:hover, .permisson-drop:active, .permisson-drop.hover {
    background-color: #e7e7e7 !important;

}
.permisson-drop {
    background-color: #f4f4f4;
    color: #444;
    border-color: #ddd;
    width: 90%;
}
.permisson-drop {
    -webkit-box-shadow: none;
    box-shadow: none;
    border: 1px solid transparent;
}
.permisson-drop {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}
  .notific {
       height: 50px;
}
.faveo-choice{
 background-color: #e4e4e4;
 border: 1px solid #aaa;
border-radius: 4px;
padding: 3px;
cursor: default;
margin: 5px;
display: table;
}
.label-warning{
    right: 6px !important;
}
/* span{
    white-space: nowrap;
} */
</style>

@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('agents')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{{Lang::get('lang.staffs')}}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">

</ol>
@stop
<!-- /breadcrumbs -->
<!-- content -->
@section('content')

<!-- open a form -->

{!! Form::model($user, ['url' => 'agents/'.$user->id,'method' => 'PATCH','id'=>'Form'] )!!}

<!-- <section class="content"> -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{!! Lang::get('lang.edit_an_agent') !!}</h3>  
    </div>
    <div class="box-body">
        @if(Session::has('errors'))
        <?php //dd($errors); ?>
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>Alert!</b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <br/>
            @if($errors->first('user_name'))
            <li class="error-message-padding">{!! $errors->first('user_name', ':message') !!}</li>
            @endif
            @if($errors->first('first_name'))
            <li class="error-message-padding">{!! $errors->first('first_name', ':message') !!}</li>
            @endif
            @if($errors->first('last_name'))
            <li class="error-message-padding">{!! $errors->first('last_name', ':message') !!}</li>
            @endif
            @if($errors->first('email'))
            <li class="error-message-padding">{!! $errors->first('email', ':message') !!}</li>
            @endif
            @if($errors->first('ext'))
            <li class="error-message-padding">{!! $errors->first('ext', ':message') !!}</li>
            @endif
            @if($errors->first('phone_number'))
            <li class="error-message-padding">{!! $errors->first('phone_number', ':message') !!}</li>
            @endif
            @if($errors->first('mobile'))
            <li class="error-message-padding">{!! $errors->first('mobile', ':message') !!}</li>
            @endif
            @if($errors->first('active'))
            <li class="error-message-padding">{!! $errors->first('active', ':message') !!}</li>
            @endif
            @if($errors->first('role'))
            <li class="error-message-padding">{!! $errors->first('role', ':message') !!}</li>
            @endif
            @if($errors->first('group'))
            <li class="error-message-padding">{!! $errors->first('group', ':message') !!}</li>
            @endif
            @if($errors->first('primary_department'))
            <li class="error-message-padding">{!! $errors->first('primary_department', ':message') !!}</li>
            @endif
            @if($errors->first('agent_time_zone'))
            <li class="error-message-padding">{!! $errors->first('agent_time_zone', ':message') !!}</li>
            @endif
            @if($errors->first('team'))
            <li class="error-message-padding">{!! $errors->first('team', ':message') !!}</li>
            @endif 
        </div>
        @endif
        @if(Session::has('fails2'))
            <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>Alert!</b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <br/>
                <li class="error-message-padding">{!! Session::get('fails2') !!}</li>
            </div>
        @endif
        <div class="row">
            <!-- Email -->
            <div class="col-xs-6 form-group {{ $errors->has('email') ? 'has-error' : '' }}">

                {!! Form::label('email',Lang::get('lang.email_address')) !!} <span class="text-red"> *</span>

                {!! Form::email('email',null,['class' => 'form-control']) !!}

            </div>
            <!-- username -->
            <div class="col-xs-6 form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">

                {!! Form::label('user_name',Lang::get('lang.user_name')) !!} <span class="text-red"> *</span>
                 {!!Form::hidden('role',$user->role)!!}
                {!! Form::text('user_name',null,['class' => 'form-control', 'pattern' => '^[a-zA-Z0-9-_\.]{1,20}$|[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$', 'title' => Lang::get('lang.username_pattern_warning')]) !!}

            </div>
        </div>
        <div class="row">
            <!-- firstname -->
            <div class="col-xs-6 form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">

                {!! Form::label('first_name',Lang::get('lang.first_name')) !!} <span class="text-red"> *</span>

                {!! Form::text('first_name',null,['class' => 'form-control']) !!}

            </div>

            <!-- Lastname -->
            <div class="col-xs-6 form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">

                {!! Form::label('last_name',Lang::get('lang.last_name')) !!}

                {!! Form::text('last_name',null,['class' => 'form-control']) !!}

            </div>
        </div>

        <div class="row">
            <!-- phone -->
            
            <div class="col-xs-1 form-group {{ $errors->has('ext') ? 'has-error' : '' }}">

                <label for="ext">{!! Lang::get('lang.ext') !!}</label>    
                {!! Form::text('ext',null,['class' => 'form-control numberOnly']) !!}


            </div>
<div class="col-xs-5 form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">

                {!! Form::label('phone_number',Lang::get('lang.phone')) !!}
                {!! Form::text('phone_number',null,['class' => 'form-control numberOnly']) !!}


            </div>
            {!! Form::hidden('country_code', null) !!}            
            <!-- Mobile -->
            <div class="col-xs-6 form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
            <?php
            $mobile_number = '';
            if (is_numeric($user->mobile)) {
                $mobile_number = $user->mobile;
            }
            ?>
                {!! Form::label('mobile',Lang::get('lang.mobile_number')) !!}@if($aoption == 'mobile' || $aoption == 'email,mobile') <span class="text-red"> *</span> @endif
                {!! Form::input('text', 'mobile', $mobile_number,['class' => 'form-control numberOnly', "id" => 'mobile']) !!}

            </div>

        </div>

        <div>
            <h4>{{Lang::get('lang.account_status_setting')}}</h4>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <!-- acccount type -->
                <div class="form-group {{ $errors->has('active') ? 'has-error' : '' }}">

                    {!! Form::label('active',Lang::get('lang.status')) !!}

                    <div class="row">
                        <div class="col-xs-3">
                            {!! Form::radio('active','1',true) !!} {{ Lang::get('lang.active') }}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::radio('active','0',null) !!} {{Lang::get('lang.inactive')}}
                        </div>
                    </div>

                </div>
                <!-- role -->
                <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">

                    {!! Form::label('role',Lang::get('lang.role')) !!}

                    <div class="row">
                        <div class="col-xs-3">
                            {!! Form::radio('role','admin',true) !!} {{Lang::get('lang.admin')}}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::radio('role','agent',null) !!} {{Lang::get('lang.agent')}}
                        </div>
                    </div>
                </div>

            </div>
            <!-- day light saving -->
            {{-- <div class="col-xs-6"> --}}

            {{-- <div> --}}
            {{-- <div class="row"> --}}
            {{-- {!! Form::label('',Lang::get('lang.day_light_saving')) !!} --}}
            {{-- <div class="col-xs-2"> --}}
            {{-- {!! Form::checkbox('daylight_save',1,null,['class' => 'checkbox']) !!} --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}

            <!-- limit access -->
            {{-- <div > --}}
            {{-- <div class="row"> --}}
            {{-- {!! Form::label('limit_access',Lang::get('lang.limit_access')) !!} --}}
            {{-- <div class="col-xs-2"> --}}
            {{-- {!! Form::checkbox('limit_access',1,null,['class' => 'checkbox']) !!} --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}

            <!-- directory listing -->
            {{-- <div> --}}
            {{-- <div class="row"> --}}
            {{-- {!! Form::label('directory_listing',Lang::get('lang.directory_listing')) !!} --}}
            {{-- <div class="col-xs-2"> --}}
            {{-- {!! Form::checkbox('directory_listing',1,null,['class' => 'checkbox']) !!} --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}

            <!-- vocation mode -->
            {{-- <div> --}}
            {{-- <div class="row"> --}}
            {{-- {!! Form::label('vocation_mode',Lang::get('lang.vocation_mode')) !!} --}}
            {{-- <div class="col-xs-2"> --}}
            {{-- {!! Form::checkbox('vocation_mode',1,null,null,['class' => 'checkbox']) !!} --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
        </div>
        <div class="row">
            <!-- assigned group -->
            <div class="col-xs-4 form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                {!! Form::label('assign_group', Lang::get('lang.assigned_group')) !!}
                
                          <div class="dropdown">
                       <button class="permisson-drop" type="button" id="menu1" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus">&nbsp</i>Add Permissions</button>
                         <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Select Permission</h4>
                                    </div>
                                    <div class="modal-body">
                         <ul class="list-group" role="menu" aria-labelledby="menu1">
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('create_ticket','Create Ticket')" style="display:block;padding: 1em">
                                    <span id="create_ticket" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="create_ticket2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Create Ticket</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('edit_ticket','Edit Ticket')" style="display:block;padding: 1em">
                                    <span id="edit_ticket" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="edit_ticket2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Edit Ticket</span></a>
                            </li>
                            
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('close_ticket','Close Ticket')" style="display:block;padding: 1em">
                                    <span id="close_ticket" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="close_ticket2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Close Ticket</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('transfer_ticket','Transfer Ticket')" style="display:  block;padding: 1em">
                                    <span id="transfer_ticket" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="transfer_ticket2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Transfer Ticket</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('delete_ticket','Delete Ticket')" style="display: block;padding: 1em">
                                    <span id="delete_ticket" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="delete_ticket2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Delete Ticket</span></a>
                            </li>
                            
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('assign_ticket','Assign Ticket')" style="display:block;padding: 1em">
                                    <span id="assign_ticket" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="assign_ticket2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Assign Ticket</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('access_kb','Access Kb')" style="display:block;padding: 1em">
                                    <span id="access_kb" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="access_kb2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Access Kb</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('report','Report')" style="display:block;padding: 1em">
                                    <span id="report" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12" style="fill:cornflowerblue"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path>
                                    </svg>
                                    <input  type="hidden"></span>
                                    <span id="report2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Report</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('ban_email','Ban Emails')" style="display: block;padding: 1em">
                                    <span id="ban_email" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span><span id="ban_email2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Ban Email</span></a>
                            </li>
                            <li role="presentation" class="list-group-item" style="border:none;padding: 0px">
                                <a role="menuitem" tabindex="-1" href="javascript:void(0)" onclick="permissionSelect('organisation_document_upload','Organisation Document Upload')" style="display:block;padding: 1em">
                                    <span id="organisation_document_upload" style="display: none"><svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z" style="fill:cornflowerblue"></path>
                                    </svg>
                                    <input  type="hidden"></span><span id="organisation_document_upload2" style="border:1px solid cornflowerblue;padding-left: 4px;padding-right: 13px;"></span>&nbsp
                                    <span>Organisation Document Upload</span></a>
                            </li>
                        </ul>    
                                    </div>
                                    <div class="modal-footer">
                                           <button type="button" class="btn btn-close" data-dismiss="modal">close</button>
                                    </div>
                                </div>
                            </div>
                         </div>
                         
                </div>
                <div id="view-permission" style="width: 90%;border:1px solid gainsboro;height: 200px;overflow: auto;"></div>
            </div>

            <!-- primary department -->
            <div class="col-xs-4 form-group {{ $errors->has('primary_department') ? 'has-error' : '' }}">
                {!! Form::label('primary_dpt', Lang::get('lang.primary_department')) !!} <span class="text-red"> *</span>
 



    {!! Form::select('primary_department[]', [Lang::get('lang.departments')=>$departments->pluck('name','id')->toArray()],$dept,['multiple'=>true,'class'=>"form-control select2" ,'id'=>"primary_department"]) !!}
            </div>

            <!-- agent timezone -->
            <div class="col-xs-4 form-group {{ $errors->has('agent_time_zone') ? 'has-error' : '' }}">
                {!! Form::label('agent_tzone', Lang::get('lang.agent_time_zone')) !!} <span class="text-red"> *</span>

                {!!Form::select('agent_time_zone', [''=>Lang::get('lang.select_a_time_zone'), Lang::get('lang.time_zones')=>$timezones->pluck('name','id')->toArray()],$user->agent_tzone,['class' => 'form-control select']) !!}
            </div>
        </div>

        <!-- team -->
        <div class="{{ $errors->has('team') ? 'has-error' : '' }}">
            {!! Form::label('agent_tzone',Lang::get('lang.assigned_team')) !!} 
        </div>
        @while (list($key, $val) = each($teams))
        <div class="form-group ">
            <input type="checkbox" name="team[]" value="<?php echo $val; ?> " <?php
            if (in_array($val, $assign)) {
                echo ('checked');
            }
            ?> > &nbsp;<?php echo "  " . $key; ?><br/>
        </div>
        @endwhile
        <div class="row">
            <!-- Agent signature -->
            <div class="col-xs-12">

                <h4>{{Lang::get('lang.agent_signature')}}</h4>

            </div>

            <div class="col-xs-12">

                {!! Form::textarea('agent_sign',null,['class' => 'form-control','size' => '30x5','id'=>'ckeditor']) !!}

            </div>
        </div>
    </div>
    <div class="box-footer">
<!--  {!! Form::submit(Lang::get('lang.update'),['class'=>'form-group btn btn-primary'])!!}-->
<!--  {!!Form::button('<i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.update'),['type' => 'submit', 'class' =>'btn btn-primary'])!!}-->
      <button type="submit" id="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-refresh fa-spin fa-1x fa-fw'>&nbsp;</i> updating..."><i class="fa fa-refresh">&nbsp;&nbsp;</i>{!!Lang::get('lang.update')!!}</button>    

    
    </div>
</div>
{!!Form::close()!!}
<!-- <script type="text/javascript">
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();
        $('.select2-selection').css('border-radius','0px')
$('.select2-container').children().css('border-radius','0px')
    });
    </script> -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{asset("lb-faveo/js/intlTelInput.js")}}"></script>
<script type="text/javascript">
    var telInput = $('#mobile');
    telInput.intlTelInput({
        geoIpLookup: function(callback) {
            $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        initialCountry: "{!! $country_code !!}",
        separateDialCode: true,
        utilsScript: "{{asset('lb-faveo/js/utils.js')}}",
        formatOnDisplay : false,
    });
    $('.intl-tel-input').css('width', '100%');

    telInput.on('blur', function() {
        if ($.trim(telInput.val())) {
            if (!telInput.intlTelInput("isValidNumber")) {
                telInput.parent().addClass('has-error');
            }
        }
    });
    $('input').on('focus', function() {
        $(this).parent().removeClass('has-error');
    });

    $('form').on('submit', function(e){
        $('input[name=country_code]').attr('value', $('.selected-dial-code').text());
    });
</script>
    <script>
        $('#primary_department').select2({
            placeholder: "{{Lang::get('lang.Choose_departments...')}}",
            minimumInputLength: 2,
            ajax: {
                // URL: '/agent/dept/find',
                   url: '{{route("agent.dept.search")}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    </script>
<script>
$(document).ready(function() {
    $(".numberOnly").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
     var permision = {!!$user->permision()->pluck("permision")!!};
     console.log(permision);
     $.each(permision[0], function(key, value) {
        if(key=="create_ticket"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Create Ticket\")'>x</span>Create Ticket</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="edit_ticket"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Edit Ticket\")'>x</span>Edit Ticket</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         
         else if(key=="close_ticket"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Close Ticket\")'>x</span>Close Ticket</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="delete_ticket"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Delete Ticket\")'>x</span>Delete Ticket</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="assign_ticket"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Assign Ticket\")'>x</span>Assign Ticket</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="ban_email"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Ban Email\")'>x</span>Ban Email</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="access_kb"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Access Kb\")'>x</span>Access Kb</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="report"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Report\")'>x</span>Report</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="transfer_ticket"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Transfer Ticket\")'>x</span>Transfer Ticket</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
         else if(key=="organisation_document_upload"){
          $('#'+key+2).css('display','none');
          $('#'+key).css('display','inline-block');
          $('#view-permission').append("<span class='faveo-choice' id='"+key+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+key+"\",\"Organisation Document Upload\")'>x</span>Organisation Document Upload</span>");
          $('#'+key).find('input').attr('name','permission['+key+']');
          $('#'+key).find('input').attr('value','1');
         }
     }); 

function permissionSelect(x,y){
    $('#submit').removeAttr('disabled');
    if($('#'+x).css('display')=='none'){
        $('#'+x+2).css('display','none');
        $('#'+x).css('display','inline-block');
        $('#'+x).find('input').attr('name','permission['+x+']');
        $('#'+x).find('input').attr('value',"1");
        $('#view-permission').append("<span class='faveo-choice' id='"+x+"1'><span   tabindex='-1' style='cursor: pointer;padding-left: 3px;padding-right: 3px;background-color: gainsboro;color: #a79e9e;margin-right: 3px' onclick='permissionSelect(\""+x+"\",\""+y+"\")'>x</span>"+y+"</span>")
        
    }
    else{
        $('#'+x+2).css('display','initial');
        $('#'+x).css('display','none');
        $('#'+x).find('input').removeAttr('name');
        $('#view-permission').find('#'+x+'1').remove();
    }
     
  }
  function closeDropdown(){
      $('.permission-menu').toggle();
  }

  $(document).click(function(event) {
     if($('.permission-menu').css('display')=='block'){
         $('.permission-menu').hide();
    }
  });
$('.permission-menu').click(function(event){
     event.stopPropagation();
 });
</script>
    <script type="text/javascript">
    $(function() {
        $('.select2-selection--multiple').css('border-radius','0px');
        $('.select2-selection__rendered').css('margin-bottom','-7px');
    });
    </script>
@stop