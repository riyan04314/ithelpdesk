@extends('themes.default1.agent.layout.agent')
<link href="{{asset("lb-faveo/js/intlTelInput.css")}}" rel="stylesheet" type="text/css">
@section('Dashboard')
class="active"
@stop

@section('dashboard-bar')
active
@stop

@section('profile')
class="active"
@stop

@section('PageHeader')
<h1>{{Lang::get('lang.edit-profile')}}</h1>
@stop

@section('content')
@if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
<div class="row">
    <div class="col-md-6">
        {!! Form::model($user,['url'=>'agent-profile', 'id' => 'agent-profile', 'method' => 'PATCH','files'=>true]) !!}
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {!! Lang::get('lang.profile') !!}
                </h3>
            </div>
            <div class="box-body">
                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>Success</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('success')}}
                </div>
                @endif
                <!-- fail message -->
                @if(Session::has('fails'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>Fail!</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('fails')}}
                </div>
                @endif
                
                <!-- first name -->
                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                    {!! Form::label('first_name',Lang::get('lang.first_name')) !!} <span class="text-red"> *</span>
                    {!! Form::text('first_name',null,['class' => 'form-control']) !!}
                </div>
                <!-- last name -->
                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                    {!! Form::label('last_name',Lang::get('lang.last_name')) !!}
                    {!! Form::text('last_name',null,['class' => 'form-control']) !!}
                </div>
                <!-- gender -->
                <div class="form-group">
                    {!! Form::label('gender',Lang::get('lang.gender')) !!}
                    <div class="row">
                        <div class="col-xs-3">
                            {!! Form::radio('gender','1',true) !!} {{Lang::get('lang.male')}}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::radio('gender','0') !!} {{Lang::get('lang.female')}}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <!-- email address -->
                    {!! Form::label('email',Lang::get('lang.email_address')) !!}
                    <div>
                        {{$user->email}}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                    <!-- company -->
                    {!! Form::label('company',Lang::get('lang.company')) !!}
                    {!! Form::text('company',null,['class' => 'form-control']) !!}
                </div>
                <div class="row">
                    <!-- phone number -->
                     <div class="col-xs-2 form-group {{ $errors->has('ext') ? 'has-error' : '' }}">
                        {!! Form::label('ext',Lang::get('lang.ext')) !!}
                        {!! Form::input('text', 'ext', $user->ext, ["class" => "form-control numberOnly"]) !!}
                    </div>
                    <div class="col-xs-10 form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
                        {!! Form::label('phone_number',Lang::get('lang.phone')) !!}
                        {!! Form::text('phone_number',$user->phone_number,['class' => 'form-control numberOnly']) !!}
                    </div>
                   
                    {!! Form::hidden('country_code', null, ['id' => 'code']) !!}
                </div>
                <!-- mobile -->
                <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
            <?php
            $number = '';
            if (is_numeric($user->mobile)) {
                $number = $user->mobile;
            }
            ?>
                    {!! Form::label('mobile',Lang::get('lang.mobile_number')) !!}
                    @if($aoption == 'mobile' || $aoption == "email,mobile") <span class="text-red"> *</span> @endif
                    {!! Form::input('text', 'mobile', $number, ["class" => "form-control numberOnly", "id" => "mobile"]) !!}
                </div>
                <div class="form-group {{ $errors->has('agent_sign') ? 'has-error' : '' }}">
                    {!! Form::label('agent_sign',Lang::get('lang.agent_sign')) !!}
                    {!! Form::textarea('agent_sign',null,['class' => 'form-control','id'=>'ckeditor']) !!}
                </div>
                <div class="form-group {{ $errors->has('profile_pic') ? 'has-error' : '' }}">
                    <!-- profile pic -->
                    <!-- <div  class="btn btn-default btn-file" style="color:orange"> -->
                        <i class="fa fa-user"> </i>
                        {!! Form::label('profile_pic',Lang::get('lang.profile_pic')) !!}

                        <input type="file" name="profile_pic"class="btn btn-default btn-file" style="color:orange;display:inline-block">
                      
                    <!-- </div>   -->
                    @if(Auth::user()->profile_pic)
                      <img src="{{Auth::user()->profile_pic}}" class="img-rounded" alt="Cinque Terre" width="40" />
                      @endif
                </div>


             


              

                @if (view()->exists("assign::setting"))
                <div class="form-group {{ $errors->has('not_accept_ticket') ? 'has-error' : '' }}">
                    {!! Form::label('not_accept_ticket','I can not accept tickets') !!}
                    <div class="row">
                        <div class="col-xs-4">
                            {!! Form::radio('not_accept_ticket','1',true) !!} {{Lang::get('lang.active')}}
                        </div>
                        <div class="col-xs-6">
                            {!! Form::radio('not_accept_ticket','0') !!} {{Lang::get('lang.inactive')}}
                        </div>
                    </div>
                </div>
                @endif
                {!! Form::token() !!}
                {!! Form::close() !!}
            </div>
            <div class="box-footer">
<!--                {!! Form::submit(Lang::get('lang.update'),['class'=>'form-group btn btn-primary'])!!}-->
                    {!!Form::button('<i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.update'),['type' => 'submit', 'class' =>'form-group btn btn-primary'])!!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        {!! Form::model($user,['url'=>'agent-profile-password/'.$user->id , 'method' => 'PATCH']) !!}
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">{!! Lang::get('lang.change_password') !!}</h4> 
            </div>
            <div class="box-body">
                @if(Session::has('success1'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>Success</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('success1')}}
                </div>
                @endif
                <!-- fail message -->
                @if(Session::has('fails1'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <b>Fail!</b>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('fails1')}}
                </div>
                @endif
                <!-- old password -->
                <div class="form-group has-feedback {{ $errors->has('old_password') ? 'has-error' : '' }}">
                    {!! Form::label('old_password',Lang::get('lang.old_password')) !!} <span class="text-red"> *</span>
                    {!! Form::password('old_password',['class' => 'form-control']) !!}
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <!-- new password -->
                <div class="form-group has-feedback {{ $errors->has('new_password') ? 'has-error' : '' }}">
                    {!! Form::label('new_password',Lang::get('lang.new_password')) !!} <span class="text-red"> *</span>
                    {!! Form::password('new_password',['class' => 'form-control']) !!}
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <!-- confirm password -->
                <div class="form-group has-feedback {{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                    {!! Form::label('confirm_password',Lang::get('lang.confirm_password')) !!} <span class="text-red"> *</span>
                    {!! Form::password('confirm_password',['class' => 'form-control']) !!}
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
            </div>
            <div class="box-footer">
<!--                {!! Form::submit(Lang::get('lang.update'),['class'=>'form-group btn btn-primary'])!!}-->
             {!!Form::button('<i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.update'),['type' => 'submit', 'class' =>'form-group btn btn-primary'])!!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@if($aoption == 'mobile' || $aoption == "email,mobile")
<!-- Modal for last step of setting -->
<div class="modal fade in" id="last-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none; padding-right: 15px;background-color: rgba(0, 0, 0, 0.7);">
    <div class="modal-dialog" role="document">
        <div class="col-md-2"></div>
        <div class="col-md-12" style="height:40%">
            <div class="modal-content">
                <div class="modal-header">
                    <span style="font-size:1.2em">{{Lang::get('lang.verify-number')}}</span> 
                    <button type="button" class="close closemodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div id="custom-alert-body2">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="loader2" style="display:none">
                                    <center><img src="{{asset('lb-faveo/media/images/gifloader.gif')}}"></center>
                                </div>
                                <div id="verify-success" style="display:none" class="alert alert-success alert-dismissable">
                                    <i class="fa  fa-check-circle"> </i>
                                    <span id = "success_message"></span>
                                </div>
                                <div id="verify-fail" style="display:none" class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"> </i> <b> {!! Lang::get('lang.alert') !!}! </b>
                                    <span id = "error_message"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="verify-number-form">
                        {!! Form::open(['id'=>'verify-otp','method' => 'POST'] )!!}
                        <div class="row">
                            <div class="col-md-8">
                                {{ Lang::get('lang.get-verify-message') }}
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('token','',['class' => 'form-control', 'required' => true, 'placeholder' => Lang::get('lang.enter-otp'), 'id' => 'otp']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-last" class="btn btn-default closemodal pull-left">{{Lang::get('lang.close')}}</button>
                    <div id="last-submit"><input  type="submit" id="merge-btn" class="btn btn-primary pull-right" value="{!! Lang::get('lang.verify') !!}"></input></div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- modal end -->
@endif
<script src="{{asset("lb-faveo/js/intlTelInput.js")}}"></script>
<script>
    $(document).ready(function() {
        $(".numberOnly").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
    $(function () {
        $("textarea").wysihtml5();
    });
    
    var telInput = $('#mobile');
    telInput.intlTelInput({
        geoIpLookup: function(callback) {
            $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        initialCountry: "{!! $country_code !!}",
        separateDialCode: true,
        utilsScript: "{{asset('lb-faveo/js/utils.js')}}",
        formatOnDisplay : false,
    });
    $('.intl-tel-input').css('width', '100%');

    telInput.on('blur', function() {
        if ($.trim(telInput.val())) {
            if (!telInput.intlTelInput("isValidNumber")) {
                telInput.parent().addClass('has-error');
            }
        }
    });

    $('input').on('focus', function() {
        $(this).parent().removeClass('has-error');
    });

    $('form').on('submit', function(e){
        $('input[name=country_code]').attr('value', $('.selected-dial-code').text());
    });
</script>
@if($aoption == 'mobile' || $aoption == "email,mobile")
<script type="text/javascript">
    $('#agent-profile').on('submit', function (e) {
        var old_mobile = "<?php echo $user->mobile; ?>";
        var email = "<?php echo $user->email; ?>";
        var full_name = "<?php echo $user->first_name; ?>";
        var mobile = document.getElementById('mobile').value;
        var code = document.getElementById('code').value;
        if (code == '' || code == null) {
            //do nothing
        } else {
            var id = "<?php echo $user->id; ?>";
            if (mobile !== old_mobile) {
                e.preventDefault();
                $('#last-modal').css('display', 'block');
                $.ajax({
                    url: '{{URL::route("resend-otp")}}',
                    type: 'post', // performing a POST request
                    data: {
                        "mobile" : mobile,
                        "user_id" : id,
                        "code" : code,
                        "_token": "{{ csrf_token() }}",
                    },
                    dataType: 'html',
                    beforeSend: function () {
                        $('#loader2').css('display', 'block');
                        $('#verify-number-form').css('display', 'none');
                        $('#verify-fail').css('display', 'none');
                        $('verify-success').css('display', 'none');
                    },
                    success: function (response) {
                        $('#loader2').css('display', 'none');
                        var response = JSON.parse(response);
                        if (response == 1) {
                            $('#verify-number-form').css('display', 'block');
                            $('#verify-otp').on('submit', function (e) {
                                e.preventDefault();
                                var otp = document.getElementById('otp').value;
                                $.ajax({
                                    url: '{{URL::route("post-agent-verify-number")}}',
                                    type: 'POST',
                                    data: {
                                        otp: otp,
                                        u_id: id,
                                        "_token": "{{ csrf_token() }}",
                                    },
                                    dataType: 'html',
                                    beforeSend: function () {
                                        $('#loader2').css('display', 'block');
                                        $('#verify-number-form').css('display', 'none');
                                        $('#verify-fail').css('display', 'none');
                                        $("#merge-btn").prop('disabled', true);
                                        $('verify-success').css('display', 'none');
                                    },
                                    success: function (response) {
                                        $("#merge-btn").prop('disabled', false);
                                        if (response == 1) {
                                            $('#loader2').css('display', 'none');
                                            var message = "{{Lang::get('lang.number-verification-sussessfull')}}";
                                            $('#success_message').html(message);
                                            $('#verify-success').css('display', 'block');
                                            $('#agent-profile').unbind('submit').submit();
                                        } else {
                                            $('#loader2').css('display', 'none');
                                            $("#error_message").html(response);
                                            $('#verify-fail').css('display', 'block');
                                            $('#verify-number-form').css('display', 'block');
                                        }
                                    }, 
                                    error: function(response) {
                                        $("#merge-btn").prop('disabled', false);
                                        $('#loader2').css('display', 'none');
                                        // $("#error_message").html(response);
                                        $('#verify-fail').css('display', 'block');
                                        $('#verify-number-form').css('display', 'block');
                                    }
                                });
                            });
                        } else {
                            $("#error_message").html(response.error);
                            $("#merge-btn").css('display', 'none');
                            $('#verify-fail').css('display', 'block');
                        }

                    },
                    complete: function (jqXHR, textStatus) {
                        if (textStatus === "parsererror" || textStatus === "timeout" || textStatus === "abort" || textStatus === "error") {
                            var message = "{{Lang::get('lang.otp-not-sent')}}";
                            $('#loader2').css('display', 'none');
                            $("#error_message").html(message);
                            $("#merge-btn").css('display', 'none');
                            $('#verify-fail').css('display', 'block');
                        }
                    }
                });
            }
        }
    });
    $('.closemodal').on('click', function () {
        $('#last-modal').css('display', 'none');
    });
</script>
@endif
@stop