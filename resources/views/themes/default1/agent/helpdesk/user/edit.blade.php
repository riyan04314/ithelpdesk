@extends('themes.default1.agent.layout.agent')
<link href="{{asset("lb-faveo/js/intlTelInput.css")}}" rel="stylesheet" type="text/css">

@section('Users')
class="active"
@stop

@section('user-bar')
active
@stop

@section('user')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('lang.edit_user') !!}</h1>
@stop
<!-- /header -->

<!-- content -->
@section('content')
<!-- check whether success or not -->
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa  fa-check-circle"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('success')}}
    </div>
@endif
@if(Session::has('fails'))
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <b>{!! Lang::get('lang.alert') !!}!</b>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{Session::get('fails')}}
</div>
@endif
<!-- open a form -->
{!! Form::model($users,['url'=>'user/'.$users->id,'method'=>'PATCH','id'=>'Form']) !!}
{!!Form::hidden('role',$users->role)!!}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            {!! Lang::get('lang.user_credentials') !!}
        </h3>
    </div>
    <div class="box-body">
        @if(Session::has('errors'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{!! Lang::get('lang.alert') !!}!</b>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <br/>
            @if($errors->first('first_name'))
            <li class="error-message-padding">{!! $errors->first('first_name', ':message') !!}</li>
            @endif
            @if($errors->first('last_name'))
            <li class="error-message-padding">{!! $errors->first('last_name', ':message') !!}</li>
            @endif
            @if($errors->first('user_name'))
            <li class="error-message-padding">{!! $errors->first('user_name', ':message') !!}</li>
            @endif
            @if($errors->first('email'))
            <li class="error-message-padding">{!! $errors->first('email', ':message') !!}</li>
            @endif
            @if($errors->first('mobile'))
            <li class="error-message-padding">{!! $errors->first('mobile', ':message') !!}</li>
            @endif
            @if($errors->first('ext'))
            <li class="error-message-padding">{!! $errors->first('ext', ':message') !!}</li>
            @endif
            @if($errors->first('phone_number'))
            <li class="error-message-padding">{!! $errors->first('phone_number', ':message') !!}</li>
            @endif
            @if($errors->first('active'))
            <li class="error-message-padding">{!! $errors->first('active', ':message') !!}</li>
            @endif
        </div>
        @endif
        <div class="row">
<?php
if (strpos($users->email, '@')) {
    $email = $users->email;
} else {
    $email = "";
}
?>

            <!-- Email Address : Email : Required -->
            <div class="col-xs-6 form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('email',Lang::get('lang.email')) !!}
                @if ($email_mandatory->status == 1 || $email_mandatory->status == '1')
                <span class="text-red"> *</span>
                @endif
                {!! Form::email('email',$email,['class' => 'form-control']) !!}
            </div>
            <!-- User Name : Text : Required-->
            <div class="col-xs-6 form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                {!! Form::label('user_name',Lang::get('lang.user_name')) !!}<span class="text-red"> *</span>
                {!! Form::text('user_name',null,['class' => 'form-control', 'pattern' => '^[a-zA-Z0-9-_\.]{1,20}$|[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$', 'title' => Lang::get('lang.username_pattern_warning')]) !!}
            </div>
        </div>
        <div class="row">
            <!-- First name : first name : Required -->
            <div class="col-xs-6 form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                {!! Form::label('first_name',Lang::get('lang.first_name')) !!}<span class="text-red"> *</span>
                {!! Form::text('first_name',null,['class' => 'form-control']) !!}
            </div>
            <!-- Last name : last name : Required -->
            <div class="col-xs-6 form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                {!! Form::label('last_name',Lang::get('lang.last_name')) !!}
                {!! Form::text('last_name',null,['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 form-group {{ $errors->has('organization') ? 'has-error' : '' }}">
                {!! Form::label('organization',Lang::get('lang.organization')) !!}


                {!! Form::select('org_id', [Lang::get('lang.organization')=>$orgs->pluck('name','id')->toArray()],$organization_id,['multiple'=>true,'class'=>"form-control select2" ,'id'=>"org_id"]) !!}

             

            </div>
            {!! Form::hidden('country_code', null) !!}

            <!-- mobile Number : Text :  -->
            <div class="col-md-6 form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
            <?php
            $mobile_number = '';
            if (is_numeric($users->mobile)) {
                $mobile_number = $users->mobile;
            }
            ?>
                {!! Form::label('mobile',Lang::get('lang.mobile')) !!}
                @if($aoption == "mobile" || $aoption == "email,mobile") <span class="text-red"> *</span> @endif
                {!! Form::input('text', 'mobile', $mobile_number,['class' => 'form-control numberOnly', 'id' => 'mobile']) !!}
            </div>
        </div>
        <div class="row">     
            <div class="col-xs-1 form-group {{ $errors->has('ext') ? 'has-error' : '' }}">
                <label for="ext">{!! Lang::get('lang.ext') !!}</label>
                {!! Form::text('ext',null,['class' => 'form-control numberOnly']) !!}
            </div>
            <div class="col-xs-5 form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
                <label for="phone_number">{!! Lang::get('lang.phone') !!}</label>
                {!! Form::input('text', 'phone_number',null,['class' => 'form-control numberOnly']) !!}
               
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 form-group {{ $errors->has('active') ? 'has-error' : '' }}">
                {!! Form::label('active',Lang::get('lang.status')) !!}
                <div class="row">
                    <div class="col-xs-4">
                        {!! Form::radio('active','1',true) !!} {{Lang::get('lang.active')}}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::radio('active','0') !!} {{Lang::get('lang.inactive')}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 form-group {{ $errors->has('ban') ? 'has-error' : '' }}">
                {!! Form::label('ban',Lang::get('lang.ban')) !!}
                <div class="row">
                    <div class="col-xs-4">
                        {!! Form::radio('ban','1',true) !!} {{Lang::get('lang.enable')}}
                    </div>
                    <div class="col-xs-6">
                        {!! Form::radio('ban','0') !!} {{Lang::get('lang.disable')}}
                    </div>
                </div>
            </div>

        </div>
        <!-- Internal Notes : Textarea -->
        <div class="form-group">
            {!! Form::label('internal_note',Lang::get('lang.internal_notes')) !!}
            {!! Form::textarea('internal_note',null,['class' => 'form-control', 'size' => '30x5','id'=>'ckeditor']) !!}
        </div>
    </div>
    <div class="box-footer">
        <!--        {!! Form::submit(Lang::get('lang.update'),['class'=>'form-group btn btn-primary'])!!}-->
        {!!Form::button('<i class="fa fa-refresh" aria-hidden="true">&nbsp;&nbsp;</i>'.Lang::get('lang.update'),['type' => 'submit', 'class' =>'btn btn-primary','id'=>'submit'])!!}


    </div>        
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
    $('#org_id').select2({

        // alert('ogkkggk');
        placeholder: "{{Lang::get('lang.Choose_organization...')}}",
        maximumSelectionLength: 1,
        minimumInputLength: 2,
        ajax: {
            url: '{{route("org.name.search")}}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

</script>
<script>
    $(document).ready(function () {
        $(".numberOnly").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl/cmd+A
                            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            // Allow: Ctrl/cmd+C
                                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                                    // Allow: Ctrl/cmd+X
                                            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                                            // Allow: home, end, left, right
                                                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                                        // let it happen, don't do anything
                                        return;
                                    }
                                    // Ensure that it is a number and stop the keypress
                                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                        e.preventDefault();
                                    }
                                });
                    });
            $(function () {
                $("textarea").wysihtml5();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue'
                });
                $('input[type="radio"]').iCheck({
                    radioClass: 'iradio_flat-blue'
                });

            });
</script>



<script type="text/javascript">
            // / autocomplete Department name
            $(document).ready(function () {
                $("#org_id").autocomplete({
                    source: "{!!URL::route('post.organization.autofill')!!}",
                    minLength: 1,
                    select: function (evt, ui) {


                    }
                });
            });
</script>
<!--submit script-->

<script>
            $('#Form').submit(function () {
                var btn = $('#submit');
                btn[0].innerHTML = "<i class='fa fa-refresh fa-spin fa-1x fa-fw'></i>Updating...";
                $('#submit').attr('disabled', 'disabled');
            });
            $('#submit').attr('disabled', 'disabled');
            $('#Form').on('input', function () {
                $('#submit').removeAttr('disabled');
            });
            $('#Form').on('change', ':input', function () {
                $('#submit').removeAttr('disabled');
            });
</script>

@stop
@section('FooterInclude')
<script src="{{asset("lb-faveo/js/intlTelInput.js")}}"></script>
<script type="text/javascript">
    var country_code = "{!! $country_code !!}";
    var telInput = $('#mobile');
    telInput.intlTelInput({
        geoIpLookup: function(callback) {
            $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        initialCountry: country_code,
        separateDialCode: true,
        utilsScript: "{{asset('lb-faveo/js/utils.js')}}",
        formatOnDisplay : false,
    });
    $('.intl-tel-input').css('width', '100%');

    telInput.on('blur', function() {
        if ($.trim(telInput.val())) {
            if (!telInput.intlTelInput("isValidNumber")) {
                telInput.parent().addClass('has-error');
            }
        }
    });
    $('input').on('focus', function() {
        $(this).parent().removeClass('has-error');
    });

    $('form').on('submit', function(e){
        $('input[name=country_code]').attr('value', $('.selected-dial-code').text());
    });
</script>
<!--<script>
    $('#org').autocomplete({
        minLength: 1,
        source: function (request, response) {
            $.getJSON("{{url('get-organization')}}", {
                term: request.term
            }, function (data) {
                var array = data.error ? [] : $.map(data, function (m) {
                    return {
                        label: m.label,
                        value: m.value
                    };
                });
                response(array);
            });
        },
        select: function (event, ui) {
            $("#org").val(ui.item.label); // display the selected text
            $("#field_id").val(ui.item.value); // save selected id to hidden input
            return false;
        }
    });

</script>-->
<script type="text/javascript">
            $(function () {
                $('.select2-selection--multiple').css('border-radius', '0px');
                $('.select2-selection__rendered').css('margin-bottom', '-7px');
            });
</script>
@stop