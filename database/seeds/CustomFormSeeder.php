<?php

use Illuminate\Database\Seeder;

class CustomFormSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->seedForms();
        $this->seedRequired();
    }

    public function seedForms() {
        \DB::table('forms')->truncate();
        $json = "[{
        'title': 'Requester',
        'agentlabel':[
                 {'language':'en','label':'Requester','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Requester','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'email',
        'agentCCfield':true,
        'customerCCfield':false,
        'customerDisplay':true,
        'agentRequiredFormSubmit':true,
        'customerRequiredFormSubmit':true,
        'default':'yes',
        'value':'',
        'unique':'requester'
        },{
        'title': 'Subject',
        'agentlabel':[
                 {'language':'en','label':'Subject','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Subject','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'text',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':true,
        'default':'yes',
        'value':'',
        'unique':'subject'
        },{
        'title': 'Type',
        'agentlabel':[
                 {'language':'en','label':'Type','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Type','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'select',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':true,
        'value':'',
        'api':'type',
        'options':[
        ],
        'default':'yes',
        'unique':'type'
        },{
        'title': 'Status',
        'agentlabel':[
                 {'language':'en','label':'Status','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Status','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'select',
        'agentRequiredFormSubmit':true,
        'customerDisplay':false,
        'customerRequiredFormSubmit':false,
        'value':'',
        'api':'status',
        'options':[
           
        ],
        'default':'yes',
        'unique':'status'
        },{
        'title': 'Priority',
        'agentlabel':[
                 {'language':'en','label':'Priority','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Priority','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'select',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':true,
        'value':'',
        'api':'priority',
        'options':[
           
        ],
        'default':'yes',
        'unique':'priority'
        },{
        'title': 'Help Topic',
        'agentlabel':[
                 {'language':'en','label':'Help Topic','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Help Topic','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'select',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':true,
        'value':'',
        'api':'helptopic',
        'options':[
           
        ],
        'default':'yes',
        'unique':'help_topic'
        },{
        'title': 'Assigned',
        'agentlabel':[
                 {'language':'en','label':'Assigned','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Assigned','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'select',
        'agentRequiredFormSubmit':true,
        'customerDisplay':false,
        'customerRequiredFormSubmit':false,
        'value':'',
        'api':'assigned_to',
        'options':[
           
        ],
        'default':'yes',
        'unique':'assigned'
        },{
        'title': 'Description',
        'agentlabel':[
                 {'language':'en','label':'Description','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Description','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'textarea',
        'agentRequiredFormSubmit':true,
        'customerDisplay':true,
        'customerRequiredFormSubmit':false,
        'default':'yes',
        'value':'',
        'unique':'description'
        },{
        'title': 'Company',
        'agentlabel':[
                 {'language':'en','label':'Company','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'clientlabel':[
                 {'language':'en','label':'Company','flag':'".faveoUrl('lb-faveo/flags/en.png')."'}
                ],
        'type':'select',
        'agentRequiredFormSubmit':false,
        'customerDisplay':false,
        'customerRequiredFormSubmit':false,
        'default':'yes',
        'value':'',
        'api':'company',
        'options':[
           
        ],
        'unique':'company'
        }]
";
        $json = trim(preg_replace('/\s+/', ' ', $json));
        $form = "ticket";
        \DB::table('forms')->insert(['form' => $form, 'json' => $json]);
    }

    public function seedRequired() {
        \DB::table('required_fields')->truncate();
        $fields = [
            ['name' => 'Requester', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Subject', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Type', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Status', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Priority', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Group', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Agent', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Description', 'is_agent_required' => 1, 'is_client_required' => 1],
            ['name' => 'Company', 'is_agent_required' => 1, 'is_client_required' => 1],
        ];
        $form = "ticket";
        foreach ($fields as $field) {
            \DB::table('required_fields')->insert(['name' => $field['name'], 'form' => $form, 'is_agent_required' => $field['is_agent_required'], 'is_client_required' => $field['is_client_required']]);
        }
    }
    

}
