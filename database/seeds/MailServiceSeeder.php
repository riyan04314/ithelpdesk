<?php

use Illuminate\Database\Seeder;
//model
use App\Model\MailJob\MailService;

class MailServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mail = new MailService();
        $services= ['smtp'=>'SMTP','mail'=>'Php Mail','sendmail'=>'Send Mail','mailgun'=>'Mailgun','mandrill'=>'Mandrill','log'=>'Log file'];
        foreach($services as $key=>$value){
        $mail->create([
            'name'=>$value,
            'short_name'=>$key,
        ]);
        }
    }
}
