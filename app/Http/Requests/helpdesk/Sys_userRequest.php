<?php

namespace App\Http\Requests\helpdesk;

use App\Model\helpdesk\Settings\CommonSettings;
use App\Http\Requests\Request;

/**
 * Sys_userRequest.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class Sys_userRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'user_name'     => $this->getUsernameCheck(),
            'email'         => getEmailValidation(), //helper function to get email validation
            'country_code'  => 'max:5',
            'mobile'        => getMobileValidation('mobile'), //helper function to get mobile validation
            'ext'           => 'max:5',
            'phone_number'  => 'max:15',
        ];
    }

    /**
     *  @category function to return validation rule array for username
     *  @param null
     *  @return array
     */
    public function getUsernameCheck()
    {
        return  [
                    'required',
                    'min:3',
                    'regex:/^(?:[A-Z\d][A-Z\d._-]{2,30}|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i',
                    'unique:users,user_name,'.$this->segment(2)
                ];
    }
}
