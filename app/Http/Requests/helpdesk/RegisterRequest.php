<?php

namespace App\Http\Requests\helpdesk;

use App\Http\Requests\Request;

/**
 * RegisterRequest.
 *
 * @author  Ladybird <info@ithelpdesk.com>
 */
class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                 => getEmailValidation(),
            'full_name'             => 'required',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'mobile'                => getMobileValidation('mobile'),
        ];
    }
}
