<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\TemplateVariablesController;
use App\Model\Common\TemplateType;
use App\Model\helpdesk\Agent\Department;
use App\Model\helpdesk\Email\Emails;
use App\Model\helpdesk\Settings\Company;
use App\Model\helpdesk\Settings\Email;
use App\User;
use Auth;
use Mail;
use Exception;
use Lang;

class PhpMailController extends Controller
{

    public function fetch_smtp_details($id = "")
    {
        if ($id)
        {
            $emails = Emails::where('id', '=', $id)->first();
        }
        else
        {
            $emails = Emails::first();
        }
        return $emails;
    }

    /**
     * Fetching comapny name to send mail.
     *
     * @return type
     */
    public function company()
    {
        $company = Company::Where('id', '=', '1')->first();
        if ($company->company_name == null)
        {
            $company = 'Support Center';
        }
        else
        {
            $company = $company->company_name;
        }

        return $company;
    }

    /**
     * Function to choose from address.
     *
     * @param type Reg        $reg
     * @param type Department $dept_id
     *
     * @return type integer
     */
    public function mailfrom($reg, $dept_id)
    {
        $email    = Email::find(1);
        $email_id = $email->sys_email;

        $dept_email = Department::select('outgoing_email')->where('id', '=', $dept_id)->first();
        if ($dept_email)
        {
            if ($dept_email->outgoing_email != "" || $dept_email->outgoing_email != null)
            {
                $email_id = $dept_email->outgoing_email;
            }
        }
        return $email_id;
    }

    public function sendmail($from, $to, $message, $template_variables, $thread = "", $auto_respond = "")
    {
        $this->setQueue();
        $job = new \App\Jobs\SendEmail($from, $to, $message, $template_variables, $thread, $auto_respond);
        dispatch($job);
    }

    public function sendEmail($from, $to, $message, $template_variables, $thread = "", $auto_respond = "")
    {
        $email_reciever_name = "";
        $reciever            = User::where('email', '=', $to['email'])->first();
        if ($reciever)
        {
            $email_reciever_name = $reciever->name();
        }
        $template_variables['receiver_name'] = $email_reciever_name;
        $from_address                        = $this->fetch_smtp_details($from);
        if ($from_address == null)
        {
            throw new Exception("Invalid Email Configuration", 601);
        }
        $this->setMailConfig($from_address);
        $recipants     = $this->checkElement('email', $to);
        $recipantname  = $this->checkElement('name', $to);
        $recipant_role = $this->checkElement('role', $to);
        $recipant_lang = $this->checkElement('preferred_language', $to);
        $cc            = $this->checkElement('cc', $message);
        $bc            = $this->checkElement('bc', $message);
        $subject       = $this->checkElement('subject', $message);
        $template_type = $this->checkElement('scenario', $message);
        $attachment    = $this->checkElement('attachments', $message);
        if ($recipants == $this->checkElement('agent_email', $template_variables))
        {
            $assigned_agent = true;
        }
        else
        {
            $assigned_agent = false;
        }
        $content_array = $this->mailTemplate($template_type, $template_variables, $message, $from, $subject, $assigned_agent, $recipant_role, $recipant_lang);
        $content       = $this->checkElement('body', $message);
        if ($content_array)
        {
            $content = checkArray('content', $content_array);
            $subject = checkArray('subject', $content_array);
        }
        $send = $this->laravelMail($recipants, $recipantname, $subject, $content, $cc, $attachment, $thread, $auto_respond);
        $this->updateFilePermission($thread);
        return $send;
    }

    public function updateFilePermission($thread)
    {
        $attach = [];
        if ($thread && is_object($thread))
        {
            $attach = $thread->attach()
                            ->where('poster', 'ATTACHMENT')
                            ->select(\DB::raw('type as mime'), \DB::raw('name as file_name'), \DB::raw('path as file_path'), \DB::raw('path as file_path'), \DB::raw('path as file_path'), \DB::raw('file as data'), 'poster')->get()->toArray();
        }
        if ($attach && is_array($attach))
        {
            foreach ($attach as $attachment)
            {
                $mime      = checkArray('mime', $attachment);
                $file_path = checkArray('file_path', $attachment);
                $file_name = checkArray('file_name', $attachment);
                if (mime($mime) != 'image' && $file_path && $file_name)
                {
                    $file = $file_path . "/" . $file_name;
                    chmod($file, 1204);
                }
            }
        }
    }

    public function setMailConfig($from_address)
    {

        $username   = $from_address->email_address;
        $fromname   = $from_address->email_name;
        $password   = $from_address->password;
        $smtpsecure = $from_address->sending_encryption;
        $host       = $from_address->sending_host;
        $port       = $from_address->sending_port;
        $protocol   = $from_address->sending_protocol;
        $this->setServices($from_address->id, $protocol);
        if ($protocol == 'mail')
        {
            $username   = '';
            $fromname   = '';
            $host       = '';
            $smtpsecure = '';
            $port       = '';
        }
        $configs = [
            'username'   => $username,
            'from'       => ['address' => $username, 'name' => $fromname,],
            'password'   => $password,
            'encryption' => $smtpsecure,
            'host'       => $host,
            'port'       => $port,
            'driver'     => $protocol,
        ];
        foreach ($configs as $key => $config)
        {
            if (is_array($config))
            {
                foreach ($config as $from)
                {
                    \Config::set('mail.' . $key, $config);
                }
            }
            else
            {
                \Config::set('mail.' . $key, $config);
            }
        }
    }

    public function setServices($emailid, $protocol)
    {
        $service    = new \App\Model\MailJob\FaveoMail();
        $services   = $service->where('email_id', $emailid)->pluck('value', 'key')->toArray();
        $controller = new \App\Http\Controllers\Admin\helpdesk\EmailsController();
        $controller->setServiceConfig($protocol, $services);
    }

    public function checkElement($element, $array)
    {
        $value = "";
        if (is_array($array))
        {
            if (key_exists($element, $array))
            {
                $value = $array[$element];
            }
        }
        return $value;
    }

    public function getReference($thread)
    {
        $reference = "";
        if ($thread)
        {
            $emailThread = $thread->emailThread()->first();
            if ($emailThread)
            {
                $reference = $emailThread->message_id;
            }
        }
        return $reference;
    }

    public function laravelMail($to, $toname, $subject, $data, $cc = [], $attach = "", $thread = "", $auto_respond = "")
    {
        $reference = $this->getReference($thread);
        $mail      = Mail::send('emails.mail', ['data' => $data, 'thread' => $thread], function ($m) use ($to, $subject, $toname, $cc, $attach, $thread, $auto_respond, $reference)
                {
                    $m->to($to, $toname)->subject($subject);
                    $swiftMessage = $m->getSwiftMessage();
                    $headers      = $swiftMessage->getHeaders();
                    if ($auto_respond)
                    {
                        $headers->addTextHeader('X-Autoreply', 'true');
                        $headers->addTextHeader('Auto-Submitted', 'auto-replied');
                        //$headers->addTextHeader('Content-Transfer-Encoding', 'base64');
                    }
                    if ($reference)
                    {
                        $headers->addTextHeader('References', $reference);
                        $headers->addTextHeader('In-Reply-To', $reference);
                    }
                    if ($cc)
                    {
                        $m->cc($cc);
                    }
                    if ($thread && is_object($thread))
                    {
                        $attach = $thread->attach()
                                        ->where('poster', 'ATTACHMENT')
                                        ->select(\DB::raw('type as mime'), \DB::raw('name as file_name'), \DB::raw('path as file_path'), \DB::raw('path as file_path'), \DB::raw('path as file_path'), \DB::raw('file as data'), 'poster')->get()->toArray();
                    }
                    $size = count($attach);
                    if ($size > 0)
                    {
                        for ($i = 0; $i < $size; $i++)
                        {
                            if (is_array($attach) && array_key_exists($i, $attach))
                            {
                                $mode = 'normal';
                                $file = $attach[$i]['file_path'];
                                if (checkArray('poster', $attach[$i]))
                                {
                                    $file = $attach[$i]['file_path'] . DIRECTORY_SEPARATOR . $attach[$i]['file_name'];
                                }
                                if (checkArray('poster', $attach[$i]) && checkArray('data', $attach[$i]))
                                {
                                    $file = $attach[$i]['data'];
                                    $mode = 'data';
                                }
                                if (is_array($attach[$i]) && array_key_exists('mode', $attach[$i]))
                                {
                                    $mode = $attach[$i]['mode'];
                                }

                                $name = $attach[$i]['file_name'];
                                $mime = $attach[$i]['mime'];

                                $this->attachmentMode($m, $file, $name, $mime, $mode);
                            }
                        }
                    }
                    $this->saveEmailThread($swiftMessage->getId(), $thread);
                });
        if (is_object($mail) || (is_object($mail) && $mail->getStatusCode() == 200))
        {
            return 1;
        }
        return $mail;
    }

    public function saveEmailThread($msg_id, $thread)
    {
        if ($thread)
        {
            $content           = ['message_id' => $msg_id];
            $ticket_controller = new \App\Http\Controllers\Agent\helpdesk\TicketController();
            $ticket_controller->saveEmailThread($thread, $content);
        }
    }

    public function setQueue()
    {
        $short        = 'database';
        $field        = [
            'driver' => 'database',
            'table'  => 'jobs',
            'queue'  => 'default',
            'expire' => 60,
        ];
        $queue        = new \App\Model\MailJob\QueueService();
        $active_queue = $queue->where('status', 1)->first();
        if ($active_queue)
        {
            $short  = $active_queue->short_name;
            $fields = new \App\Model\MailJob\FaveoQueue();
            $field  = $fields->where('service_id', $active_queue->id)->pluck('value', 'key')->toArray();
        }
        $this->setQueueConfig($short, $field);
    }

    public function setQueueConfig($short, $field)
    {
        \Config::set('queue.default', $short);
        foreach ($field as $key => $value)
        {
            \Config::set("queue.connections.$short.$key", $value);
        }
    }

    public function attachmentMode($message, $file, $name, $mime, $mode)
    {
        chmod($file, 0755);
        if ($mode == 'data')
        {
            return $message->attachData(base64_decode($file, true), $name, ['mime' => $mime]);
        }
        return $message->attach($file, ['as' => $name, 'mime' => $mime]);
    }

    public function mailTemplate($template_type, $template_variables, $message, $from, $subject, $assigned_agent, $role, $lang)
    {
        if ($template_type == 'registration-notification' || $template_type == 'reset-password' || $template_type == 'reset_new_password' || $template_type == 'registration')
        {
            $template_category = 'common-tmeplates';
        }
        elseif ($assigned_agent)
        {
            $template_category = 'assigend-agent-templates';
        }
        elseif ($role == 'user' || $message['scenario'] == 'check-ticket' || $message['scenario'] == 'create-ticket')
        {
            $template_category = 'client-templates';
        }
        else
        {
            $template_category = 'agent-templates';
        }
        if ($lang == null)
        {
            $lang = \Config::get('app.locale');
        }
        $content       = $this->checkElement('body', $message);
        $ticket_number = $this->checkElement('ticket_number', $template_variables);
        $template      = TemplateType::where('name', '=', $template_type)->first();
        $set           = \App\Model\Common\TemplateSet::where('active', '=', 1)->where('template_language', '=', $lang)->first();
        if ($set == null)
        {
            $set = \App\Model\Common\TemplateSet::where('id', '=', 1)->first();
        }
        $temp = [];
        if ($template)
        {
            $temp                = $this->set($set->id, $ticket_number, $message, $template, $template_category);
            $contents            = $temp['content'];
            $temp_var_controller = new TemplateVariablesController;
            $variables           = $temp_var_controller->getVariableValues($template_variables, $content);
            foreach ($variables as $k => $v)
            {
                $messagebody = str_replace($k, $v, $contents);
                $contents    = $messagebody;
            }
            if ($template_type == 'ticket-reply-agent')
            {
                $line    = '---Reply above this line--- <br/><br/>';
                $content = $line . $messagebody;
            }
            else
            {
                $content = $messagebody;
            }
        }
        if (checkArray('subject', $temp))
        {
            $subject = checkArray('subject', $temp);
        }
        return ['content' => $content, 'subject' => $subject];
    }

    public function set($set, $ticket_number, $message, $template, $template_category)
    {
        $contents = null;
        $subject  = null;
        if (isset($set))
        {
            $subject       = checkArray('subject', $message);
            $template_data = \App\Model\Common\Template::where('set_id', '=', $set)->where('type', '=', $template->id)->where('template_category', '=', $template_category)->first();
            $contents      = $template_data->message;
            if ($template_data->variable == 1)
            {
                if ($template_data->subject)
                {
                    $subject = $template_data->subject;
                    if ($ticket_number != null)
                    {
                        $subject = $subject . ' [#' . $ticket_number . ']';
                    }
                }
            }
        }
        return ['content' => $contents, 'subject' => $subject];
    }

}
