<?php

namespace App\Http\Controllers\Agent\helpdesk;

// controllers
use App\Http\Controllers\Controller;
// requests
use App\Http\Requests\helpdesk\OrganizationRequest;
/* include organization model */
use App\Http\Requests\helpdesk\OrganizationUpdate;
// models
/* Define OrganizationRequest to validate the create form */
use App\Model\helpdesk\Agent_panel\Organization;
use App\Model\helpdesk\Agent_panel\User_org;
/* Define OrganizationUpdate to validate the create form */
use App\User;
// classes
use Exception;
use Lang;
use Illuminate\Http\Request;
use Datatables;

/**
 * OrganizationController
 * This controller is used to CRUD organization detail.
 *
 * @author      Ladybird <info@ithelpdesk.com>
 */
class OrganizationController extends Controller {

    /**
     * Create a new controller instance.
     * constructor to check
     * 1. authentication
     * 2. user roles
     * 3. roles must be agent.
     *
     * @return void
     */
    public function __construct() {
        // checking for authentication
        $this->middleware('auth');
        // checking if the role is agent
        $this->middleware('role.agent');
    }

    /**
     * Display a listing of the resource.
     *
     * @param type Organization $org
     *
     * @return type Response
     */
    public function index() {
        try {
            $table = \ Datatable::table()
                    ->addColumn(
                            Lang::get('lang.name'), 
                            Lang::get('lang.website'), 
                            Lang::get('lang.phone'), 
                            Lang::get('lang.action'))  // these are the column headings to be shown
                    ->noScript();
            /* get all values of table organization */
            return view('themes.default1.agent.helpdesk.organization.index', compact('table'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * This function is used to display the list of Organizations.
     *
     * @return datatable
     */
    public function org_list(Request $request) {
        // dd($request->all());
        $organization_list = \DB::table('organization')->select('name', 'website', 'phone', 'id');
        // chumper datable package call to display Advance datatable
        return Datatables::of($organization_list)
                        ->removeColumn('id')
                        ->editColumn('name', function($model) {
                            if (strlen($model->name) > 30) {

                                // return '<a href="'.route('organizations.show', $model->id).'" class="btn btn-primary btn-xs">'.mb_substr($model->name, 0, 30, 'UTF-8') . '...</a>';

                                return '<a  href="'.route('organizations.show', $model->id).'" title="'.$model->name.'">'.mb_substr($model->name, 0, 30, 'UTF-8') . '...</a>';

                                // return '<p title="' . $model->name . '">' . mb_substr($model->name, 0, 30, 'UTF-8') . '...</p>';
                            } else {


                  return '<a  href="'.route('organizations.show', $model->id).'" title="'.$model->name.'">'.$model->name.'</a>';


                                // return $model->name;
                            }
                        })
                        ->addColumn('action', function($model) {
                            return '<span  data-toggle="modal" data-target="#deletearticle' . $model->id . '"><a href="#" ><button class="btn btn-primary btn-xs"></a><i class="fa fa-trash" style="color:white;">&nbsp; </i> </i> ' . \Lang::get('lang.delete') . ' </button></span>&nbsp;&nbsp;<a href="' . route('organizations.edit', $model->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit" style="color:white;">&nbsp;&nbsp; </i>' . \Lang::get('lang.edit') . '</a>&nbsp;&nbsp;<a href="' . route('organizations.show', $model->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye" style="color:white;">&nbsp;&nbsp; </i>' . \Lang::get('lang.view') . '</a>
                <div class="modal fade" id="deletearticle' . $model->id . '">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">' . Lang::get('lang.delete') . ' ' . $model->name . '</h4>
                            </div>
                            <div class="modal-body">
                                ' . Lang::get('lang.are_you_sure_you_want_to_delete') . '
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="dismis2">' . Lang::get('lang.close') . '</button>
                                <a href="' . route('org.delete', $model->id) . '"><button class="btn btn-primary">' . Lang::get('lang.delete') . '</button></a>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>';
                        })
                        ->make();
    }

    /**
     * Show the form for creating a new organization.
     *
     * @return type Response
     */
    public function create() {
        try {
            return view('themes.default1.agent.helpdesk.organization.create');
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Store a newly created organization in storage.
     *
     * @param type Organization        $org
     * @param type OrganizationRequest $request
     *
     * @return type Redirect
     */
    public function store(Organization $org, OrganizationRequest $request) {
        try {
            /* Insert the all input request to organization table */
            /* Check whether function success or not */
            // if ($org->fill($request->input())->save() == true) {
            //     /* redirect to Index page with Success Message */
            //     return redirect('organizations')->with('success', Lang::get('lang.organization_created_successfully'));
            // } else {
            //     /* redirect to Index page with Fails Message */
            //     return redirect('organizations')->with('fails', Lang::get('lang.organization_can_not_create'));
            // }


            $orgss = new Organization();
            $orgss->name = $request->name;
            $orgss->phone = $request->phone;
            $orgss->website = $request->website;
            $orgss->address = $request->address;
            $orgss->internal_notes = $request->internal_notes;


            
            if( $request->client_Code){


            $orgss->client_Code = $request->client_Code;
            $orgss->phone1 = $request->phone1;
            $orgss->line_of_business = $request->line_of_business;
            $orgss->relation_type = $request->relation_type;
            $orgss->branch = $request->branch;
            $orgss->fax = $request->fax;
            }
            $orgss->save();
             return redirect('organizations')->with('success', Lang::get('lang.organization_saved_successfully'));






        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('organizations')->with('fails', Lang::get('lang.organization_can_not_create'));
        }
    }

    /**
     * Display the specified organization.
     *
     * @param type              $id
     * @param type Organization $org
     *
     * @return type view
     */
    public function show($id, Organization $org) {
  try {

    
            /* select the field by id  */
            $orgs = $org->whereId($id)->first();
            $org_heads_check = User_org::where('org_id', '=', $id)->where('role', '!=', 'members')->select('user_id')->get();
 
            if (!$org_heads_check->isEmpty()) {

                $org_heads_ids = User_org::where('org_id', '=', $id)->where('role', '!=', 'members')->pluck('user_id')->toArray();
                foreach ($org_heads_ids as $org_heads_id) {
                    $org_heads_emails[] = user::where('id', '=', $org_heads_id)->pluck('email')->first();
                }
            } else {
                $org_heads_emails = 0;
            }
            $org_heads = User_org::where('org_id', '=', $id)->where('role', '!=', 'members')->select('user_id')->get();
            if (!$org_heads->isEmpty()) {

                foreach ($org_heads as $org_head) {

                    $userss[] = User::where('id', '=', $org_head->user_id)->select('id', 'email', 'user_name', 'phone_number', 'first_name', 'last_name')->first();
                }
            } else {
                $userss = 0;
            }
            
// dd($org_heads_emails);

            /* To view page */
            return view('themes.default1.agent.helpdesk.organization.show', compact('orgs', 'userss', 'org_heads_emails'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified organization.
     *
     * @param type              $id
     * @param type Organization $org
     *
     * @return type view
     */
    public function edit($id, Organization $org) {
        try {
            /* select the field by id  */
            $orgs = $org->whereId($id)->first();

            // dd($orgs->phone);
            /* To view page */
            return view('themes.default1.agent.helpdesk.organization.edit', compact('orgs'));
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * Update the specified organization in storage.
     *
     * @param type                    $id
     * @param type Organization       $org
     * @param type OrganizationUpdate $request
     *
     * @return type Redirect
     */
    public function update($id, Organization $org, OrganizationUpdate $request) {
        try {
            /* select the field by id  */
            // $orgs = $org->whereId($id)->first();
            // /* update the organization table   */
            // /* Check whether function success or not */
            // if ($orgs->fill($request->input())->save() == true) {
            //     /* redirect to Index page with Success Message */
            //     return redirect('organizations')->with('success', Lang::get('lang.organization_updated_successfully'));
            // } else {
            //     /* redirect to Index page with Fails Message */
            //     return redirect('organizations')->with('fails', Lang::get('lang.organization_can_not_update'));
            // }
            $orgss = $org->whereId($id)->first();
            $orgss->name = $request->name;
            $orgss->phone = $request->phone;
            $orgss->website = $request->website;
            $orgss->address = $request->address;
            $orgss->internal_notes = $request->internal_notes;
             if( $request->client_Code){
            $orgss->client_Code = $request->client_Code;
            $orgss->phone1 = $request->phone1;
            $orgss->line_of_business = $request->line_of_business;
            $orgss->relation_type = $request->relation_type;
            $orgss->branch = $request->branch;
            $orgss->fax = $request->fax;
           }
            $orgss->save();
            return redirect('organizations')->with('success', Lang::get('lang.organization_updated_successfully'));
        } catch (Exception $e) {
            //            dd($e);
            /* redirect to Index page with Fails Message */
            return redirect('organizations')->with('fails', $e->getMessage());
        }
    }

    /**
     * Delete a specified organization from storage.
     *
     * @param type int $id
     *
     * @return type Redirect
     */
    public function destroy($id, Organization $org, User_org $user_org) {
        try {
            /* select the field by id  */
            $orgs = $org->whereId($id)->first();
            $user_orgs = $user_org->where('org_id', '=', $id)->get();
            foreach ($user_orgs as $user_org) {
                $user_org->delete();
            }
            /* Delete the field selected from the table */
            /* Check whether function success or not */
            $orgs->delete();
            /* redirect to Index page with Success Message */
            return redirect('organizations')->with('success', Lang::get('lang.organization_deleted_successfully'));
        } catch (Exception $e) {
            /* redirect to Index page with Fails Message */
            return redirect('organizations')->with('fails', $e->getMessage());
        }
    }

     public function getHrdname(Request $request, $org_id) {
        // $term = $request->input('term');

        // $users = new User();

        // // $users = app\User::where('email', 'LIKE', '%' . $term . '%')->where('active', '=', 1)->where('role', '=', 'user')->get();
        // $user = $users->where('email', 'LIKE', '%' . $term . '%')->where('role', '!=', 'user')->pluck('email')->toArray();
        // return $user;


        $term = $request->input('term');
      
        $user = User::join('user_assign_organization', 'users.id', '=', 'user_assign_organization.user_id')
                ->join('organization', 'user_assign_organization.org_id', '=', 'organization.id')
                ->where('organization.id', $org_id)
                ->where('user_assign_organization.role', '!=', 'manager')
                ->where('users.email', 'LIKE', '%' . $term . '%')
                ->select('users.email')
                ->pluck('email');

       
        return $user;
    }

    public function Head_Org(Request $request) {
        
        $emails = $request->user;
        if (!$emails) {
            return redirect()->back()->with('fails1', Lang::get('lang.please_select_manager'));
        }
        foreach ($emails as $email) {
            $userids[] = user::where('email', '=', $email)->pluck('id')->first();
        }


        // $delete_user_id= User_org::where('user_id', '=', $userids)->where('role','=','members')->delete();
        // $reir_org = User_org::where('org_id', '=', $request->org_id)->where('role','=','manager')->delete();
        // Page::where('id', $id)->update(array('role' => 'members'));
        $update = User_org::where('org_id', '=', $request->org_id)->update(array('role' => 'members'));

        foreach ($userids as $userid) {
            $org_head = User_org::where('org_id', '=', $request->org_id)->where('user_id', '=', $userid)->where('role', '=', 'members')->first();
           
            $org_head->role = 'manager';
            $org_head->save();
        }

        // route('organizations.show', $model->id)
        return redirect()->back()->with('success1', Lang::get('lang.organization_updated_successfully'));
        // $emails = $request->user;
        // if (!$emails) {
        //     return redirect('organizations')->with('fails', Lang::get('lang.please_select_manager'));
        // }
        // foreach ($emails as $email) {
        //     $userids[] = user::where('email', '=', $email)->pluck('id')->first();
        // }
        // $reir_org = User_org::where('org_id', '=', $request->org_id)->where('role','=','manager')->delete();


        // foreach ($userids as $userid) {
        //     $org_head = new User_org;
        //     $org_head->org_id = $request->org_id;
        //     $org_head->user_id = $userid;
        //     $org_head->role = 'manager';
        //     $org_head->save();
        // }


        // return redirect('organizations')->with('success', Lang::get('lang.organization_updated_successfully'));

        // // get the user to make organization head
        // // $head_user = \Input::get('user');
        // // get an instance of the selected organization
        // // $org_head = Organization::where('id', '=', $id)->first();
        // // $org_head->head = $id;
        // // save the user to organization head
        // // $org_head->save();
        // // $org_heads=new User_org_head();
        // // foreach ($org_heads as $org_head) {
        // //     # code...
        // // }
    }

    public function EditHead_Org(Request $request) {


         $emails = $request->user;
        if (!$emails) {
            $reir_org = User_org::where('org_id', '=', $request->org_id)->update(array('role' => 'members'));           

             return redirect()->back()->with('success1', Lang::get('lang.organization_updated_successfully'));
        }
        foreach ($emails as $email) {
            $userids[] = user::where('email', '=', $email)->pluck('id')->first();
        }
        // $reir_org = User_org::where('org_id', '=', $request->org_id)->where('role','=','manager')->delete();
// dd($userids);
        $update = User_org::where('org_id', '=', $request->org_id)->update(array('role' => 'members'));

        // dd($update);
        foreach ($userids as $userid) {
           
            $org_head = User_org::where('org_id', '=', $request->org_id)->where('user_id', '=', $userid)->where('role', '=', 'members')->first();
            
            $org_head->role = 'manager';
            $org_head->save();
        
        }
        return redirect()->back()->with('success1', Lang::get('lang.organization_updated_successfully'));

        // $emails = $request->user;

        // if (!$emails) {
        //     $reir_org = User_org::where('org_id', '=', $request->org_id)->delete();
        //     return redirect('organizations')->with('success', Lang::get('lang.organization_updated_successfully'));
        // }
        // foreach ($emails as $email) {
        //     $userids[] = user::where('email', '=', $email)->pluck('id')->first();
        // }
        // $reir_org = User_org::where('org_id', '=', $request->org_id)->where('role','=','manager')->delete();


        // foreach ($userids as $userid) {
        //     $org_head = new User_org;
        //     $org_head->org_id = $request->org_id;
        //     $org_head->user_id = $userid;
        //     $org_head->role = 'manager';
        //     $org_head->save();
        // }


        // return redirect('organizations')->with('success', Lang::get('lang.organization_updated_successfully'));
    }

    /**
     * Display the specified organization.
     *
     * @param type              $id
     * @param type Organization $org
     *
     * @return type view
     */
    public function HeadDelete(Request $request) {
        try {



           $user_id = $request->user_id;
   
            $org_head = User_org::where('org_id', '=', $orgs_id)->where('user_id', '=', $user_id)->delete();
            // return redirect('themes.default1.agent.helpdesk.organization.show')->with('success', Lang::get('lang.organization_maneger_delete_successfully'));
            return 'maneger successfully delete';
        } catch (Exception $e) {
            return redirect()->back()->with('fails', $e->getMessage());
        }
    }

    /**
     * get the report of organizations.
     *
     * @param type $id
     * @param type $date111
     * @param type $date122
     *
     * @return type array
     */
    public function orgChartData($id, $date111 = '', $date122 = '') {
        $date11 = strtotime($date122);
        $date12 = strtotime($date111);
        if ($date11 && $date12) {
            $date2 = $date12;
            $date1 = $date11;
        } else {
            // generating current date
            $date2 = strtotime(date('Y-m-d'));
            $date3 = date('Y-m-d');
            $format = 'Y-m-d';
            // generating a date range of 1 month
            $date1 = strtotime(date($format, strtotime('-1 month' . $date3)));
        }
        $return = '';
        $last = '';
        for ($i = $date1; $i <= $date2; $i = $i + 86400) {
            $thisDate = date('Y-m-d', $i);

            $user_orga_relation_id = '';
            $user_orga_relations = User_org::where('org_id', '=', $id)->get();
            foreach ($user_orga_relations as $user_orga_relation) {
                $user_orga_relation_id[] = $user_orga_relation->user_id;
            }
            $created = \DB::table('tickets')->select('created_at')->whereIn('user_id', $user_orga_relation_id)->where('created_at', 'LIKE', '%' . $thisDate . '%')->count();
            $closed = \DB::table('tickets')->select('closed_at')->whereIn('user_id', $user_orga_relation_id)->where('closed_at', 'LIKE', '%' . $thisDate . '%')->count();
            $reopened = \DB::table('tickets')->select('reopened_at')->whereIn('user_id', $user_orga_relation_id)->where('reopened_at', 'LIKE', '%' . $thisDate . '%')->count();

            $value = ['date' => $thisDate, 'open' => $created, 'closed' => $closed, 'reopened' => $reopened];
            $array = array_map('htmlentities', $value);
            $json = html_entity_decode(json_encode($array));
            $return .= $json . ',';
        }
        $last = rtrim($return, ',');
        $users = User::whereId($id)->first();

        return '[' . $last . ']';
    }

    public function getOrgAjax(Request $request) {
        $org = new Organization();
        $q = $request->input('term');
        $orgs = $org->where('name', 'LIKE', '%' . $q . '%')
                ->select('name as label', 'id as value')
                ->get()
                ->toJson();
        return $orgs;
    }

    /**
     * This function is used autofill organizations name .
     *
     * @return datatable
     */
    public function organizationAutofill() {
        return view('themes.default1.agent.helpdesk.organization.getautocomplete');
    }

}
