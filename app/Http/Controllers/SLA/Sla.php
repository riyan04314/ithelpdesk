<?php

namespace App\Http\Controllers\SLA;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\helpdesk\Manage\Sla\Sla_plan;
use App\Model\helpdesk\Manage\Sla\SlaTargets;
use App\Model\helpdesk\Ticket\Ticket_Priority;
use App\Model\helpdesk\Manage\Sla\BusinessHours;

class Sla extends Controller {

    public $id;
    public $sla;
    protected $name;
    protected $admin_note;
    protected $status;
    protected $sla_target;
    protected $departments;
    protected $companies;
    protected $ticket_type;
    protected $ticket_source;
    protected $today;
    public $priority;

    public function __construct($slaid) {
        $plan = new Sla_plan();
        $sla = $plan->find($slaid);
        $this->sla = $sla;
        if ($sla) {
            $this->name = $sla->name;
            $this->admin_note = $sla->admin_note;
            $this->status = $sla->status;
            $this->id = $sla->id;
            //$this->today = date('l', time()+(86400*0));
            $this->today = date('l', time());
            $this->priority = $this->getPrority()->priority_id;
        }
    }

    public function getId() {
        if ($this->sla) {
            return $this->sla->id;
        }
    }

    public function getName() {
        if ($this->sla) {
            return $this->sla->name;
        }
    }

    public function getNote() {
        if ($this->sla) {
            return $this->sla->admin_note;
        }
    }

    public function getStatus() {
        $check = false;
        if ($this->sla && $this->sla->status == 1) {
            $check = true;
        }
        return $check;
    }

    protected function getTargetModel() {
        if ($this->sla) {
            $targetid = $this->sla->sla_target;
            if ($targetid) {
                $model = SlaTargets::find($targetid);
                return $model;
            }
        }
    }

    protected function getPrority() {
        if ($this->sla) {
            $target = $this->getTargetModel();
            if ($target) {
                $priorityid = $target->priority_id;
                $model = Ticket_Priority::find($priorityid);
                return $model;
            }
        }
    }

    public function getProrityName() {
        if ($this->sla) {
            $priority = $this->getPrority();
            if ($priority) {
                return $priority->priority;
            }
        }
    }

    public function getProrityId() {
        if ($this->sla) {
            $priority = $this->getPrority();
            if ($priority) {
                return $priority->id;
            }
        }
    }

    public function isSendEmail() {
        if ($this->sla) {
            $target = $this->getTargetModel();
            $check = false;
            if ($target && $target->send_email == 1) {
                $check = true;
            }
            return $check;
        }
    }

    public function isSendSms() {
        if ($this->sla) {
            $target = $this->getTargetModel();
            $check = false;
            if ($target && $target->send_sms == 1) {
                $check = true;
            }
            return $check;
        }
    }

    public function respondTime() {
        if ($this->sla) {
            $target = $this->getTargetModel();
            if ($target) {
                $respond = $target->respond_within;
                $array = explode('-', $respond);
                $time = checkArray(0, $array);
                $period = checkArray(1, $array);
                return $this->timeInMinutes($time, $period);
            }
        }
    }

    public function resolveTime() {
        if ($this->sla) {
            $target = $this->getTargetModel();
            if ($target) {
                $respond = $target->resolve_within;
                $array = explode('-', $respond);
                $time = checkArray(0, $array);
                $period = checkArray(1, $array);
                return $this->timeInMinutes($time, $period);
            }
        }
    }

    public function timeInMinutes($time, $period) {
        switch ($period) {
            case "min":
                return $time;
            case "hrs":
                return $time * 60;
            case "days":
                return $time * 24 * 60;
            case "months":
                return $time * 24 * 30 * 60;
            default :
                return 0;
        }
    }

    public function getDepartments() {
        if ($this->sla) {
            return explode(',', $this->sla->apply_sla_depertment);
        }
    }

    public function getCompanies() {
        if ($this->sla) {
            return explode(',', $this->sla->apply_sla_company);
        }
    }

    public function getTicketTypes() {
        if ($this->sla) {
            return explode(',', $this->sla->apply_sla_tickettype);
        }
    }

    public function getTicketSources() {
        if ($this->sla) {
            return explode(',', $this->sla->apply_sla_ticketsource);
        }
    }

    protected function getApproaches() {
        if ($this->sla) {
            return $this->sla->approach;
        }
    }

    public function getApproachResponseTime($time) {
        if ($this->sla) {
            $approach = $this->getApproaches();
            if ($approach) {
                $response_escalate_time = $approach->response_escalate_time;
                return $time->subMinutes($response_escalate_time);
            }
        }
    }

    public function getApproachResponsePerson() {
        if ($this->sla) {
            $approach = $this->getApproaches();
            if ($approach) {
                $response_escalate_person = $approach->response_escalate_person;
                return explode(',', $response_escalate_person);
            }
        }
    }

    public function getApproachResolutionTime($time) {
        if ($this->sla) {
            $approach = $this->getApproaches();
            if ($approach) {
                $response_resolution_time = $approach->resolution_escalate_time;
                return $time->subMinutes($response_resolution_time);
            }
        }
    }

    public function getApproachResolutionPerson() {
        if ($this->sla) {
            $approach = $this->getApproaches();
            if ($approach) {
                $response_resolution_person = $approach->resolution_escalate_person;
                return explode(',', $response_resolution_person);
            }
        }
    }

    protected function getViolates() {
        if ($this->sla) {
            return $this->sla->violated;
        }
    }

    public function getViolateResponseTime($time) {
        if ($this->sla) {
            $approach = $this->getViolates();
            if ($approach) {
                $response_escalate_time = $approach->response_escalate_time;
                return $time->addMinutes($response_escalate_time);
            }
        }
    }

    public function getViolateResponsePerson() {
        if ($this->sla) {
            $approach = $this->getViolates();
            if ($approach) {
                $response_escalate_person = $approach->response_escalate_person;
                return explode(',', $response_escalate_person);
            }
        }
    }

    public function getViolateResolutionTime($time) {
        if ($this->sla) {
            $approach = $this->getViolates();
            if ($approach) {
                $response_resolution_time = $approach->resolution_escalate_time;
                return $time->addMinutes($response_resolution_time);
            }
        }
    }

    public function getViolateResolutionPerson() {
        if ($this->sla) {
            $approach = $this->getViolates();
            if ($approach) {
                $response_resolution_person = $approach->resolution_escalate_person;
                return explode(',', $response_resolution_person);
            }
        }
    }

    protected function getBusinessHours() {
        if ($this->sla) {
            $business = $this->sla->target->businessHour;
            return $business;
        }
    }

    public function getBusinessHourName() {
        if ($this->sla) {
            $model = $this->getBusinessHours();
            if ($model) {
                return $model->name;
            }
        }
    }

    public function getBusinessHourDescription() {
        if ($this->sla) {
            $model = $this->getBusinessHours();
            if ($model) {
                return $model->description;
            }
        }
    }

    public function getBusinessHourStatus() {
        if ($this->sla) {
            $model = $this->getBusinessHours();
            $check = false;
            if ($model && $model->status) {
                $check = true;
            }
            return $check;
        }
    }

    public function getBusinessHourTimezone() {
        if ($this->sla) {
            $model = $this->getBusinessHours();
            if ($model) {
                $time_zone = $model->timezone;
            }
            if (!$time_zone || $time_zone == "0" || $time_zone == "") {
                $systems = new \App\Model\helpdesk\Settings\System();
                $system = $systems->first();
                if ($system) {
                    $time_zone = $system->timezone->name;
                }
            }
            return $time_zone;
        }
    }

    public function date($format = "m-d", $string = "now") { // "+1 day"
        return gmdate($format, strtotime($string));
    }

    public function getBusinessSchedule($string = "now") {
        if ($this->sla) {
            $date = $this->date("m-d", $string);
            $day = $this->date('l', $string);
            if ($this->sla) {
                $holiday = $this->isHoliday($date);
                if ($holiday == true) {
                    return false;
                }
                $business = $this->sla->target->businessHour->schedule()->where('days', $day)->select('id', 'status')->first();
                if ($business) {
                    return $this->getScheduledTime($business);
                }
            }
            return true;
        }
    }

    public function getScheduledTime($schedule) {
        if ($schedule) {
            switch ($schedule->status) {
                case "Open_custom":
                    //dd($schedule->custom()->select('open_time', 'close_time')->get()->toArray());
                    return $schedule->custom()->select('open_time', 'close_time')->get()->toArray();
                case "Open_fixed":
                    return true;
                case "Closed":
                    return false;
            }
        }
    }

    public function getHolidayList() {
        if ($this->sla) {
            return $business = $this->sla->target->businessHour->holiday()->pluck('name', 'date')->toArray();
        }
    }

    public function isHoliday($date) {
        //dd($date);
        if ($this->sla) {
            $holiday = $this->sla->target->businessHour->holiday()->where('date', $date)->first();
            if ($holiday) {
                return true;
            }
        }
    }
    
    public function timezone(){
        $time_zone = "";
        if ($this->sla) {
            $model = $this->getBusinessHours();
            if ($model && $model->timezone) {
                $time_zone = $model->timezone;
            }
            if($time_zone==""){
                $time_zone = timezone();
            }
            return $time_zone;
        }
    }

}
