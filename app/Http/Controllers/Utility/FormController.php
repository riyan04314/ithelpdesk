<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Custom\Form;
use DB;
use Auth;

class FormController extends Controller
{

    public function getTicketFormJson($array = false)
    {
        $form   = new Form();
        $custom = $form->where('form', 'ticket')->select('json')->first();
        $json   = "";
        if ($custom)
        {
            $json = str_replace("'", '"', $custom->json);
        }
        if ($array)
        {
            return $json;
        }
        return '[' . $json . "]";
    }

    public function dependancy(Request $request)
    {
        $dependency = $request->input('dependency', 'priority');
        return $this->getDependancy($dependency);
    }

    public function getDependancy($dependency)
    {
        switch ($dependency)
        {
            case "priority":
                return DB::table('ticket_priority')->where('status', '=', 1)->where('ispublic', 1)->select('priority_id as id', 'priority as optionvalue')->get()->toJson();
            case "department":
                return DB::table('department')->select('id', 'name as optionvalue')->get()->toJson();
            case "type":
                return DB::table('ticket_type')->where('status', '=', 1)->select('id', 'name as optionvalue')->get()->toJson();
            case "assigned_to":
                return DB::table('users')->where('role', '!=', 'user')->where('is_delete', '!=', 1)->select('id', 'user_name as optionvalue')->get()->toJson();
            case "company":
                return DB::table('organization')->select('id', 'name as optionvalue')->get()->toJson();
            case "status":
                if (Auth::user() && Auth::user()->role != 'user')
                {
                    return DB::table('ticket_status')->select('id', 'name as optionvalue')->get()->toJson();
                }
                return DB::table('ticket_status')->where('visibility_for_client', 1)->where('allow_client', 1)->select('id', 'name as optionvalue')->where('purpose_of_status', 1)->get()->toJson();

            // case "status":
            //     return DB::table('ticket_status')->select('id', 'name as optionvalue')->get()->toJson();
            case "helptopic":
                return DB::table('help_topic')->where('status', '=', 1)->where('type', 1)->select('id', 'topic as optionvalue')->get()->toJson();
        }
    }

    public function requester(Request $request)
    {
        $method = $request->input('type', 'agent');
        $user   = new \App\User();
        $term   = $request->input('term');
        if ($method == 'agent')
        {
            $users             = $user
                    ->where('is_delete', '!=', 1)
                    ->where('first_name', 'LIKE', '%' . $term . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $term . '%')
                    ->orWhere('user_name', 'LIKE', '%' . $term . '%')
                    ->orWhere('email', 'LIKE', '%' . $term . '%')
                    ->with(['org' => function($org)
                        {
                            $org->select('org_id', 'user_id');
                        }, 'org.organisation' => function($company)
                        {
                            $company->select('id', 'name as company');
                        }])
                    ->select('id', 'user_name as name', 'first_name', 'last_name')
                    ->get();
            return $users;
        }
        else
        {
            $users = $user->where('user_name', $term)
                    ->where('is_delete', '!=', 1)
                    ->select('id', 'user_name as name', 'first_name', 'last_name')
                    ->first();
            return $users;
        }
    }

    public function authRequesterClient()
    {
        $user = NULL;
        if (\Auth::user())
        {
            $user = \Auth::user();
        }
        return $user;
    }

    public function ticketFormBuilder()
    {
        $json            = $this->getTicketFormJson(true);
        $array           = json_decode($json, true);
        $array_title_key = collect($array)->keyBy('title')->toArray();
        $result          = $this->parseTicketFormArray($array_title_key);
        return $result;
    }

    public function parseTicketFormArray($array)
    {
        foreach ($array as $key => $value)
        {
            $result[$key] = $this->parseParent($value, $key);
        }

        return $result;
    }

    public function parseParent($value, $key = "", $parent = "", $child = "",$option_value="")
    {
        $agent            = checkArray('agentRequiredFormSubmit', $value);
        $client           = checkArray('customerRequiredFormSubmit', $value);
        $result['agent']  = $agent;
        $result['client'] = $client;
        $result['parent'] = $parent;
        $result['label']  = $child;
        $result['option']  = $option_value;
        $options          = checkArray('options', $value);
        if ($options && count($options) > 0)
        {
            $array = $this->parseOptions($options, $key);
            if (is_array($array))
            {
                $result = array_merge($result, $array);
            }
        }
        return $result;
    }

    public function parseOptions($options, $parent = "")
    {
        $result = [];
        foreach ($options as $option)
        {
            $nodes = checkArray('nodes', $option);
            $option_value = checkArray('optionvalue', $option);
            if ($nodes && checkArray('0', $nodes))
            {
                $node              = $nodes[0];
                $result['child'][] = $this->parseParent($node, $node['agentlabel'], $parent, $node['agentlabel'],$option_value);
            }
        }
        return $result;
    }

    public function saveRequired($form = 'ticket')
    {
        \App\Model\Custom\Required::truncate();
        $array = $this->ticketFormBuilder();
        foreach ($array as $parent => $values)
        {
            $this->saveOptions($parent, $values, $form);
        }
    }

    public function saveOptions($key, $values, $form, $option='required',$parent_id = "")
    {
        $required=[];
        $child = checkArray('child', $values);
        $option_value = checkArray('option', $values);
        if(!is_string($option_value)){
            $option_value = head($option_value)['option'];
        }
        $required['option'] = $option_value;
        if (checkArray('agent', $values))
        {
            $required['agent'] = $option;
        }
        
        
        if (checkArray('client', $values))
        {
            $required['client'] = $option;
        }
        if ($parent_id)
        {
            $required['parent'] = $parent_id;
        }
        if ($required)
        {
            
            if(!is_string($key)){
                $key = head($key)['label'];
            }
            $required['field'] = $key;
            //echo json_encode($required).PHP_EOL;
            $required_model = \App\Model\Custom\Required::updateOrCreate($required);
            if ($child)
            {
                $parent_field_name = $key;
                $this->saveChild($child, $parent_field_name,$required_model, $form);
            }
        }
    }

    public function saveChild($child,  $parent_field_name,$model, $form,$option_value="")
    {
        foreach ($child as $values)
        {
            $option = "required_if:$parent_field_name";
            //$option = 'required';
            $this->saveOptions($values['label'], $values, $form, $option,$model->id);
        }
    }

}
