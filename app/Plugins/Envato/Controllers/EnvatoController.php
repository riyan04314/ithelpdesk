<?php

namespace App\Plugins\Envato\Controllers;

/**
 * Envato Market API
 *
 * @author Ladybird Web Solution <info@ithelpdesk.com>
 * @version 1.0
 * @license GPL v2
 */
class EnvatoController {

    private $personal_token;
    private $api_url = "https://api.envato.com/v2/market";
    private $download_url;

    public function verify_purchase_code($code) {
        //dd( $this->personal_token);
        $url = $this->api_url . "/buyer/purchase/purchase?code=" . $code;
        $x = $this->request($url);
        return $x;
    }

    public function request($url, $decode_json = false) {
//          $client = new \GuzzleHttp\Client();
//        $data = $client->request('GET', $url,[
//            'headers'=>[
//                'Authorization'=>"Bearer ".$this->personal_token,
//            ],
//        ]);
//        $data->getHeaderLine('content-type');
//        return $data->getBody();
        $context = stream_context_create(array(
            'http' => array(
                'method' => "GET",
                'header' => "Authorization: Bearer " . $this->personal_token
            )
        ));
        $data = file_get_contents($url, false, $context);
        return $data;
    }

    public function set_personal_token($token) {
        $this->personal_token = $token;
        //dd($this->personal_token);
    }

}
