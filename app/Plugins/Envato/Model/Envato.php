<?php namespace App\Plugins\Envato\Model;

use Illuminate\Database\Eloquent\Model;

class Envato extends Model {

	protected $table='envato';

	protected $fillable = ['token','mandatory'];

}
