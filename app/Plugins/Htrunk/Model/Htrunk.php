<?php namespace App\Plugins\Reseller\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\helpdesk\Agent_panel\Organization;

class Reseller extends Model {

	   protected $table = 'organization';

    /* Define the fillable fields */
    // protected $fillable = ['id', 'name', 'phone', 'website', 'address', 'head', 'internal_notes'];
     protected $fillable = ['id', 'name', 'phone', 'website', 'address', 'head', 'internal_notes','client_Code','phone1','line_of_business','relation_type','branch','sla_type'];

}
