<?php

namespace App\Plugins\Telephony\Controllers\Mcube;

use App\Http\Controllers\Controller;
use App\Plugins\Telephony\Model\Core\Telephone;
use Exception;
use App\Plugins\Telephony\Model\Core\TelephoneDetail;
use Illuminate\Http\Request;

class McubeController extends Controller {
    
    public function setSettings(){
        $settings = new \App\Plugins\Telephony\Controllers\Core\SettingsController();
        return $settings;
    }


    public function passThrough($modelid,$model,$request){
       // dd($modelid,$model,$request);
        $data = $request->input('data');
//        $data = $this->dummy();
        loging('mcube', $data, 'info');
        $requests = json_decode($data,true);
       // dd($requests);
        $callid = $requests['callid'];
        $provider = "mcube";
        $requ = $this->setParameter($requests);
        $settings = $this->setSettings();
        $settings->saveCall($callid,$provider,$requ,$modelid,$model);
        return response("success", 200);
    }
    
    public function setParameter($requests){
        $details = new TelephoneDetail();
        $domain = $details->getValue('m_cube','domain');
        $settings = $this->setSettings();
        $record = $settings->checkKey('filename',$requests);
        $record_url = "";
        if($record!=""){
            $record_url = $domain."/sounds/".$record;
        }
        return[
            'from'=>$settings->checkKey('callfrom',$requests),//$requests['From'],
            'to'=>$settings->checkKey('empnumber',$requests),//$requests['To'],
            'record'=>$record_url,//$requests['RecordingUrl'],
            'toWhom'=>$settings->checkKey('empnumber',$requests),//$requests['DialWhomNumber'],
            'date'=>$settings->checkKey('starttime',$requests),//$requests['Created'],
        ];
    }
    
    public function getValues($modelid,$model){
        $values = $this->dummyValues();
        echo "<form action='".url('telephone/mcube/pass/'.$modelid.'/'.$model)."' name='redirect'>";
        echo $values;
        echo '</form>';
        echo "<script language='javascript'>document.redirect.submit();</script>";
    }
    
    public function dummyValues(){
        $values = "";
        $json = '{"callid":"80420960731474885338","calid":"14","refid":"14","bid":"MTUzNw==","gid":"Faveo Helpdesk","assignto":"Vijay","eid":"Vijay","source":"calltrack","landingnumber":"08033688724","callfrom":"8042096073","callto":"9663218862","starttime":"2016-09-26 15:52:18","endtime":"2016-09-26 15:52:18","pulse":"0","callername":"","callerbusiness":"","calleraddress":"","remark":"","caller_email":"","status":"1","keyword":"","keyword1":"","keyword2":"","filename":"80420960731474885338.wav","dialstatus":"CONNECTING","last_modified":"0000-00-00 00:00:00","lead":"","suptkt":"","businessname":"Faveo Helpdesk","asto":"1","duration":"0","empnumber":"9663218862","empid":"1","geid":"1","grid":"1","empemail":"vijaysebastian111@gmail.com","region":"KA","apikey":"12898471760a93f1d4427f4817e67207"}';
        $array = json_decode($json);
        foreach($array as $key=>$value){
            $values .="<input type='text' name='".$key."' value='".$value."'>"; 
        }
        return $values;
    }
    
    public function dummy(){
        return '{"callid":"80420960731474885338","calid":"14","refid":"14","bid":"MTUzNw==","gid":"Faveo Helpdesk","assignto":"Vijay","eid":"Vijay","source":"calltrack","landingnumber":"08033688724","callfrom":"8042096073","callto":"9663218862","starttime":"2016-09-26 15:52:18","endtime":"2016-09-26 15:52:18","pulse":"0","callername":"","callerbusiness":"","calleraddress":"","remark":"","caller_email":"","status":"1","keyword":"","keyword1":"","keyword2":"","filename":"80420960731474885338.wav","dialstatus":"CONNECTING","last_modified":"0000-00-00 00:00:00","lead":"","suptkt":"","businessname":"Faveo Helpdesk","asto":"1","duration":"0","empnumber":"9663218862","empid":"1","geid":"1","grid":"1","empemail":"vijaysebastian111@gmail.com","region":"KA","apikey":"12898471760a93f1d4427f4817e67207"}';
    }
    
   
    
}
