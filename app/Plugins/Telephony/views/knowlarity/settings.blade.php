@extends('themes.default1.admin.layout.admin')
@section('content')

<section class="content-heading-anchor">
    <h1>
        {{Lang::get('telephone::lang.telephone-integration')}}  


    </h1>

</section>


<!-- Main content -->

<div class="box box-primary">
    <div class="box-header with-border">
        <h4>  {{ucfirst($name)}}   </h4>
        <!-- /.box-header -->
        <!-- form start -->
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
<!--        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Your exotel information Pass Through url : <pre>{{url('telephone/knowlarity/pass') }}</pre>
        </div>-->
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif
        {!! Form::open(['url'=>'telephone/'.$short,'method'=>'post']) !!}
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 form-group {{ $errors->has('department') ? 'has-error' : '' }}">

                    {!! Form::label('department','Departments') !!}
                    {!! Form::select('department',$departments,null,['class' => 'form-control']) !!}

                </div>
                <div class="col-md-6 form-group {{ $errors->has('help') ? 'has-error' : '' }}">

                    {!! Form::label('help','Helptopic') !!}
                    {!! Form::select('help',$topics,null,['class' => 'form-control']) !!}

                </div>
                <div class="col-md-12">
                    <div id="url"></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group{{ $errors->has('domain') ? 'has-error' : '' }}">
                    <label for="domain" class="control-label">Domain</label>
                    {!! Form::text('domain',$details->getValue($short,'domain'),['class'=>'form-control']) !!}
                </div>
                
            </div>
        </div>
    </div>
    <div class="box-footer">
        {!! Form::submit(Lang::get('telephone::lang.save'),['class'=>'btn btn-success']) !!}
        {!! Form::close() !!}
    </div>
</div>

@stop
@section('FooterInclude')
<script>
    $(document).ready(function () {
        var department = $("#department").val();
        send(department,'department');
        $("#department").on('change', function () {
            department = $("#department").val();
            send(department,'department');
        });
        $("#help").on('change', function () {
            var help = $("#help").val();
            send(help,'helptopic');
        });
        function send(modelid,model) {
            var app = "{{strtolower($name)}}";
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('telephone/ajax-url')}}",
                data: {'modelid': modelid,'model':model ,'app': app},
                success: function (data) {
                    $("#url").html(data);
                },
            });
        }
    });
</script>
@stop
