<div class="box box-primary">

    @if(Session::has('orgdoclistssuccess'))
    <div id="success-alert" class="alert alert-success alert-dismissable">
        <i class="fa  fa-check-circle"> </i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('orgdoclistssuccess')}}
    </div>
    @endif
    <!-- failure message -->
    @if(Session::has('fails'))
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"> </i> <b> {!! Lang::get('lang.alert') !!} ! </b>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('fails')}}
    </div>
    @endif

    <div class="box-header with-border">
        <span class="lead border-right">{{Lang::get('HtrunkDocs::lang.list_of_documents')}}</span>

        <!-- {{Lang::get('htrunkDocs::lang.list_of_documents')}} -->
        <div class="pull-right">

        </div>
    </div>
    <div class="box-body">



        <table id="docs_list_table" class="display" cellspacing="0" width="100%">
            <thead>

                <tr>
                    <th>{{ Lang::get('HtrunkDocs::lang.name')}}</th>
                    <th>{{  Lang::get('HtrunkDocs::lang.created_date')}}</th>
                    <th>{{  Lang::get('HtrunkDocs::lang.action')}}</th> 

                </tr>
            </thead> 
            @foreach($license_docs_list as $license_docs_lists)

            <tbody>
                <tr>
                    <td>{!! $license_docs_lists->file_name !!}</td>
                    <td>{!! $license_docs_lists->created_at !!}</td>
                    <td>

                        <a href="{!! route('org.download.docs',$license_docs_lists->file_name) !!}"><button class="btn btn-info btn-xs" title="{!! Lang::get('lang.download') !!}"> <i class='fa fa-download'> </i></button></a>
                        @if(Auth::user()->role!='user')
                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#{{$license_docs_lists->id}}delete"> <i class='fa fa-trash'> </i></button>
                        <div class="modal fade" id="{{$license_docs_lists->id}}delete">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">{!! Lang::get('lang.delete') !!}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>{!! Lang::get('lang.are_you_sure_you_want_to_delete') !!}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <a href="{!! route('org.download.delete.docs',$license_docs_lists->id) !!}"><button class="btn btn-info">Submit</button></a>

                                    </div>
                                </div> 
                            </div>
                        </div>
                        @endif 
                    </td>   

                </tr>
            </tbody>
            @endforeach
        </table>

    </div>
    <div class="box-footer">
    </div>
</div>



<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
// $('#myTable1').DataTable();
$('#docs_list_table').DataTable();
</script>

















