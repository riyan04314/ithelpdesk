




        <div class="row">
            <div class="col-xs-4 form-group {{ $errors->has('client_Code') ? 'has-error' : '' }}">
                {!! Form::label('client_Code',Lang::get('lang.client_Code')) !!}

                {!! Form::text('client_Code',null,['class' => 'form-control']) !!}

            </div>
            <!-- phone : Text : -->
            <div class="col-xs-4 form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                {!! Form::label('phone1',Lang::get('lang.phone1')) !!}
                {!! Form::input('number','phone1',null,['class' => 'form-control']) !!}

            </div>
            <!-- website : Text :  -->
            <div class="col-xs-4 form-group {{ $errors->has('Line of business') ? 'has-error' : '' }}">
                {!! Form::label('line_of_business',Lang::get('lang.line_of_business')) !!}
                {!! Form::text('line_of_business',null,['class' => 'form-control']) !!}

            </div>
        </div>

        <div class="row">
            <div class="col-xs-4 form-group {{ $errors->has('relation_type') ? 'has-error' : '' }}">
                {!! Form::label('relation_type',Lang::get('lang.relation_type')) !!} 
                <select name="relation_type" class="form-control">
                    <option value="partner">{{Lang::get('lang.partner')}}</option>
                    <option value="direct_client">{{Lang::get('lang.direct_client')}}</option>

                </select>
            </div>

            <div class="col-xs-4 form-group {{ $errors->has('branch') ? 'has-error' : '' }}">
                {!! Form::label('branch',Lang::get('lang.branch')) !!}
                {!! Form::text('branch',null,['class' => 'form-control']) !!}
            </div>

        
            <div class="col-xs-4 form-group {{ $errors->has('fax') ? 'has-error' : '' }}">
                {!! Form::label('fax',Lang::get('lang.fax')) !!} 
                {!! Form::input('number','fax',null,['class' => 'form-control']) !!}
                <!-- {!! Form::text('fax',null,['class' => 'form-control']) !!} -->

            </div>


        </div>


       