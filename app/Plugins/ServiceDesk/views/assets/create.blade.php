@extends('themes.default1.agent.layout.agent')
@section('content')

<section class="content-heading-anchor" id="heading">
    <h2>
        {{Lang::get('service::lang.assets')}}  

    </h2>

</section>
<div class="box box-primary" ng-controller="CreateTicketServicedesk">
    <div id="response"></div>
    <div class="box-header with-border">
        <h4>{{Lang::get('service::lang.create_new_asset')}}</h4>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success')}}
        </div>
        @endif
        <!-- fail message -->
        @if(Session::has('fails'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <b>{{Lang::get('message.alert')}}!</b> {{Lang::get('message.failed')}}.
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('fails')}}
        </div>
        @endif


    </div>
    <div class="box-body">

        <div class="row">
            {!! Form::open(['url'=>'service-desk/assets/post/create/','method'=>'POST','files'=>true,'id'=>'asset-type-form']) !!}
 
                <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name" class="control-label">{{Lang::get('service::lang.name')}} <span class="text-red"> *</span></label> 
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                    <!--<input type="text" class="form-control" name="asset_name" placeholder="name" id="name">-->
                </div>
                <div class="form-group col-md-6 {{ $errors->has('external_id') ? 'has-error' : '' }}">
                    <label for="external_id" class="control-label">{{Lang::get('service::lang.identifier')}}</label> 
                    {!! Form::text('external_id',null,['class'=>'form-control']) !!}
                    <!--<input type="text" class="form-control" name="asset_name" placeholder="name" id="name">-->
                </div>

                <div class="form-group  col-md-6 {{ $errors->has('department_id') ? 'has-error' : '' }}">
                    <label for="department_id" class="control-label">{{Lang::get('service::lang.department')}}</label>
                    {!! Form::select('department_id',$departments,null,['class'=>'form-control']) !!}

                </div>
                <div class="form-group col-md-6 {{ $errors->has('impact_type_id') ? 'has-error' : '' }}">
                    <label class="control-label">{{Lang::get('service::lang.impact_type')}}</label>    
                    {!! Form::select('impact_type_id',$sd_impact_types,null,['class'=>'form-control']) !!}

                </div> 
                <div class="col-md-6 form-group {{ $errors->has('organization') ? 'has-error' : '' }}">
                    {!! Form::label('organization',Lang::get('service::lang.organization')) !!}
                    {!! Form::select('organization',[''=>'Select','Organizations'=>$organizations],null,['class'=>'form-control','id'=>'org']) !!}
                </div>
                <div class="form-group col-md-6 {{ $errors->has('location_id') ? 'has-error' : '' }}">
                    <label class="control-label">{{Lang::get('service::lang.location')}}  <span class="text-red"> *</span></label>
                    {!! Form::select('location_id',[''=>'Select','Location'=>$sd_locations],null,['class'=>'form-control','id'=>'location']) !!}

                </div> 
                <div class="form-group col-md-6 {{ $errors->has('managed_by') ? 'has-error' : '' }}">
                    <label class="control-label">{{Lang::get('service::lang.managed_by')}}</label>
                    {!! Form::select('managed_by',$users,null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group col-md-6 {{ $errors->has('used_by') ? 'has-error' : '' }}">
                    <label class="control-label">{{Lang::get('service::lang.used_by')}}</label>    
                    {!! Form::select('used_by',$users,null,['class'=>'form-control']) !!}

                </div> 
                <div class="form-group col-md-6 {{ $errors->has('product_id') ? 'has-error' : '' }}">
                    <label class="control-label">{{Lang::get('service::lang.product')}}</label>    
                    {!! Form::select('product_id',$products,null,['class'=>'form-control']) !!}

                </div> 
                <div class="form-group col-md-6 {{ $errors->has('asset_type_id') ? 'has-error' : '' }}">

                    <label class="control-label">{{Lang::get('service::lang.asset_type')}}</label>  
                    {!! Form::select('asset_type_id',$sd_asset_types,null,['class'=>'form-control','id'=>'type']) !!}

                </div>
                <div class="form-group col-md-6 {{ $errors->has('assigned_on') ? 'has-error' : '' }}">
                    <label class="control-label">{{Lang::get('service::lang.assigned_on')}}</label>
                    {!! Form::text('assigned_on',null,['class'=>'form-control','id'=>'date']) !!}
                    <!--<input type="text" class="form-control" name="date" id="assignedDate">-->

                </div>
                <div class="form-group col-md-12 {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="internal_notes" class="control-label">{{Lang::get('service::lang.description')}} <span class="text-red"> *</span></label>
                    {!! Form::textarea('description',null,['class'=>'form-control']) !!}
                    <!--<textarea class="form-control textarea" placeholder="Description" name="description" rows="7" id=""style="width: 97%; margin-left:14px;"></textarea>-->
                </div>

                <!--asset type form-->



                

                <!--asset end-->

                <div class="form-group col-md-12 {{ $errors->has('attachments') ? 'has-error' : '' }}">
                    <label for="internal_notes" class="control-label">{{Lang::get('service::lang.attachment')}}</label>
                    {!! Form::file('attachments[]',null,['multiple'=>true]) !!}

                </div>
                {!! Form::close() !!}
<div id="asset-form" class="row form-group" ng-show="assetsView" style="padding-left: 0px;padding-right: 0px;">
                           
    <div class="box-body">
        <script type="text/ng-template" id="nodes_renderer2.html">
          <ng-form name="faveoClientForm">
          <div class="row" style="margin:15px;width:100%">
             
                 <label>@{{node.label}}<span ng-show="node.required==true" style="color:red">*</span></label>
             
                <input type="text" name="textfield@{{$index}}"  ng-if="node.type=='text'" class="form-control" style="border-radius: 0px;width:85%" ng-model="node.value" ng-required="@{{node.required}}" placeholder="@{{node.placeholder}}">
                <span style="color:red" ng-show="faveoClientForm.textfield@{{$index}}.$dirty && faveoClientForm.textfield@{{$index}}.$invalid">
                                          <span ng-show="faveoClientForm.textfield@{{$index}}.$error.required">@{{node.label}} is required.</span>
                </span>
                <input type="text" name="numberfield@{{$index}}"  ng-if="node.type=='number'" class="form-control numberOnly" style="border-radius: 0px;width:85%" ng-model="node.value" ng-required="@{{node.required}}" placeholder="@{{node.placeholder}}">
                <span style="color:red" ng-show="faveoClientForm.numberfield@{{$index}}.$dirty && faveoClientForm.numberfield@{{$index}}.$invalid">
                                          <span ng-show="faveoClientForm.numberfield@{{$index}}.$error.required">@{{node.label}} is required.</span>
                </span>
                <input type="text"  name="datefield@{{$index}}" ng-if="node.type=='date'" class="form-control" style="border-radius: 0px;width:85%" ng-pattern="/^[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[9]{1}\d{1})|([2-9]{1}\d{3}))$/" ng-required="@{{node.required}}" placeholder="MM/DD/YYYY" ng-model="node.value">
                <span style="color:red" ng-show="faveoClientForm.datefield@{{$index}}.$dirty && faveoClientForm.datefield@{{$index}}.$invalid">
                                          <span ng-show="faveoClientForm.datefield@{{$index}}.$error.required">@{{node.label}} is required.</span>
                                          <span ng-show="faveoClientForm.datefield@{{$index}}.$error.pattern">Please Enter Valid Correct Format-MM/DD/YYYY</span>
                </span>
                <div class="input-group" ng-if="node.type=='email'" style="width:100%">
                           <input type="email" name="requsEmail@{{$index}}" class="form-control" style="border-radius:0;margin-top:10px;width:85%" ng-model="req.email" ng-pattern="emailFormat" placeholder="@{{node.placeholder}}" id="requesterEmail" ng-required="@{{node.required}}"/>
                                <span style="color:red" ng-show="faveoClientForm.requsEmail@{{$index}}.$dirty && faveoClientForm.requsEmail@{{$index}}.$invalid">
                                          <span ng-show="faveoClientForm.requsEmail@{{$index}}.$error.required">Email is required.</span>
                                          <span ng-show="faveoClientForm.requsEmail@{{$index}}.$error.pattern">Invalid email address.</span>
                                </span>   
                </div>
                <textarea name="description0"  class="form-control" ng-if="node.type=='textarea'" style="border-radius: 0px;width:85%" ng-model="node.value" ng-required="@{{node.required}}" placeholder="@{{node.placeholder}}"></textarea>
                <select  ng-model="node.value"    name="selected@{{$index}}" ng-if="node.type=='select'" ng-options="option.optionvalue for option in node.options" class="form-control" style="border-radius: 0px;width:85%" ng-required="@{{node.required}}">
                  <option value="">@{{node.placeholder}}</option>
                </select>
                <span style="color:red" ng-show="faveoClientForm.selected@{{$index}}.$dirty && faveoClientForm.selected@{{$index}}.$invalid">
                  <span ng-show="faveoClientForm.selected@{{$index}}.$error.required">@{{node.label}} is required.</span>
               </span>
                <ul class="list-group" ng-if="node.type=='radio'" style="border:none">
                      <li ng-repeat="option in node.options"  class="list-group-item" style="border:none">
                                          <input type="radio" name="selection@{{$parent.$index}}" id="happy@{{$index}}" ng-model="node.value" value="@{{option}}" />
                                            <label for="happy@{{$index}}">@{{option.optionvalue}}</label>
                      </li>
                </ul>
                <ul class="list-group" ng-if="node.type=='checkbox'" style="border:none">
                      <li ng-repeat="option in node.options"  class="list-group-item" style="border:none">
                                          <input type="checkbox" name="selection@{{$parent.$index}}@{{$index}}" id="happy" ng-model="node.value" value="@{{option}}" ng-click="checkboxValue(option)">
                                            <label for="selection@{{$parent.$index}}@{{$index}}">@{{option.optionvalue}}</label>
                      </li>
                </ul>
            
              <div class="col-sm-12"  ng-repeat="option in node.options" ng-if="option.nodes.length>0 && node.value">
                  <ul ng-model="option.nodes" ng-class="{hidden: collapsed}" style="list-style-type:none;margin-left: -70px" ng-if="option==node.value">
                      <li  ng-repeat="node in option.nodes" ng-include="'nodes_renderer2.html'" class="col-sm-6" style="padding-left: 0px;padding-right: 0px;">
                    </li>
                  </ul>
              </div>

       
          </div>
          <ul  ng-model="node.nodes" ng-class="{hidden: collapsed}" style="list-style-type:none">
            <li ng-repeat="node in node.nodes"  ng-include="'nodes_renderer2.html'">
            </li>
          </ul>
          </ng-form>
        </script>
        <div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
            <form name="assetForm">
            <label style="margin-left:20px">Title:@{{FBTitile}}</label>
            <div class="row" style="margin-right:0px">
                
                    <ul  ng-model="tree3"  style="list-style-type:none">
                        <li ng-repeat="node in tree3"  ng-include="'nodes_renderer2.html'" class="col-sm-6" style="padding-left: 0px;padding-right: 0px;">
                            
                        </li>
                    </ul>

            </div> 
            
            <div class="from-group" style="margin-left: 25px">
                <input type="submit" value="{{Lang::get('service::lang.create')}}" ng-disabled="assetForm.$invalid" class="btn btn-primary" id="submit" ng-click="getEditor($event,requesterName)">     
            </div>
           </form>
        </div>
    </div>
            
</div>

            <!--        end row-->

        </div>

        <div class="box-footer" ng-hide="assetsView">
            <div class="from-group">
                <input type="submit" value="{{Lang::get('service::lang.create')}}" class="btn btn-primary" id="submit" ng-click="getEditor($event,requesterName)">     
            </div>
        </div>
    </div>
</div>
@stop
@section('FooterInclude')
<script src="{{asset("lb-faveo/plugins/moment-develop/moment.js")}}" type="text/javascript"></script>
<script src="{{asset("lb-faveo/js/bootstrap-datetimepicker4.7.14.min.js")}}" type="text/javascript"></script>
<script type="text/javascript">
                    $(function () {
                        $('#date').datetimepicker({
                            format: 'YYYY/MM/DD'
                        });
                    });
</script>
<script>
$(document).ready(function() {
    $(".numberOnly").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
    $(document).ready(function(){
        var org = $("#org").val();
        send(org);
        $("#org").on('change',function(){
            org = $("#org").val();
            send(org);
        });
        function send(org){
            $.ajax({
                dataType : 'html',
                type : 'GET',
                url : "{{url('service-desk/location/org')}}",
                data : {'org':org},
                success : function(response){
                    $("#location").html(response);
                }
            });
        }
    });
    // $(document).ready(function () {
    //     var type = $("#type").val();
    //     asset_type(type);
    //     $("#type").on('change', function () {
    //         type = $("#type").val();
    //         asset_type(type);
    //     });

    //     function asset_type(type) {
    //         $.ajax({
    //             url: "{{url('service-desk/asset-types/form')}}",
    //             dataType: "html",
    //             data: {'asset_type': type},
    //             success: function (response) {
    //                 $("#asset-form").html(response);
    //             },
    //             error: function (response) {
    //                 $("#asset-form").html(response);
    //             }
    //         });
    //     }
    // });
    // function submit() {
    //     //var form = new FormData($("#asset-type-form"));
    //     var form = $("#asset-type-form").serialize();
    //     console.log(form);
    //    // 
    //     $.ajax({
    //             url: "{{url('service-desk/assets/create')}}",
    //             dataType: "json",
    //             type:"post",
    //             data: form,
    //             success: function (json) {
    //                 console.log(json.result);
    //                 var res = "";
    //                 $.each(json.result, function (idx, topic) {
    //                     if (idx === "success") {
    //                         res = "<div class='alert alert-success'>" + topic + "</div>";
    //                     }
    //                     if (idx === "fails") {
    //                         res = "<div class='alert alert-danger'>" + topic + "</div>";
    //                     }
    //                 });

    //                 $("#response").html(res);
    //              setInterval(function(){
    //               if($('.alert-success').html()){
    //                 $('.alert-success').slideUp( 3000, function() {});
    //             }
                  
    //                 }, 2000);
    //                 $('html, body').animate({ scrollTop: 0}, 500);
    //             },
    //             error: function (json) {
    //                 var res = "";
    //                 $.each(json.responseJSON, function (idx, topic) {
    //                     res += "<li>" + topic + "</li>";
    //                 });
    //                 $("#response").html("<div class='alert alert-danger'><strong>Whoops!</strong> There were some problems with your input.<br><br><ul>" + res + "</ul></div>");
                 
    //                 $('html, body').animate({ scrollTop: 0}, 500);
    //             }
    //         });
    // }
</script>

@stop
@push('scripts')
<script>
app.controller('CreateTicketServicedesk', function($scope,$http, $sce,$window,$compile,Upload){
    $scope.disable=true;
      $scope.inlineImage=true;
      $scope.arrayImage=[];
      $scope.attachmentImage=[];
      $scope.inlinImage=[];
      $scope.editorValues={};
         $scope.tree3 = [];
        $scope.uploadArray=[];
        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    $(document).ready(function(){
        var asset_type_id=$("#type").val();
       $http({
                 url: "{{url('service-desk/asset-types/form')}}", 
                 method: "GET",
                 params: {'asset_type':asset_type_id }
             }).success(function (data) {
                   if(data!=null){
                    $scope.tree3 = data.form;
                    $scope.FBTitile=data.title;
                    $scope.assetsView=true;
                }
                else{
                    $scope.assetsView=false;
                    $scope.editorValues={};
                }
             }).error(function(data) {
                    $scope.assetsView=false;
                    $scope.editorValues={};
             });
      })
       $("#type").on('change', function () {
            var type = $("#type").val();
            $http({
                 url: "{{url('service-desk/asset-types/form')}}", 
                 method: "GET",
                 params: {'asset_type': type}
             }).success(function (data) {
                if(data!=null){
                    $scope.tree3 = data.form;
                    $scope.FBTitile=data.title;
                    $scope.assetsView=true;
                }
                else{
                    $scope.assetsView=false;
                    $scope.editorValues={};
                }
             }).error(function(data) {
                    $scope.assetsView=false;
                    $scope.editorValues={};
             });
            
        });
        
      $scope.getEditor=function(x){
       var form1=angular.copy($scope.tree3);
       var form = $("#asset-type-form").serialize();  
       if($scope.tree3!=null) {
          $scope.tree3.forEach(function (k) {
               for(var i in $scope.tree3){
                if($scope.tree3[i].label==k.label){
                   if($scope.tree3[i].label!=$scope.tree3[i].title){
                        $scope.tree3[i].label=k.label+'_'+i; 
                  }
                }
              }
            })
           
           for(var i in $scope.tree3) {
                var a=$scope.tree3[i].label;
                var b=a.split('_');
                if($scope.tree3[i].type=='select'){
                    $scope['formDetails'+i]=[];
                    var array=$scope['formDetails'+i];
                  array[b[0]]=$scope.tree3[i].value.optionvalue;
                  $scope.editorValues[$scope.tree3[i].label]=array;
                }
                else{
                  $scope['formDetails'+i]=[];
                  var array=$scope['formDetails'+i];
                  array[b[0]]=$scope.tree3[i].value;
                  console.log(array);
                  $scope.editorValues[$scope.tree3[i].label]=array;
               } 
            }
        }
        console.log($scope.editorValues);
          $scope.editorValues['EditJson']=form1;
          console.log($scope.editorValues);
          console.log(form);
          $scope.uploadArray.upload = Upload.upload({
                    url: "{{url('service-desk/assets/post/create')}}?"+form,
                    data: $scope.editorValues,
                }).success(function(data){
           var res = "";
                    $.each(data.result, function (idx, topic) {
                        if (idx === "success") {
                            res = "<div class='alert alert-success'>" + topic + "</div>";
                        }
                        if (idx === "fails") {
                            res = "<div class='alert alert-danger'>" + topic + "</div>";
                        }
                    });

                    $("#response").html(res);
                 setInterval(function(){
                  if($('.alert-success').html()){
                    $('.alert-success').slideUp( 3000, function() {});
                }
                  
                    }, 2000);
                    $('html, body').animate({ scrollTop: 0}, 500);
          })
          .error(function(data){
                 var res = "";
                    $.each(data, function (idx, topic) {
                        res += "<li>" + topic + "</li>";
                    });
                    $("#response").html("<div class='alert alert-danger'><strong>Whoops!</strong> There were some problems with your input.<br><br><ul>" + res + "</ul></div>");
                 
                    $('html, body').animate({ scrollTop: 0}, 500);
                })
          
    }
            
    
});
</script>
@endpush