<?php

return [
    'name' => 'Social',
    'description' => 'Faveo Social is a plugin to provide Social authentication',
    'author' => 'ladybird',
    'website' => 'www.ithelpdesk.com',
    'version' => '1.0.0',
    'settings' => 'social/settings',
    
];

