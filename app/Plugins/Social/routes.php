<?php

Route::group(['middleware' => ['web', 'auth', 'roles']], function() {
    Route::group(['prefix' => 'facebook'], function() {
        Route::get('/', ['as' => 'facebook', 'uses' => 'App\Plugins\Social\Controllers\FacebookController@getFacebookAccess']);
        Route::get('/access', ['as' => 'facebook.access', 'uses' => 'App\Plugins\Social\Controllers\FacebookController@getAccessCode']);
        Route::post('/page/save', ['as' => 'facebook.page.save', 'uses' => 'App\Plugins\Social\Controllers\FacebookController@pageSave']);
        Route::get('/pages/messages', ['as' => 'facebook.page.save', 'uses' => 'App\Plugins\Social\Controllers\FacebookController@fetchMessages']);
    });
    Route::group(['prefix' => 'twitter'], function() {
        Route::get('/callback', ['as' => 'twitter.callback', 'uses' => 'App\Plugins\Social\Controllers\TwitterController@callBack']);
       
        Route::get('/messages/show', ['as' => 'twitter.message.show', 'uses' => 'App\Plugins\Social\Controllers\Core\SettingsController@showMessage']);
        Route::get('/tweets/reply', ['as' => 'twitter.message.show', 'uses' => 'App\Plugins\Social\Controllers\TwitterController@replyTweet']);
        Route::post('/save', ['as' => 'twitter.save', 'uses' => 'App\Plugins\Social\Controllers\TwitterController@save']);
    
    });
    Route::get('social/settings', ['as' => 'social.settings', 'uses' => 'App\Plugins\Social\Controllers\Core\SettingsController@settings']);
     \Event::listen('Reply-Ticket',function($event){
        $settings = new \App\Plugins\Social\Controllers\Core\SettingsController();
        $controller = new \App\Plugins\Social\Controllers\Core\TwitterTicketController($settings);
        $controller->replyTwitter($event);
    });
    \Event::listen('Reply-Ticket',function($event){
        
        $controller = new \App\Plugins\Social\Controllers\FacebookController();
        $controller->replyPageMessage($event);
    });
    
    
    
});
\Event::listen('ticket.fetch',function($event){
        $controller = new \App\Plugins\Social\Controllers\Core\SettingsController();
        $controller->fetchSocialTicket($event);
});
Route::group(['middleware' => ['web']], function() {
 Route::get('facebook/page', [
     'as' => 'facebook.page', 
     'uses' => 'App\Plugins\Social\Controllers\Core\FacebookTicketController@createNewTicketByFacebookPageMsg'
     ])->middleware(['cron.url']);
 Route::get('twitter/tweets', [
     'as' => 'twitter.tweet', 
     'uses' => 'App\Plugins\Social\Controllers\Core\TwitterTicketController@createNewTicketByTweet'
     ])->middleware(['cron.url']);
 Route::get('twitter/messages', [
     'as' => 'twitter.message', 
     'uses' => 'App\Plugins\Social\Controllers\Core\TwitterTicketController@createNewTicketByTwitterMsg'
     ])->middleware(['cron.url']);
});  
