<?php
return [
    'social-integration'=>'Social Integrate',
    'integrate'=>'Integrate',
    'choose-a-page'=>'Choose A Page',
    'your-are-not-amin-of-any-pages'=>'You are not admin of any pages',
    'connect-to-your-facebook-account'=>'Connect to your facebook account',
    'facebook'=>'Facebook',
    'get-access-from-facebook'=>'Get Access from facebook',
    'you-havent-choosen-any-pages'=>'You haven\'t choosen any pages',
    'save'=>'Save',
    'twitter'=>'Twitter',
    'client-id'=>'Client Id',
    'client-secrete'=>'Client Secrete',
    'redirect-url'=>'Redirect Url',
    'access-token'=>'Access Token',
    'access-secrete'=>'Access secrete',
    'query'=>'Query',
    'app-id'=>'App ID',
    'app-secrete'=>'App Secrete',
];

