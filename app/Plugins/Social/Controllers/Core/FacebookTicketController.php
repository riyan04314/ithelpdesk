<?php

namespace App\Plugins\Social\Controllers\Core;

use App\Http\Controllers\Controller;
use App\Plugins\Social\Controllers\Core\SettingsController;

class FacebookTicketController extends Controller {

    protected $settings;

    public function __construct(SettingsController $settings) {
        $this->settings = $settings;
    }

    public function ticketController() {
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $NotificationController = new \App\Http\Controllers\Common\NotificationController();
        $ticket_controller = new \App\Http\Controllers\Agent\helpdesk\TicketController($PhpMailController, $NotificationController);
        return $ticket_controller;
    }

    public function createTicket($social_fields, $subject = "Message from Facebook", $provider = "facebook", $via = 'page_message', $page = "") {
        $result = "";
        $ticket_controller = $this->ticketController();
            foreach ($social_fields as $social_field) {
                $users = $this->settings->getUser($social_field);
                $email = $this->settings->checkArray('email', $users);
                $username = $this->settings->checkArray('username', $users);
                $userid = $this->settings->checkArray('user_id', $users);
                $body = $this->settings->checkArray('message', $social_field);

                $con_id = $this->settings->checkArray('con_id', $social_field);
                $message_id = $this->settings->checkArray('message_id', $social_field);
                $name = $this->settings->checkArray('name', $social_field);
                $posted_at = $this->settings->checkArray('posted_at', $social_field);
                //$priority = $this->settings->getSystemDefaultPriority();
                $department = $this->settings->getSystemDefaultDepartment();
                $helptopic = $this->settings->getSystemDefaultHelpTopic();

                $content = "$name : $body<br>" . $this->convertDate($posted_at);

                $phone = "";
                $phonecode = "";
                $mobile = "";
                $source = $ticket_controller->getSourceByname($provider)->id;
                $headers = [];
                $assignto = NULL;
                $from_data = [];
                $auto_response = "";
                $status = "";
                $subject = $page . " (Facebook) from " . $name;
//            $sla = sla("", "", $department, $source);
//            if ($sla) {
//                $priority = priority($sla);
//            }
                $sla = "";
                $priority = "";
                $type = "";
                $ticket_type = \App\Model\helpdesk\Manage\Tickettype::select('id')->first();
                if ($ticket_type) {
                    $type = $ticket_type->id;
                }
                if ($this->messageIdExists($message_id) == false) {
                    if ($this->checkReply($provider, $con_id, $page)) {
                        $user = $this->checkUsers($username, $email);
                        $userid = "";
                        $result = $this->reply($provider, $page, $body, $userid, $con_id, $name, $posted_at);
                        $this->socialFacebook($social_field, $provider, $page, $result->ticket_id);
                    } else {
                        $result = $ticket_controller->create_user($email, $username, $subject, $content, $phone, $phonecode, $mobile, $helptopic, $sla, $priority, $source, $headers, $department, $assignto, $from_data, $auto_response, $status, $type);
                    }

                    if (is_array($result)) {
                        $this->insertInfo($social_field, $result, $provider, $page);
                    }
                }
            }
            \Log::info("Facebook has read");
    }

    public function createNewTicketByFacebookPageMsg() {
        $social_fields = $this->settings->facebookPageMessages();
        if (count($social_fields) > 0) {
            foreach ($social_fields as $page => $social_field) {
                $social = $this->array_sort($social_field, 'posted_at');
                $subject = "Facebook message from ";
                $provider = "facebook";
                $via = "page_message";
                $this->createTicket($social, $subject, $provider, $via, $page);
            }
        }
    }

    public function insertInfo($info, $result, $provider, $via) {
        $userid = $this->findUserFromTicketCreateUserId($result);
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $guest_controller = new \App\Http\Controllers\Client\helpdesk\GuestController($PhpMailController);
        $user = $this->settings->getUser($info);
        //dd($info);
        $guest_controller->update($userid, $user, $provider);

        $this->socialChannel($info, $result, $provider, $via);
    }

    public function reply($provider, $via, $body, $userid, $con_id, $name, $posted_at) {

        $ticket_id = $this->getTicketIdForReply($provider, $via, $con_id);

        $uri = url('/');
        $method = "POST";
        $parameters = [
            'content' => "$name : $body<br>$posted_at",
            'do-not-send' => true,
        ];
        $request = \Illuminate\Http\Request::create($uri, $method, $parameters, [], ['attachment' => [0 => ""]]);
        $ticket_controller = $this->ticketController();
        $result = $ticket_controller->reply($request, $ticket_id, false, false, $userid, false);
        return $result;
    }

    public function getTicketIdForReply($provider, $via, $con_id) {

        $ticket_id = "";
        $channels = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $channels->where('channel', $provider)->where('via', $via)->where('con_id', $con_id)->first();
        if ($channel) {
            $ticket_id = $channel->ticket_id;
        }
        return $ticket_id;
    }

    public function checkReplyFacebookPageMsg($con_id, $page) {
        $check = false;
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $social_channel->getChannelMessageid('facebook', $page, $con_id);
        if ($channel) {
            $check = true;
        }
        return $check;
    }

    public function checkReply($provider, $con_id, $page) {
        //dd($provider);
        switch ($provider) {

            case "facebook":
                return $this->checkReplyFacebookPageMsg($con_id, $page);
        }
    }

    public function socialChannel($info, $result, $provider, $via) {
        $users = $this->settings->getUser($info);
        $array['channel'] = $provider;
        $array['via'] = $via;
        $array['message_id'] = $this->settings->checkArray('message_id', $info);
        $array['user_id'] = $this->settings->checkArray('user_id', $users);
        $array['ticket_id'] = $this->lastTicket($result);
        $array['username'] = $this->settings->checkArray('username', $users);
        $array['posted_at'] = $this->settings->checkArray('posted_at', $info);
        $array['con_id'] = $this->settings->checkArray('con_id', $info);
        $this->updateSocialChannel($array);
    }

    public function socialFacebook($info, $provider, $via, $ticket_id) {
        $con_id = $this->settings->checkArray('con_id', $info);
        $users = $this->settings->getUser($info);
        $array['channel'] = $provider;
        $array['via'] = $via;
        $array['message_id'] = $this->settings->checkArray('message_id', $info);
        $array['user_id'] = $this->settings->checkArray('user_id', $users);
        $array['ticket_id'] = $ticket_id;
        $array['username'] = $this->settings->checkArray('username', $users);
        $array['posted_at'] = $this->settings->checkArray('posted_at', $info);
        $array['con_id'] = $con_id;
        $this->updateSocialChannel($array);
    }

    public function ticketIdByMessageId($channel, $via, $msg_id) {
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $social = $social_channel->getChannelMessageid($channel, $via, $msg_id);
        if ($social) {
            $ticket_id = $social->ticket_id;
            return $ticket_id;
        }
    }

    public function lastTicket($result) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        //$threads = new \App\Model\helpdesk\Ticket\Ticket_Thread();
        if ($ticket) {
            $ticket_id = $ticket->id;
            //$thread_id = $threads->where('ticket_id', $ticket_id)->max('id');
            return $ticket_id;
        }
    }

    public function updateSocialChannel($array) {
        $social_channel = new \App\Plugins\Social\Model\SocialChannel();
        $social_channel->create($array);
    }

    public function findTicketFromTicketCreateUser($result = []) {
        $ticket_number = $this->settings->checkArray('0', $result);
        if ($ticket_number !== "") {
            $tickets = new \App\Model\helpdesk\Ticket\Tickets();
            $ticket = $tickets->where('ticket_number', $ticket_number)->first();
            if ($ticket) {
                return $ticket;
            }
        }
    }

    public function findUserFromTicketCreateUserId($result = []) {
        $ticket = $this->findTicketFromTicketCreateUser($result);
        if ($ticket) {
            $userid = $ticket->user_id;
            return $userid;
        }
    }

    public function getLatestTicketId($ticketid) {
        $threads = new \App\Model\helpdesk\Ticket\Ticket_Thread();
        $thread = $threads->where('ticket_id', $ticketid)->max('id');
        return $thread;
    }

    public function getChannel($ticketid) {
        $channels = new \App\Plugins\Social\Model\SocialChannel();
        $channel = $channels->where('ticket_id', $ticketid)->first();
        //dd($channel);
        return $channel;
    }

    public function checkUsers($username, $email) {

        $users = new \App\User();
        $user = $users->where('user_name', $username)->orWhere('email', $email)->first();
        if ($user) {
            return $user;
        }
    }

    public function getAdmin() {
        $users = new \App\User();
        $admin = $users->where('role', 'admin')->first();
        return $admin;
    }

    public function checkPageName($name) {
        $check = false;
        $controller = new \App\Plugins\Social\Controllers\FacebookController();
        $pageName = $controller->getPageName();
        if (in_array($name, $pageName)) {
            $check = true;
        }
        return $check;
    }

    public function messageIdExists($message_id) {
        $check = false;
        $channel = new \App\Plugins\Social\Model\SocialChannel();
        $msg = $channel->where('message_id', $message_id)->first();
        if ($msg) {
            $check = true;
        }
        return $check;
    }

    public function convertDate($date) {
        $time = strtotime($date);
        $newformat = date('l jS \of F Y h:i:s A', $time);
        return $newformat;
    }

    public function array_sort($array, $on, $order = SORT_ASC) {

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

}
