<?php
return [
       // Licenses 
     'licenses'                     => 'Licenses',
     'licenses_key'=>'Licenses key',
     'licenses_type'=>'Licenses Type',
     'activated_on'=>'Activated on',
     'start_date'=>'Start Date',
     'expires_on'=>'Expires on',


     'create_licenses'=>'Create Licenses',
     'key_generator'=>'key Generator',
     'licenses_successfully_created'=>'Licenses successfully created',
     'licenses_successfully_edit'=>'licenses_ successfully edit',
     'make-default-licenses'=>'Make default licenses',
     'action'=>'Action',
     'submit'=>'Submit',
     'comments'=>'Comments',
     'organization'=>'Organization',
     'edit'=>'Edit',
     'licenses_delete_successfully'=>'licenses Delete successfully',
     'name'=>'Name',
     'created_date'=>'Created date',
     'your_status_updated'=>'Your Status Updated',
];
