


<?php

\Event::listen('licenses.show',function($org_id){

	// dd($org_id);
    $controller = new App\Plugins\Licenses\Controllers\LicensesController;
	 echo $controller->show($org_id);

  //   echo 
  // Route::get('licenses/create', ['as' => 'licenses.show11', 'uses' => ' new App\Plugins\Licenses\Controllers\LicensesController@show']);

});

// \Event::listen('create.licenses',function($org_id){

// 	// dd($org_id);
//     $controller = new App\Plugins\Licenses\Controllers\LicensesController;
// 	 echo $controller->licensesCreate($org_id);


// });




/**
 * Auth route
 */
Route::group(['middleware' =>'web'], function() {
    Route::get('/organizations/index',['as'=>'organizations.index1','uses'=>'App\Plugins\Htrunk\Controllers\OrganizationsController@index']);
     Route::get('org-list', ['as' => 'org.list', 'uses' => 'Agent\helpdesk\OrganizationController@org_list']);


    Route::get('organizations/licenses/create/{org_id}', ['as' => 'organizations.create.licenses', 'uses' => 'App\Plugins\Licenses\Controllers\LicensesController@create']);

     Route::post('organizations/licenses/create1', ['as' => 'org.licenses.create1', 'uses' => 'App\Plugins\Licenses\Controllers\LicensesController@licensesCreate1']);

       Route::get('org/licenses/edit/{id}', ['as' => 'org.licenses.edit', 'uses' => 'App\Plugins\Licenses\Controllers\LicensesController@licensesEdit']);



             Route::post('org/licenses/edit1', ['as' => 'org.licenses.edit1', 'uses' => 'App\Plugins\Licenses\Controllers\LicensesController@licensesEdit1']);

              Route::get('org/licenses/{id}/destroy', ['as' => 'org.licenses.destroy', 'uses' => 'App\Plugins\Licenses\Controllers\LicensesController@destroyLicenses']);
// 



Route::get('licenses/settings',['as'=>'htrunk.settings.get','uses'=>'App\Plugins\Licenses\Controllers\SettingsController@index']);
 Route::get('generate/licenses/keys', ['as' => 'generate.licenses.keys', 'uses' => 'App\Plugins\Licenses\Controllers\LicensesController@licensesKey']);



Route::post('licenses/postsettings',['as'=>'licenses.settings.post','uses'=>'App\Plugins\Licenses\Controllers\SettingsController@update']);



});

 
