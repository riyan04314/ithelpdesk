@extends('themes.default1.agent.layout.agent')

@section('Manage')
active
@stop

@section('manage-bar')
active
@stop

@section('licenses')
class="active"
@stop

@section('HeadInclude')
@stop
<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('lang.licenses') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop

<!-- /breadcrumbs -->
<!-- content -->
@section('content')
<!-- open a form -->
<form action="{!!URL::route('org.licenses.edit1')!!}" method="post" role="form">
{{ csrf_field() }}
    <input type="hidden" name="licenses_id" value="{{$licenses->id}}">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">{{Lang::get('Licenses::lang.edit')}}</h2>
        </div>
        <div class="box-body">
            @if(Session::has('errors'))

            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <b>Alert!</b>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <br/>
                @if($errors->first('licenses_type'))
                <li class="error-message-padding">{!! $errors->first('licenses_type', ':message') !!}</li>
                @endif
                @if($errors->first('licenses_key'))
                <li class="error-message-padding">{!! $errors->first('licenses_key', ':message') !!}</li>
                @endif
                @if($errors->first('activated_on'))
                <li class="error-message-padding">{!! $errors->first('activated_on', ':message') !!}</li>
                @endif
                @if($errors->first('start_date'))
                <li class="error-message-padding">{!! $errors->first('start_date', ':message') !!}</li>
                @endif
                @if($errors->first('expires_on'))
                <li class="error-message-padding">{!! $errors->first('expires_on', ':message') !!}</li>
                @endif


            </div>
            @endif
            <!-- <table class="table table-hover" style="overflow:hidden;"> -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('licenses_type') ? 'has-error' : '' }}">
                        {!! Form::label('licenses_type',Lang::get('Licenses::lang.licenses_type')) !!} <span class="text-red"> *</span>
                        <input type="text" class="form-control" name="licenses_type" value=" {{$licenses->licenses_type}}" >
                    </div>
                </div>
                <!-- Grace Period text form Required -->
                <div class="col-md-6">

                    <div class="form-group {{ $errors->has('licenses_key') ? 'has-error' : '' }}">
                        {!! Form::label('start_date',Lang::get('Licenses::lang.licenses_key')) !!}<span class="text-red"> *</span>

                       <!--  <span class="btn btn-info pull-right" id="licenseskey">

                            <i class="fa fa-refresh fa-spin"></i>


                            {{Lang::get('lang.key_generator')}}</span> -->

                        <input type="text" name="licenses_key" id="licenses_key" class="form-control" value=" {{$licenses->licenses_key}}" readonly>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('activated_on') ? 'has-error' : '' }}">

                        {!! Form::label('activated_on',Lang::get('Licenses::lang.activated_on')) !!} <span class="text-red"> *</span>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text"  name="activated_on" class="form-control" id="activated_on" value=" {{$licenses->activated_on}}">
                        </div>
                    </div>
                </div>
                <!-- Grace Period text form Required -->
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                        {!! Form::label('start_date',Lang::get('Licenses::lang.start_date')) !!}<span class="text-red"> *</span>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" name="start_date" id="start_date" value=" {{$licenses->start_date}}">
                        </div>


                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('expires_on') ? 'has-error' : '' }}">
                        {!! Form::label('expires_on',Lang::get('Licenses::lang.expires_on')) !!} <span class="text-red"> *</span>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" name="expires_on" id="expires_on" value=" {{$licenses->expires_on}}">
                        </div>


                    </div>
                </div>

                 <div class="col-md-6">
                    <div class="form-group {{ $errors->has('expires_on') ? 'has-error' : '' }}">
                        {!! Form::label('org_id',Lang::get('Licenses::lang.organization')) !!} <span class="text-red"> *</span>

                        <div class="input-group date">
                            
                            <input type="text" class="form-control" name="org_name" value="{{$org_name->name}}" readonly >
                        </div>


                    </div>
                </div>
                </div>
 <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('comments',Lang::get('Licenses::lang.comments')) !!}
                        {!! Form::textarea('comments',$licenses->comments,['class' => 'form-control','size' => '30x5']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            {!! Form::submit(Lang::get('Licenses::lang.submit'),['class'=>'btn btn-primary'])!!}
        </div>
    </div>


    <!-- close form -->
    {!! Form::close() !!}

    <script>
        $(document).ready(function() {       //Error happens here, $ is not defined.


            $('#licenseskey').click(function() {
                $.ajax({
                    type: 'get',
                    url: '{{route("generate.licenses.keys")}}',
                    success: function(result) {
                        var sum = result;
                        document.getElementById("licenses_key").value = sum;
                    }
                });

            });
        });

    </script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
  $( function() {
    $( "#activated_on" ).datepicker({
        minDate: 0 ,
  
   onSelect: function (date) {
            var dt2 = $('#start_date');
   
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            dt2.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets dt2 maxDate to the last day of 30 days window
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
            // $(this).datepicker('option', 'minDate', minDate);
   
   
    var dt3 = $('#expires_on');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            dt3.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets dt2 maxDate to the last day of 30 days window
            dt3.datepicker('option', 'maxDate', startDate);
            dt3.datepicker('option', 'minDate', minDate);
            // $(this).datepicker('option', 'minDate', minDate);
   
   
   
   
   
        }
     
 
    });
  } );
  </script>


<script type="text/javascript">


$( function() {
$(document).ready(function () {
   $( "#start_date" ).datepicker({
    // $("#start_date").datepicker({
        // dateFormat: "dd-M-yy",
        minDate: 0,
        onSelect: function (date) {
            var dt2 = $('#expires_on');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            dt2.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets dt2 maxDate to the last day of 30 days window
            dt2.datepicker('option', 'maxDate', startDate);
            dt2.datepicker('option', 'minDate', minDate);
            // $(this).datepicker('option', 'minDate', minDate);
        }
    });
    });

    $( function() {
    $( "#expires_on" ).datepicker({
        minDate: 0 ,
   
 
    });
  } );
   
});


</script>

    @stop