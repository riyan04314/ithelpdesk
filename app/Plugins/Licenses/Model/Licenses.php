<?php namespace App\Plugins\Licenses\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\helpdesk\Agent_panel\Organization;

class Licenses extends Model {

	 protected $table = 'licenses';
    protected $fillable = [
        'id','org_id','licenses_type','licenses_key ','activated_on','start_date','expires_on','comments','created_at','updated_at'
    ];

}
