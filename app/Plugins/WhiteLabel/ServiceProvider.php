<?php

namespace App\Plugins\WhiteLabel;

class ServiceProvider extends \App\Plugins\ServiceProvider {

    public function register() {
        parent::register('WhiteLabel');
    }

    public function boot() {
        /** 
         *View
         */
        $view_path = app_path().DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.'WhiteLabel'.DIRECTORY_SEPARATOR.'views';
        $this->loadViewsFrom($view_path, 'WhiteLabel');
        parent::boot('WhiteLabel');
        
        // if (class_exists('Breadcrumbs')) {
        //     require __DIR__ . '/breadcrumbs.php';
        // }
        
        /**
         *language
         */
        $trans = app_path() . DIRECTORY_SEPARATOR . 'Plugins' . DIRECTORY_SEPARATOR . 'WhiteLabel' . DIRECTORY_SEPARATOR . 'lang';
        $this->loadTranslationsFrom($trans, 'WhiteLabel');
    }
    

}
