<?php

namespace App\Plugins\WhiteLabel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use Lang;

use App\Model\helpdesk\Settings\Plugin;

class SettingsController extends Controller {

    public function index() {


        $white_label_status=Plugin::where('name','=','WhiteLabel')->select('status')->first();
        return view('WhiteLabel::settings',compact('white_label_status'));
    }

    public function update(Request $request) {
        
        try {

            $change_status=$request->settings_licenses;
            $htrunk_status_update=Plugin::where('name','=','WhiteLabel')->update(['status' => $change_status]);
            return Lang::get('WhiteLabel::lang.your_status_updated');
            // return $this->successResponse('Updated Successfully');
        } catch (\Exception $ex) {

            return $this->failsResponse($ex->getMessage());
        }
    }

   

}
