<?php

namespace App\Plugins\Ldap\Model;

use Illuminate\Database\Eloquent\Model;
use Crypt;

class Ldap extends Model {

    protected $table = 'ldap';
    protected $fillable = ['domain', 'username', 'password', 'search_base', 'agent_auth', 'client_auth'];

    public function setPasswordAttribute($value) {

        $this->attributes['password'] = Crypt::encrypt($value);
    }

    public function getPasswordAttribute($value) {
        if ($value) {
            return Crypt::decrypt($value);
        }
    }

}
