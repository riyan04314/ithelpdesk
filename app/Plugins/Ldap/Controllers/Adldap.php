<?php

namespace App\Plugins\Ldap\Controllers;

use App\Http\Controllers\Controller;
use App\Plugins\Ldap\Utility;
use App\Plugins\Ldap\Model\Ldap;
use Exception;
use Illuminate\Http\Request;
use Input;
use App\User;
use Hash;
use Auth;

class Adldap extends Controller {

    protected $utility;
    protected $ldap;
    protected $user;

    public function __construct() {
        $utility = new Utility();
        $this->utility = $utility;
        try {
            $this->utility->configure();
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }

        $ldap = new Ldap();
        $this->ldap = $ldap->find(1);

        $user = new User();
        $this->user = $user;

        $this->middleware('auth');
    }

    public function settings() {
        try {
            $path = app_path() . '/Plugins/Ldap/views';
            \View::addNamespace('plugins', $path);
            $settings = $this->ldap;
            return view('plugins::settings', compact('settings'));
        } catch (Exception $ex) {
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    public function postSettings(Request $request) {
        try {

            $domain = $request->input('domain');
            $username = $request->input('username');
            $password = $request->input('password');
            $search = $request->input('search_base');

            $check = $this->checkLdap($domain, $username, $password, $search);
            if ($check == true) {
                $this->ldap->fill($request->input())->save();
                return $this->successResponse('saved successfully');
            } else {
                return $this->failsResponse("Can not find the server $domain");
            }
        } catch (Exception $ex) {

            return $this->failsResponse($ex->getMessage());
        }
    }

    public function loginForm() {
        try {
            return "<p><input type=checkbox name=ldap value=1> Authentication via ldap</p>";
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function authLdap() {
        $username = Input::get('email');
        $password = Input::get('password');
        $is_ldap_auth = Input::get('ldap');
        $domain = $this->ldap->domain;
        $search = $this->ldap->search_base;
        $user_name = $this->ldap->username;
        $user_password = $this->ldap->password;
        if($is_ldap_auth){
            $ldap = $this->checkLdap($domain, $user_name, $user_password, $search, true);            
            $auth = $ldap->auth()->attempt($username, $password,true);
            if($auth){
                $user = User::where('email',$username)->first();
                if(!$user){
                    $user = User::where('user_name',$username)->first();
                }
                if(!$user){
                    $user =  User::firstOrCreate([
                        'user_name'=>$username,
                        'role' => 'user',
                        'active' => 1,
                    ]);
                }
                Auth::login($user);
            }
            
        }
        
        
    }

    public function authLdapOld() {

        try {
            $username = Input::get('email');
            $password = Input::get('password');
            $domain = $this->ldap->domain;


            $user_check = $this->checkUser($username);
            if ($user_check == 'local') {
                $user = $this->getUser($username);
                if (Hash::check($password, $user->getAuthPassword())) {
                    Auth::login($user);
                    return redirect('/');
                }
            }
            if ($user_check == 'ldap') {
                $check = $this->checkLdap($domain, $username, $password);
                if ($check == FALSE) {
                    throw new Exception('Invalid ldap user');
                } else {
                    $user = $this->getUser($username);
                    Auth::login($user);
                    return redirect('home');
                }
            }
            if ($user_check == 'both') {
                $check = $this->checkLdap($domain, $username, $password);
                if ($check == TRUE) {
                    $user = $this->getUser($username);
                    Auth::login($user);
                    return redirect('home');
                }
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function register($username, $password, $role = 'user') {
        try {
            $this->user->first_name = $username;
            $this->user->last_name = $username;
            $this->user->user_name = $username;
            $this->user->password = Hash::make($password);
            $this->user->role = $role;
            $this->user->save();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function userIndex() {
        try {
            $path = app_path() . '/Plugins/Ldap/views';
            \View::addNamespace('plugins', $path);
            $users = $this->user
                    ->select('id', 'user_name', 'first_name', 'last_name', 'email', 'ldap_username')
                    ->simplePaginate(10);
            $users->setPath('users');
            return view('plugins::user-index', compact('users'));
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function users() {
        try {
            $search = Input::get('search');
            $users = $this->user
                    ->where('id', 'LIKE', '%' . $search . '%')
                    ->orWhere('user_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('first_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%')
                    ->orWhere('ldap_username', 'LIKE', '%' . $search . '%')
                    ->select('id', 'user_name', 'first_name', 'last_name', 'email', 'ldap_username')
                    ->get()
                    ->take(10);
            return response()->json($users);
        } catch (Exception $ex) {
            return json_encode($ex->getMessage());
        }
    }

    public function userEdit($id) {
        try {

            $user = $this->user->find($id);
            $team = new \App\Model\helpdesk\Agent\Teams();
            $team = $team->get();
            $teams1 = $team->pluck('name', 'id');
            $timezone = new \App\Model\helpdesk\Utility\Timezones();
            $timezones = $timezone->get();
            $group = new \App\Model\helpdesk\Agent\Groups();
            $groups = $group->get();
            $department = new \App\Model\helpdesk\Agent\Department();
            $departments = $department->get();
            $team_assign_agent = new \App\Model\helpdesk\Agent\Assign_team_agent();
            $table = $team_assign_agent->where('agent_id', $id)->first();
            $teams = $team->pluck('id', 'name');
            $assign = $team_assign_agent->where('agent_id', $id)->pluck('team_id');



            $path = app_path() . '/Plugins/Ldap/views';
            \View::addNamespace('plugins', $path);
            return view('plugins::user-edit', compact('teams', 'assign', 'table', 'teams1', 'selectedTeams', 'user', 'timezones', 'groups', 'departments', 'team', 'exp', 'counted'));
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function patchUser($id) {
        try {
            $user = $this->user->find($id);
            $v = \Validator::make(Input::all(), [
                        'ldap_username' => 'required|unique:users,ldap_username,' . $user->id,
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email',
                        'active' => 'required',
                        'role' => 'required',
                        'assign_group' => 'required',
                        'primary_dpt' => 'required',
                        'agent_tzone' => 'required',
            ]);

            if ($v->fails()) {
                return redirect()->back()->withErrors($v)->withInput();
            }

            $ldap_username = Input::get('ldap_username');
            $isldapauth = Input::get('isldapauth');
            $user->ldap_username = $ldap_username;
            $user->isldapauth = $isldapauth;
            $user->fill(Input::all())->save();
            return redirect()->back()->with('success', 'updated successfully');
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function checkRole($username) {
        try {
            $client = $this->ldap->client_auth;
            $agent = $this->ldap->agent_auth;
            if ($client == 1 && $agent == 1) {
                return true;
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function checkUser($username) {
        try {
            $user = $this->user->where('email', $username)->orWhere('user_name', $username)->first();
            if (!$user) {
                $user = $this->user->where('ldap_username', $username)->first();
            }

            if (!$user) {
                return FALSE;
            }
            $client = $this->ldap->client_auth;
            $agent = $this->ldap->agent_auth;
            if (($client == 1 && $user->role == 'user') || ($agent == 1 && $user->role == 'admin')) {
                $isldapauth = $user->isldapauth;

                if ($isldapauth == 0) {
                    return 'local';
                }
                if ($isldapauth == 1) {
                    return 'ldap';
                }
                if ($isldapauth == 2) {
                    return "both";
                }
            }
            return 'local';
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function getUser($username) {
        try {
            $user = $this->user->where('email', $username)->orWhere('user_name', $username)->first();
            if (!$user) {
                $user = $this->user->where('ldap_username', $username)->first();
            }
            return $user;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function checkLdap($doamin, $username, $password, $search = '', $class = false) {
        $respond = false;
        $ad = new \App\Plugins\Ldap\Adldap\Adldap();
        $config = [
            'domain_controllers'    => [$doamin],
            'base_dn'               => $search,
            'admin_username'        => $username,
            'admin_password'        => $password,
        ];

        $provider = new \App\Plugins\Ldap\Adldap\Connections\Provider($config);
        $ad->addProvider('default', $provider);
        $ad->connect('default');
        if ($class == true) {
            return $ad;
        }
        if ($ad) {
            $respond = true;
        }
        return $respond;
    }

    public function search($doamin, $username, $password, $search) {
        $ldap_host = $doamin; //Host LDAP
        $user = $username;
        $ldap_pass = $password;
        $base_dn = "";
        $ldap_user = "cn=" . $user;
        $search = "uid=$search*";
        $filter = "($search)"; // Just results for this user
        $ldap_conn = ldap_connect($ldap_host);
        dd($ldap_conn);
        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        if ($ldap_conn) {
            if ($bind = ldap_bind($ldap_conn, $ldap_user . "," . $base_dn, $ldap_pass)) {
                $read = ldap_search($ldap_conn, $base_dn, $filter);
                if (!$read) {
                    throw new Exception('Invalid search');
                }
                $info = ldap_get_entries($ldap_conn, $read);

                $ii = 0;
                for ($i = 0; $ii < $info[$i]["count"]; $ii++) {
                    $data = $info[$i][$ii];
                    dd($data);
                    if ($data == "givenname") {
                        $name = $info[$i][$data][0];
                    }
                }

                ldap_close($ldap_conn);
            } else {
                throw new Exception('Invalid user/dn');
            }
        } else {
            throw new Exception('Invalid domain');
        }
    }

    public function successResponse($message) {
        return "<div class='alert alert-success alert-dismissable'>
        <b>Success!</b>
        <button type=button class=close data-dismiss=alert aria-hidden=true>&times;</button>
        $message
    </div>";
    }

    public function failsResponse($message) {
        return "<div class='alert alert-danger alert-dismissable'>
        <b>Fails!</b>
        <button type=button class=close data-dismiss=alert aria-hidden=true>&times;</button>
        $message
    </div>";
    }

}
