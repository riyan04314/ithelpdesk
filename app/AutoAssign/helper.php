<?php

function autoAssignGetOptions($field){
    $common = new App\Model\helpdesk\Settings\CommonSettings();
    $status = $common->getOptionValue('auto_assign', $field);
    return $status;
}

function isAutoAssign(){
    $schema = autoAssignGetOptions('status');
    $check = false;
    if($schema && $schema->option_value==1){
        $check = true;
    }
    return $check;
}

function isOnlyLogin(){
    $schema = autoAssignGetOptions('only_login');
    $check = false;
    if($schema && $schema->option_value==1){
        $check = true;
    }
    return $check;
}
function isAssignIfNotAccept(){
    $schema = autoAssignGetOptions('assign_not_accept');
    $check = false;
    if($schema && $schema->option_value==1){
        $check = true;
    }
    return $check;
}

function thresold(){
    $schema = autoAssignGetOptions('thresold');
    $check = "";
    if($schema && $schema->option_value){
        $check = $schema->option_value;
    }
    return $check;
}
