<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Agent\helpdesk\MailController;
use Event;
use App\Http\Controllers\Agent\helpdesk\TicketWorkflowController;

class TicketFetch extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching the tickets from service provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if (env('DB_INSTALL') == 1) {
            try {
                $controller = $this->mailController();
                $emails = new \App\Model\helpdesk\Email\Emails();
                $settings_email = new \App\Model\helpdesk\Settings\Email();
                $system = new \App\Model\helpdesk\Settings\System();
                $ticket = new \App\Model\helpdesk\Settings\Ticket();
                $controller->readmails($emails, $settings_email, $system, $ticket);
                Event::fire('ticket.fetch', ['event' => '']);
                loging('fetching-ticket-cron', 'Ticket has read', 'info');
                //\Log::info('Ticket has read');
                $this->info('Ticket has read');
            } catch (\Exception $ex) {
                loging('fetching-ticket-cron', $ex->getMessage());
                $this->error($ex->getMessage());
            }
        }
    }

    public function mailController() {
        $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
        $NotificationController = new \App\Http\Controllers\Common\NotificationController();
        $ticket = new \App\Http\Controllers\Agent\helpdesk\TicketController($PhpMailController, $NotificationController);
        $work = new TicketWorkflowController($ticket);
        $controller = new MailController($work);
        return $controller;
    }

}
