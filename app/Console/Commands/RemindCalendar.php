<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class RemindCalendar extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is to send ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $execute = false;
            $PhpMailController = new \App\Http\Controllers\Common\PhpMailController();
            $controller = new \App\Plugins\Calendar\Controllers\CalendarRemindAndNotifyController($PhpMailController);
            $execute = $controller->main();
            if ($execute === 'true') {
                loging('remind-cron', 'Calendar event reminders have been sent to users.', 'info');
                $this->info('Calender reminder executed successfullly');
            }
        } catch (\Exception $ex) {
            loging('remind-cron', $ex->getMessage());
            $this->error($ex->getMessage());
        }
    }

}
