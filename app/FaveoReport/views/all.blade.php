@extends('themes.default1.admin.layout.admin')

@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

@section('HeadInclude')
@stop

@section('content')

<section class="content-header">
    <h1>{!! Lang::get('lang.report') !!}</h1>

</section>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<div class="box box-primary">
    <div class="box-header">
        <div class="btn-group">
            <input type="text" name="daterange" class="form-control" value="{{Carbon\Carbon::now()->subMonth()->format('m/d/Y')}}-{{Carbon\Carbon::now()->format('m/d/Y')}}" />
        </div>

        <div class="btn-group pull-right">
            @include('report::popup.export')
            <button class="btn btn-default period" value="today">{!! Lang::get('lang.today') !!}</button>
            <button class="btn btn-default period" value="week">{!! Lang::get('lang.last-7-day') !!}</button>
            <button class="btn btn-default period" value="month">{!! Lang::get('lang.last-30-days') !!}</button>
            <button class="btn btn-default period" value="year">{!! Lang::get('lang.last-1-year') !!}</button>
            @include('report::popup.mail')
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8">

                <div>
                    <div id="loading-all">
                        <img src="{{asset('lb-faveo/media/images/gifloader.gif')}}">
                    </div>
                    <canvas id="all"></canvas>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li>

                        <div>
                            <div id="loading-source">
                                <img src="{{asset('lb-faveo/media/images/gifloader.gif')}}">
                            </div>
                            <canvas id="source"></canvas>
                        </div>

                    </li>
                    <li>

                        <div>
                            <div id="loading-priority">
                                <img src="{{asset('lb-faveo/media/images/gifloader.gif')}}">
                            </div>
                            <canvas id="priority"></canvas>
                        </div>

                    </li>
                    <li>

                        <div>
                            <div id="loading-sla">
                                <img src="{{asset('lb-faveo/media/images/gifloader.gif')}}">
                            </div>
                            <canvas id="sla"></canvas>
                        </div>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop
@section('FooterInclude')
<script src="{{asset("lb-faveo/js/jquery.ui.js")}}" type="text/javascript"></script>

<!--<script src="{{asset('lb-faveo/plugins/chartjs/Chart.js')}}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
@include('report::asset.all-js')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript">
$(function () {
    $('input[name="interval"]').daterangepicker();
    $('input[name="daterange"]').daterangepicker();
    $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
        var start = picker.startDate.format('YYYY-MM-DD');
        var end = picker.endDate.format('YYYY-MM-DD');
        var period = "custom";
        var chart = "doughnut";
        all(period, 'line', start, end);
        source(chart, period, start, end);
        priority(chart, period, start, end);
        sla(chart, period, start, end);
    });
    $('input[name="date"]').daterangepicker();
    $('input[name="date"]').on('apply.daterangepicker', function (ev, picker) {
        var start = picker.startDate.format('YYYY-MM-DD');
        var end = picker.endDate.format('YYYY-MM-DD');
        var data = $("#export-form").serialize();
        $.ajax({
            cache: false,
            responseType: 'ArrayBuffer', //not sure if needed
            type: "post",
            //dataType: "html",
            url: "{{url('report/export')}}",
            data: data,
            beforeSend: function () {
                $("#loading-form").show();
            },
            complete: function () {
                $("#loading-form").hide();
            },
            success: function (data, textStatus, request) {
                if (data==='success'){
                    window.location = "{{url('report/download')}}";
                }
            },
            error: function (ajaxContext) {
                toastr.error('Export error: ' + ajaxContext.responseText);
            }

        });
    });
});
</script>
<script src="{{asset('lb-faveo/plugins/hailhood-tag/js/tagit.js')}}"></script>
<script>
$('#to').tagit({
    tagSource: "{{url('report/mail/to')}}",
    allowNewTags: true,
    placeholder: 'Enter email address',
    select: true,
});

</script>
@stop

