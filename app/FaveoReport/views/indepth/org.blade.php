@extends('themes.default1.agent.layout.agent')
@section('Staffs')
active
@stop

@section('staffs-bar')
active
@stop

@section('report')
class="active"
@stop

<!-- header -->
@section('PageHeader')
<h1>{!! Lang::get('report::lang.top-customer-analysis') !!}</h1>
@stop
<!-- /header -->
<!-- breadcrumbs -->
@section('breadcrumbs')
<ol class="breadcrumb">
</ol>
@stop
@section('content')
<div class="box box-primary">
    <div class="box-header">
        <button class="btn btn-primary" onclick="showhidefilter();">{!! Lang::get('report::lang.filter') !!}</button>
        @include('report::indepth.filter')
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <canvas id="created-ticket"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="resolved-ticket"></canvas>
            </div>

        </div><br>
        <div class="row">
            <div class="col-md-6">
                <canvas id="unresolved-ticket"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="reopened-ticket"></canvas>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-6">
                <canvas id="response-sla"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="resolve-sla"></canvas>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-6">
                <canvas id="agent-responses"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="client-responses"></canvas>
            </div>
        </div><br>
    </div>
</div>
@stop
@section('FooterInclude')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
<script>
var options_bar = {
    scales: {
        xAxes: [{
            stacked: true,
            display: false,
        }],
        yAxes: [{
            stacked: true,
            barThickness: 10
        }],
    },
    responsive: true,
    maintainAspectRatio: false,
};
var data = $("#filter").serialize();
thisAjax("created-ticket", data);
thisAjax("resolved-ticket", data);
thisAjax("unresolved-ticket", data);
thisAjax("reopened-ticket", data);
thisAjax("response-sla", data,"true");
thisAjax("resolve-sla", data,"true");
thisAjax("agent-responses", data);
thisAjax("client-responses", data);
function thisAjax(active, data, percent = "false", sort = "desc") {
  $.ajax({
        type: "GET",
        dataType: "json",
        url: "{{url('report/organization/get')}}",
        data: "active=" + active + "&percentage=" + percent + "&sort="+sort + "&" + data,
        success: function (data) {
            var ctx = $('#' + active).get(0).getContext("2d");
            var myBarChart = new Chart(ctx,
            {
                type: 'horizontalBar',
                data: data.chart,
                options: options_bar
            });
        },
        error: function (error) {
            alert('error');
        }
    });
}


</script>
@stop

