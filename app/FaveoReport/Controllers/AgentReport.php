<?php

namespace App\FaveoReport\Controllers;

use App\FaveoReport\Controllers\ReportIndepth;
use DB;
use Exception;
use File;

/**
 * Agent report
 * 
 * @abstract ReportController
 * @author Ladybird Web Solution <admin@ithelpdesk.com>
 * @name AgentReport
 * 
 */
class AgentReport extends ReportIndepth {

    /**
     * 
     * get the tickets of an agent
     * 
     * @return json
     */
    public function agent() {
        $chart = $this->request->input('chart', 'line');
        $category = $this->request->input('category', 'week');
        $join = $this->tickets();
        $tickets = $join
                ->when($category, function($query) use ($category) {
                    $ranges = $this->dateRange($category);
                    return $query->whereBetween('tickets.created_at', $ranges)
                            ->groupBy('date');
                })
                ->select(DB::raw('COUNT(tickets.id) as tickets'), 'agent.user_name', 'agent.first_name', 'agent.last_name', 'agent.id', DB::raw("DATE_FORMAT(tickets.created_at, '%Y-%m-%d') as date"))
                ->groupBy('agent.id')
                ->get();
        //dd($tickets, $chart, $category);
        return $this->selectChart($tickets, $chart, $category);
    }

    /**
     * 
     * Get the agent's ticket according to status
     * 
     * @return type
     */
    public function agentStatus() {
        $chart = $this->request->input('chart');
        $category = $this->request->input('category');
        $agentid = $this->request->input('agent', 'vijay');
        $join = $this->tickets();
        $tickets = $join
                ->when($agentid, function($query) use($agentid) {
            return $query->where('agent.user_name', $agentid);
        });
        return $tickets;
    }

    /**
     * 
     * Get the chart of agent
     * 
     * @return json
     */
    public function agentChart() {
        $chart = $this->request->input('chart', 'line');
        $category = $this->request->input('category', 'week');
        $tickets = $this->agentStatus()
                ->select(DB::raw('COUNT(tickets.id) as tickets'), 'agent.user_name', 'agent.first_name', 'agent.last_name', 'agent.id', 'ticket_status.name', DB::raw("DATE_FORMAT(tickets.created_at, '%Y-%m-%d') as date"))
                ->when($category, function($query) use ($category) {
                    $ranges = $this->dateRange($category);
                    return $query->whereBetween('tickets.created_at', $ranges)
                            ->groupBy('date');
                })
                ->groupBy('ticket_status.name')
                ->get();
        //dd($tickets);
        return $this->selectChart($tickets, $chart, $category);
    }

    /**
     * 
     * get agent report view
     * 
     * @return view
     */
    public function getAgent() {
        try {
            if (!$this->policy->report()) {
                return redirect('/')->with('fails', \Lang::get('lang.access-denied'));
            }
            $agents = \App\User::where('role', '!=', 'user')->pluck('user_name', 'user_name')->toArray();
            $table = \App\Model\helpdesk\Ticket\Ticket_Status::pluck('name')->toArray();
            arsort($table);
            return view('report::agents.details', compact('agents', 'table'));
        } catch (Exception $ex) {
            // dd($ex);
            return redirect()->back()->with('fails', $ex->getMessage());
        }
    }

    /**
     * 
     * get the Agent's ticket according to ticket status
     * 
     * @param boolean $table
     * @return collection|json
     */
    public function getAgentStatusTable($table = true) {
        $start = $this->request->input("agent_export_start_date");
        $end = $this->request->input("agent_export_end_date");
        $tickets = $this->agentStatus()
                ->where('agent.role', '!=', 'user')
                ->whereNotNull('agent.id')
                ->when($start, function($query) use ($start) {
                    return $query->whereDate('tickets.created_at', '>', $start);
                })
                ->when($end, function($query) use ($end) {
                    return $query->whereDate('tickets.created_at', '>', $end);
                })
                ->select(DB::raw('COUNT(tickets.id) as tickets'), 'agent.user_name', 'agent.first_name', 'agent.last_name', 'agent.id', 'ticket_status.name as status')
                ->groupBy('agent.id', 'ticket_status.name');
        $collection = $tickets->get()->groupBy('id');
        $agents = collect([]);
        if ($collection->count() > 0) {
            $agents = $this->refin($collection);
        }
        if ($table == true) {
            return \Datatables::of($agents)
                            ->make();
        } else {
            return $agents;
        }
    }

    /**
     * 
     * refine the collection data
     * 
     * @param collection $collections
     * @return collection
     */
    public function refin($collections) {
        $collections->transform(function ($items, $key) {
            foreach ($items as $item) {
                $agent['zz_user_name'] = $item['user_name'];
                $agent['zz_first_name'] = $item['first_name'];
                $agent['zz_last_name'] = $item['last_name'];
                if ($item['status'] != null) {
                    $agent[$item['status']] = $item['tickets'];
                }
            }
            return collect($agent);
        });
        $statuses = \App\Model\helpdesk\Ticket\Ticket_Status::select('name')->get();
        foreach ($statuses as $status) {
            foreach ($collections as $agent_tickets) {
                if (!$agent_tickets->has($status->name)) {
                    $agent_tickets->put($status->name, 0);
                }
            }
        }
        foreach ($collections as $key => $sort) {
            $arrays[$key] = $sort->toArray();
            krsort($arrays[$key]);
        }
        return collect($arrays);
    }

    /**
     * 
     * Export the agent report 
     * 
     * @param boolean $storage
     * @return excel sheet
     */
    public function AgentExport($storage = true, $mail = false) {
        $storage_path = storage_path('exports');
        if (is_dir($storage_path)) {
            delTree($storage_path);
        }
        $tickets = $this->agentPerformance()->get()->toArray();
        if (count($tickets) > 0) {
            $filename = "Agents";
            $excel = \Excel::create($filename, function($excel) use($tickets) {
                        $excel->sheet('sheet', function($sheet) use($tickets) {
                            $sheet->fromArray($tickets);
                        });
                    });
            if ($storage == false) {
                $excel->download("csv");
            } else {
                $path = $excel->store('csv', false, true);
            }
        } else {
            return 'No data to export';
        }
        if ($mail == true) {
            return $path;
        }
        return 'success';
    }

    /**
     * 
     * Send mail of agent report
     * 
     * @return mail
     */
    public function mail() {
        $this->validate($this->request, [
            'to' => 'required',
            'subject' => 'max|20',
        ]);

        try {
            $path = $this->AgentExport(true);
            $this->mailing($path, 'agents');
            $message = "Mail has sent";
            $status_code = 200;
        } catch (Exception $ex) {
            $message = [$ex->getMessage()];
            $status_code = 500;
        }
        return $this->mailResponse($message, $status_code);
    }

    public function getView() {
        if (!$this->policy->report()) {
            return redirect('/')->with('fails', \Lang::get('lang.access-denied'));
        }
        return view("report::agents.performance");
    }

    public function agentPerformance() {
        $tickets = $this->thisTickets()
                ->distinct('tickets.id')
                ->select(
                        \DB::raw('count(distinct tickets.id) as assigned_tickets'), \DB::raw('count(open_status.id) as open_tickets'), \DB::raw('count(close_status.id) as closed_tickets'), \DB::raw('SUM(CASE WHEN tickets.reopened > 0 THEN 1 ELSE 0 END) AS reopened'), \DB::raw('SUM(ticket_thread.response_time) as response_time'), \DB::raw('AVG(ticket_thread.response_time) as avg_response_time'), \DB::raw('count(distinct ticket_thread.id) as number_of_response'), \DB::raw('SUM(CASE WHEN tickets.is_response_sla = 1 THEN 1 ELSE 0 END) AS success_response_sla'), \DB::raw('SUM(CASE WHEN tickets.is_resolution_sla = 1 THEN 1 ELSE 0 END) AS success_resolution_sla'), \DB::raw('AVG(tickets.resolution_time) as avg_resolution_time'), 'agent.user_name as agent'
                )
                ->groupBy('agent.user_name')

        ;
        return $this->addConditionToSchema($tickets);
    }

    public function thisTickets() {
        return \App\Model\helpdesk\Ticket\Tickets::
                        join('users as agent', 'tickets.assigned_to', '=', 'agent.id')
                        ->leftJoin('ticket_status', 'tickets.status', '=', 'ticket_status.id')
                        ->leftJoin('ticket_status_type as open_status', function($join) {
                            $join->on('open_status.id', '=', 'ticket_status.purpose_of_status')
                            ->where('open_status.name', '=', 'open');
                        })
                        ->leftJoin('ticket_status_type as close_status', function($join) {
                            $join->on('close_status.id', '=', 'ticket_status.purpose_of_status')
                            ->where('close_status.name', '=', 'closed');
                        })
                        ->leftJoin('ticket_thread', function($join) {
                            $join->on('tickets.id', '=', 'ticket_thread.ticket_id')
                            ->where('ticket_thread.poster', '=', 'support')
                            ->where('ticket_thread.is_internal', '=', '0');
                        })
        ;
    }

    public function addConditionToSchema($schema, $force = false) {
        //dd($this->request->all());
        $agents = $this->request->input('agents');
        $departmets = $this->request->input('departments');
        $client = $this->request->input('clients');
        $source = $this->request->input('sources');
        $priority = $this->request->input('priorities');
        $type = $this->request->input('types');
        $end_date = \Carbon\Carbon::now();
        $start_date = \Carbon\Carbon::now()->subMonth()->format('Y-m-d');
        $start = $this->request->input('start_date');
        $end = $this->request->input('end_date');
        if (!$start) {
            $start = $start_date;
        }
        if (!$end) {
            $end = $end_date->format('Y-m-d');
        }
        if ($force != true) {
            $schema = $schema
                    ->when($agents, function($query) use($agents) {
                        return $query
                                //->join('users as agent', 'tickets.assigned_to', '=', 'agent.id')
                                ->whereIn('agent.id', $agents);
                    })
                    ->when($departmets, function($query)use($departmets) {
                        return $query
                                ->join('department', 'tickets.dept_id', '=', 'department.id')
                                ->whereIn('department.id', $departmets);
                    })
                    ->when($client, function($query)use($client) {
                        return $query
                                ->join('users as client', 'tickets.user_id', '=', 'client.id')
                                ->whereIn('client.id', $client);
                    })
                    ->when($source, function($query)use($source) {
                        return $query->whereIn('ticket_source.id', $source);
                    })
                    ->when($priority, function($query)use($priority) {
                        return $query->whereIn('ticket_priority.priority_id', $priority);
                    })
                    ->when($type, function($query)use($type) {
                        return $query->whereIn('type.id', $type);
                    })
                    ->whereDate('tickets.created_at', ">=", $start)
                    ->whereDate('tickets.created_at', "<=", $end)
            ;
        }

        return $schema;
    }

    public function agentDatatable() {
        $schema = $this->agentPerformance();
        return \Datatables::of($schema)
                        ->addColumn('avg_response_time', function($ticket) {
                            $hours = "--";
                            if ($ticket->avg_response_time) {
                                $hours = convertToHours($ticket->avg_response_time, '%02dh %02dm');
                            }
                            return $hours;
                        })
                        ->addColumn('avg_resolution_time', function($ticket) {
                            $hours = "--";
                            if ($ticket->avg_resolution_time) {
                                $hours = convertToHours($ticket->avg_resolution_time, '%02dh %02dm');
                            }
                            return $hours;
                        })
                        ->addColumn('success_response_sla', function($ticket) {
                            $hours = 0;
                            if ($ticket->success_response_sla !== '0') {
                                $hours = $ticket->success_response_sla - 1;
                            }
                            return $hours;
                        })
                        ->addColumn('success_resolution_sla', function($ticket) {
                            $hours = 0;
                            if ($ticket->success_resolution_sla !== '0') {
                                $hours = $ticket->success_resolution_sla - 1;
                            }
                            return $hours;
                        })
                        ->make(true);
    }

}
