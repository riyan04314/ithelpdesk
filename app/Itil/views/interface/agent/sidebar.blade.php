<li class="treeview">
    <a href="{{url('dashboard')}}">
        <i class="fa fa-dashboard"></i> 
        <span style="margin-left:-2%;">Dashboard</span> 
    </a>
</li>
<li class="treeview ">
    <a href="#">
        <i class="fa fa-user"></i> <span>Tickets</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu " >
        @if($ticket_policy->create())
        <li @yield('newticket')>
             <a href="{{ url('/newticket')}}" >
                <i class="fa fa-ticket"></i>{!! Lang::get('lang.create_ticket') !!}
            </a>
        </li>
        @endif
        <li @yield('inbox')>
             <a href="{{ url('/tickets')}}" id="load-inbox">
                <i class="fa fa-inbox"></i> <span>{!! Lang::get('lang.inbox') !!}</span> <small class="label pull-right bg-green">{{$tickets -> count()}}</small>                                            
            </a>
        </li>
        <li @yield('myticket')>
             <a href="{{ url('/tickets?show=mytickets')}}" id="load-myticket">
                <i class="fa fa-user"></i> <span>{!! Lang::get('lang.my_tickets') !!} </span>
                <small class="label pull-right bg-green">{{$myticket -> count()}}</small>
            </a>
        </li>
        <li @yield('unassigned')>
             <a href="{{ url('/tickets?assigned[]=0')}}" id="load-unassigned">
                <i class="fa fa-user-times"></i> <span>{!! Lang::get('lang.unassigned') !!}</span>
                <small class="label pull-right bg-green">{{$unassigned -> count()}}</small>
            </a>
        </li>
        <li @yield('overdue')>
             <a href="{{url('/tickets?show=overdue')}}" id="load-unassigned">
                <i class="fa fa-calendar-times-o"></i> <span>{!! Lang::get('lang.overdue') !!}</span>
                <small class="label pull-right bg-green">{{$overdues -> count()}}</small>
            </a>
        </li>
        <li @yield('followup')>
             <a href="{{ url('/tickets?show=followup')}}" id="load-inbox">
                <i class="glyphicon glyphicon-import"></i> <span>{!! Lang::get('lang.followup') !!}</span>
                <small class="label pull-right bg-green">{{$followup_ticket -> count()}}</small>
            </a>
        </li>
        <li @yield('closed')>
             <a href="{{ url('/tickets?show=closed')}}" >
                <i class="fa fa-minus-circle"></i> <span>{!! Lang::get('lang.closed') !!}</span>
                <small class="label pull-right bg-green">{{$closed -> count()}}</small>
            </a>
        </li>
        <li @yield('trash')>
             <a href="{{ url('/tickets?show=trash')}}">
                <i class="fa fa-trash-o"></i> <span>{!! Lang::get('lang.trash') !!}</span>
                <small class="label pull-right bg-green">{{$deleted -> count()}}</small>
            </a>
        </li>

    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-users"></i> <span>	Users</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('user')}}"><i class="fa fa-circle-o"></i> User Directory</a></li>
        <li class=""><a href="{{url('organizations')}}"><i class="fa fa-circle-o"></i>Organization</a></li>
        <li class=""><a href="{{url('user-export')}}"><i class="fa fa-circle-o"></i>Export Users</a></li>
    </ul>
</li>





<li class="treeview">
    <a href="#">
        <i class="fa fa-wrench"></i> <span>	Tools</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu ">
        <li><a href="{{url('canned/list')}}"><i class="fa fa-circle-o"></i> Canned Response</a></li>
        <li class=""><a href="{{url('comment')}}"><i class="fa fa-circle-o"></i>Knowledge Base</a></li>
    </ul>
</li>



<li class="treeview">
    <a href="#">
        <i class="fa fa-bug"></i> <span>Problems</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('service-desk/problems')}}"><i class="fa fa-circle-o"></i> All Problem</a></li>
        <li class=""><a href="{{url('service-desk/problem/create')}}"><i class="fa fa-circle-o"></i>New Problem </a></li>

    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-refresh"></i> <span>Changes</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('service-desk/changes')}}"><i class="fa fa-circle-o"></i> All Changes</a></li>
        <li class=""><a href="{{url('service-desk/changes/create')}}"><i class="fa fa-circle-o"></i>New Changes </a></li>

    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-newspaper-o"></i> <span>Releases</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('service-desk/releases')}}"><i class="fa fa-circle-o"></i> All Releases</a></li>
        <li class=""><a href="{{url('service-desk/releases/create')}}"><i class="fa fa-circle-o"></i>New Releases </a></li>

    </ul>
</li>
{{--
<li class="treeview">
    <a href="#">
        <i class="fa fa-server"></i> <span>Assets</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('service-desk/assets')}}"><i class="fa fa-circle-o"></i> All Assets</a></li>
<li class=""><a href="{{url('service-desk/assets/create')}}"><i class="fa fa-circle-o"></i>New Assets </a></li>
<li><a href="{{url('service-desk/assets/export')}}"><i class="fa fa-circle-o"></i> Export Assets</a></li>
</ul>
</li>
--}}
@push('scripts')
                     <script src="{{asset("lb-faveo/plugins/slimScroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
<script src="{{asset("lb-faveo/js/app.min.js")}}" type="text/javascript"></script>
@endpush