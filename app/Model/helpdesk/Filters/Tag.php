<?php

namespace App\Model\helpdesk\Filters;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    protected $table = 'tags';
    protected $fillable = ['name', 'description'];

    public function filter() {
        $name = $this->attributes['name'];
        $filter = Filter::where('value', $name)->where('key', 'tag');
        return $filter;
    }

    public function update(array $attributes = array(), array $options = array()) {
        if (checkArray('name', $attributes)) {
            $this->filter()->update([
                'value' => $attributes['name']
            ]);
        }
        parent::update($attributes, $options);
    }

    public function delete() {
        $this->filter()->delete();
        parent::delete();
    }

}
